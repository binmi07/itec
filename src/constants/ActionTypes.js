export const GET_USER = 'GET_USER';
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
export const GET_USER_FAILURE = 'GET_USER_FAILURE';
export const REQUEST_SEND_OTP = 'REQUEST_SEND_OTP';
export const REQUEST_SEND_OTP_SUCCESS = 'REQUEST_SEND_OTP_SUCCESS';
export const REQUEST_SEND_OTP_FAILURE = 'REQUEST_SEND_OTP_FAILURE';
export const REQUEST_RESEND_OTP = 'REQUEST_RESEND_OTP';
export const REQUEST_RESEND_OTP_SUCCESS = 'REQUEST_RESEND_OTP_SUCCESS';
export const REQUEST_RESEND_OTP_FAILURE = 'REQUEST_RESEND_OTP_FAILURE';
export const ACTIVATE = 'ACTIVATE';
export const ACTIVATE_SUCCESS = 'ACTIVATE_SUCCESS';
export const ACTIVATE_FAILURE = 'ACTIVATE_FAILURE';
export const NEW_PASSWORD = 'NEW_PASSWORD';
export const NEW_PASSWORD_SUCCESS = 'NEW_PASSWORD_SUCCESS';
export const NEW_PASSWORD_INMAIN_SUCCESS = 'NEW_PASSWORD_INMAIN_SUCCESS';
export const NEW_PASSWORD_FAILURE = 'NEW_PASSWORD_FAILURE';
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_FAILURE = 'CHANGE_PASSWORD_FAILURE';
export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT = 'LOGOUT';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE';
export const SET_AUTOPAYMENT = 'SET_AUTOPAYMENT';
export const SET_AUTOPAYMENT_SUCCESS = 'SET_AUTOPAYMENT_SUCCESS';
export const SET_AUTOPAYMENT_FAILURE = 'SET_AUTOPAYMENT_FAILURE';
export const GET_NOTI = 'GET_NOTI';
export const GET_NOTI_SUCCESS = 'GET_NOTI_SUCCESS';
export const GET_NOTI_FIRST_SUCCESS = 'GET_NOTI_FIRST_SUCCESS';
export const GET_NOTI_NULL_SUCCESS = 'GET_NOTI_NULL_SUCCESS';
export const GET_NOTI_FAILURE = 'GET_NOTI_FAILURE';
export const READ_NOTI_ALL = 'READ_NOTI_ALL';
export const READ_NOTI_ALL_SUCCESS = 'READ_NOTI_ALL_SUCCESS';
export const READ_NOTI_ALL_FAILURE = 'READ_NOTI_ALL_FAILURE';
export const READ_NOTI = 'READ_NOTI';
export const READ_NOTI_SUCCESS = 'READ_NOTI_SUCCESS';
export const READ_NOTI_FAILURE = 'READ_NOTI_FAILURE';
export const DEL_NOTI = 'DEL_NOTI';
export const DEL_NOTI_SUCCESS = 'DEL_NOTI_SUCCESS';
export const DEL_NOTI_FAILURE = 'DEL_NOTI_FAILURE';
export const GET_BILL = 'GET_BILL';
export const GET_BILL_SUCCESS = 'GET_BILL_SUCCESS';
export const GET_BILL_FAILURE = 'GET_BILL_FAILURE';
export const PAYMENT_REQUEST = 'PAYMENT_REQUEST';
export const PAYMENT_REQUEST_SUCCESS = 'PAYMENT_REQUEST_SUCCESS';
export const PAYMENT_REQUEST_FAILURE = 'PAYMENT_REQUEST_FAILURE';
export const PAYMENT_VERIFY = 'PAYMENT_VERIFY';
export const PAYMENT_VERIFY_SUCCESS = 'PAYMENT_VERIFY_SUCCESS';
export const PAYMENT_VERIFY_FAILURE = 'PAYMENT_VERIFY_FAILURE';
export const GET_CALENDAR = 'GET_CALENDAR';
export const GET_CALENDAR_SUCCESS = 'GET_CALENDAR_SUCCESS';
export const GET_CALENDAR_FAILURE = 'GET_CALENDAR_FAILURE';
export const GET_HISTORYS = 'GET_HISTORYS';
export const GET_HISTORYS_SUCCESS = 'GET_HISTORYS_SUCCESS';
export const GET_HISTORYS_FAILURE = 'GET_HISTORYS_FAILURE';
export const GET_HISTORY_DETAIL = 'GET_HISTORYS';
export const GET_HISTORY_DETAIL_SUCCESS = 'GET_HISTORY_DETAIL_SUCCESS';
export const GET_HISTORY_DETAIL_FAILURE = 'GET_HISTORY_DETAIL_FAILURE';
export const GET_CHECKING = 'GET_CHECKING';
export const GET_CHECKING_SUCCESS = 'GET_CHECKING_SUCCESS';
export const GET_CHECKING_FAILURE = 'GET_CHECKING_FAILURE';
export const GET_CHECKING_DETAIL = 'GET_CHECKING_DETAIL';
export const GET_CHECKING_DETAIL_SUCCESS = 'GET_CHECKING_DETAIL_SUCCESS';
export const GET_CHECKING_DETAIL_FAILURE = 'GET_CHECKING_DETAIL_FAILURE';

export const USER_OTP_COUNTDOWN = 'USER_OTP_COUNTDOWN';
export const RESET_OTP_COUNTDOWN = 'RESET_OTP_COUNTDOWN';
export const USER_INPUT_OTP_CODE = 'USER_INPUT_OTP_CODE';
export const RESET_OTP_INPUT = 'RESET_OTP_INPUT';

export const UPDATE_NOTI_SETTING = 'UPDATE_NOTI_SETTING';
export const UPDATE_AUTO_PAYMENT_DAY = 'UPDATE_AUTO_PAYMENT_DAY';
export const UPDATE_CARDS = 'UPDATE_CARDS';
export const UPDATE_CARD_ACTIVE = 'UPDATE_CARD_ACTIVE';
export const UPDATE_CARD_CHOOSE = 'UPDATE_CARD_CHOOSE';
export const UPDATE_KEY = 'UPDATE_KEY';
export const UPDATE_FIRST_START = 'UPDATE_FIRST_START';
export const STATUS_OK = 200;
//------------------------------------------------------------------------
export const CHECK_ACTIVATE = 'CHECK_ACTIVATE';
export const CHECK_ACTIVATE_SUCCESS = 'CHECK_ACTIVATE_SUCCESS';
//-------------------------------------------------------------------------
export const GO_SPLASH = 'GO_SPLASH';
export const GO_LOGIN = 'GO_LOGIN';
export const GO_LOGIN_FIRST = 'GO_LOGIN_FIRST';
export const GO_OTP = 'GO_OTP';
export const GO_MAIN = 'GO_MAIN';
export const GO_WELCOME = 'GO_WELCOME';
export const GO_PIN = 'GO_PIN';
export const GO_AUTOPAYMENT = 'GO_AUTOPAYMENT';
export const GO_PASSWORDVERIFY = 'GO_PASSWORDVERIFY';
export const GO_OTPVERIFY = 'GO_OTPVERIFY';
export const GO_BACKSCREEN = 'GO_BACKSCREEN';
export const GO_PAYMENTSUCCESS = 'GO_PAYMENTSUCCESS';
export const GO_ERRORNETWORK = 'GO_ERRORNETWORK';
export const GO_HISTORYDETAIL = 'GO_HISTORYDETAIL';
export const GO_CHECKINGDETAIL = 'GO_CHECKINGDETAIL';