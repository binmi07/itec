import {Platform} from 'react-native';

export const SFUIRegular = (Platform.OS === 'ios') ? 'SFUIText-Regular' : 'SF-UI-Text-Regular';
export const SFUISemibold = (Platform.OS === 'ios') ? 'SFUIText-Semibold' : 'SFUIText-Semibold';
export const SFUILight = (Platform.OS === 'ios') ? 'SFUIText-Light' : 'SFUIText-Light';
export const SFUIBold = (Platform.OS === 'ios') ? 'SFUIText-Bold' : 'SF-UI-Text-Bold';
export const SFUIMedium = (Platform.OS === 'ios') ? 'SFUIText-Medium' : 'SF-UI-Text-Medium';
// export const SFUIRegular ='SFUIText-Regular';
// export const SFUISemibold ='SFUIText-Semibold';
// export const SFUILight ='SFUIText-Light';
// export const SFUIBold ='SFUIText-Bold';
// export const SFUIMedium ='SFUIText-Medium';