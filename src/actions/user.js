import * as types from '../constants/ActionTypes';
import * as MesTypes from '../constants/MesTypes';
import {errorProcess} from '../components/GlobalVarContainer';
import {
  Alert,
  AlertIOS,
  Platform,
  AsyncStorage
} from 'react-native';

var { 
    getUser,
    activateUser,
    setPassword,
    login,
    logout, 
    resetPassword,
    resetPayment,
    changePassword,
    requestSendActivate,
    requestResendActivate,
} = require('../services/ITECApi');

export function loadConfig(){  
  return async (dispatch) => {
    dispatch({ type: types.GET_USER });
    try{ 
      const cardidactivate = await AsyncStorage.getItem('cardidactivate');      
      if(cardidactivate !== null) {
        let CardId = parseInt(cardidactivate);
        dispatch({
          type: types.UPDATE_CARD_ACTIVE,
          payload: { CardId }
        });
      }
      const username = await AsyncStorage.getItem('username');        
      if (username !== null){
        try{
          let response = await getUser(username);
          console.log(response.data._55);
          let _response = response.data._55;
          if(response.response_status === 200){            
            dispatch({
              type: types.GET_USER_SUCCESS,
              payload: { _response }
            });
            if(_response.status === 0) dispatch({ type: types.GO_OTP });
            else if(_response.status === 1) {
              if(_response.force_login === 'false') {
                let cards = _response.cards;
                dispatch({ type: types.UPDATE_CARDS, payload: { cards } });
                dispatch({ type: types.GO_MAIN });
              }
              else dispatch({ type: types.GO_LOGIN_FIRST });
            }            
          }else{            
            dispatch({ type: types.GET_USER_FAILURE });  
            dispatch({ type: types.GO_WELCOME });
            Alert.alert(
              'Thông báo',
              _response.reason,
              [ {text: 'OK', onPress: () => console.log('OK Pressed')}, ],{ cancelable: false })        
          } 
        }catch(error){ 
          console.log(error) 
        }
      }else{
        dispatch({ type: types.GO_WELCOME });
      }
    }catch(error){dispatch({ type: types.GO_WELCOME });}
  }
}

export function getUser(username) {  
  console.log('ACTION-getUser');  
  return async (dispatch) => {
    dispatch({ type: types.GET_USER });
    try{
      let response = await getUser(username);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({
          type: types.GET_USER_SUCCESS,
          payload: { _response }
        });
        if(_response.status === 0) dispatch({ type: types.GO_OTP });
        else if(_response.status === 1) {
          if(_response.force_login === 'false') {
            let cards = _response.cards;
            dispatch({ type: types.UPDATE_CARDS, payload: { cards } });
            dispatch({ type: types.GO_MAIN });
          }
          else dispatch({ type: types.GO_LOGIN });
        }
      }else{
        dispatch({ type: types.GET_USER_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      } 
    }catch(error){ }
  }
}

export function requestSendActivate(username, tokenid){
  console.log('ACTION-requestSendActivate');
  return async (dispatch) => {
    dispatch({ type: types.REQUEST_SEND_OTP });
    try{
      let response = await requestSendActivate(username, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){        
        dispatch({          
          type: types.REQUEST_SEND_OTP_SUCCESS,  
          payload: { _response }
        });   
      }else{
        dispatch({ type: types.REQUEST_SEND_OTP_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){ }
  }
}

export function requestResendActivate(username, tokenid, retrytime){
  console.log('ACTION-requestResendActivate');
  return async (dispatch) => {
    dispatch({ type: types.REQUEST_RESEND_OTP });
    try{
      let response = await requestResendActivate(username, tokenid, retrytime);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){        
        dispatch({          
          type: types.REQUEST_RESEND_OTP_SUCCESS,  
          payload: { _response }
        });   
      }else{
        dispatch({ type: types.REQUEST_SEND_OTP_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){ }
  }
}

export function activateUser(username, otp, tokenid) {  
  console.log('ACTION-activate');
  return async (dispatch) => {
    dispatch({ type: types.ACTIVATE });
    try{
      let response = await activateUser(username, otp, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){        
        dispatch({          
          type: types.ACTIVATE_SUCCESS,  
          payload: { _response }
        });
        let cards = _response.cards;
        dispatch({
          type: types.UPDATE_CARDS,
          payload: { cards }
        });
        dispatch({ type: types.GO_PIN });        
      }else{
        dispatch({ type: types.ACTIVATE_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){ }
  }
}

export function setPassword(username, password, tokenid) {  
  console.log('ACTION-setPassword');
  return async (dispatch) => {
    dispatch({ type: types.NEW_PASSWORD });
    try{
      let response = await setPassword(username, password, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({ type: types.NEW_PASSWORD_SUCCESS });
        Alert.alert(
          'Thông báo',
          MesTypes.CREATE_PASS_SUCCESS,
          [ {text: 'OK', onPress: () => dispatch({ type: types.GO_LOGIN })}, ],
          { cancelable: false }
        )
      }else{
        dispatch({ type: types.NEW_PASSWORD_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){  }
  }
}

export function setPasswordInMain(username, password, tokenid) {  
  console.log('ACTION-setPassword');
  return async (dispatch) => {
    dispatch({ type: types.NEW_PASSWORD });
    try{
      let response = await setPassword(username, password, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({ type: types.NEW_PASSWORD_INMAIN_SUCCESS });
        Alert.alert(
          'Thông báo',
          MesTypes.CREATE_PASS_SUCCESS,
          [ {text: 'OK', onPress: () => dispatch({ type: types.GO_SPLASH })}, ], { cancelable: false })
      }else{
        dispatch({ type: types.NEW_PASSWORD_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){  }
  }
}

export function login(username, password, tokenid) {  
  console.log('ACTION-login');
  return async (dispatch) => {
    dispatch({ type: types.LOGIN });
    try{
      let response = await login(username, password, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({
          type: types.LOGIN_SUCCESS,
          payload: { _response }
        });
        let cards = _response.cards;
        dispatch({
          type: types.UPDATE_CARDS,
          payload: { cards }
        });
        dispatch({type: types.GO_MAIN});
      }else{
        dispatch({ type: types.LOGIN_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }
    }catch(error){ dispatch({type: types.GO_WELCOME}); }
  }
}

export function logout(username, tokenid, isLogin) {  
  console.log('ACTION-logout');
  return async (dispatch) => {
    dispatch({ type: types.LOGOUT });
    try{
      let response = await logout(username, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({ type: types.LOGOUT_SUCCESS });   
        if(isLogin) dispatch({ type: types.GO_LOGIN_FIRST });     
        else dispatch({ type: types.GO_WELCOME }); 
      }else{
        dispatch({ type: types.LOGOUT_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }
    }catch(error){  }
  }
}

export function resetPassword(username, password, tokenid) {  
  console.log('ACTION-resetPassword');
  return async (dispatch) => {
    try{
      let response = await resetPassword(username, password, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){        
        dispatch({ type: types.GO_WELCOME });        
      }else{
        Alert.alert(
          'Thông báo',
          _response.reason,
          [ {text: 'OK', onPress: () => console.log('OK Pressed')}, ],{ cancelable: false })
      }
    }catch(error){  }
  }
}

export function resetPayment(username, tokenid, amount, transref){
  console.log('ACTION-resetPayment');
  return async (dispatch) => {
    try{
      let response = await resetPayment(username, tokenid, amount, transref);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){        
        
      }else{
        Alert.alert(
          'Thông báo',
          _response.reason,
          [ {text: 'OK', onPress: () => console.log('OK Pressed')}, ],{ cancelable: false })
      }
    }catch(error){  }
  }
}

export function changePassword(username, oldPassword, newPassword, tokenid) {  
  console.log('ACTION-changePassword');
  return async (dispatch) => {
    dispatch({ type: types.CHANGE_PASSWORD });
    try{
      let response = await changePassword(username, oldPassword, newPassword, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){       
        dispatch({ type: types.CHANGE_PASSWORD_SUCCESS });
        Alert.alert(
          'Thông báo',
          MesTypes.CHANGE_PASS_SUCCESS,
          [ {text: 'OK', onPress: () => dispatch({ type: types.GO_SPLASH })},],{ cancelable: false })
      }
      else{
        dispatch({ type: types.CHANGE_PASSWORD_FAILURE }); 
        errorProcess(response.response_status, _response, dispatch);
      }
    }catch(error){  }
  }
}

export function otpCountdown() {
  return {
    type: 'USER_OTP_COUNTDOWN',
  };
}

export function resetOtpCountdown() {
  return {
    type: 'RESET_OTP_COUNTDOWN',
  };
}

export function changeInputOtpCode(InputOtpCode) {
  return {
    type: 'USER_INPUT_OTP_CODE',
    payload: { InputOtpCode },
  };
}

export function updateKey(prevKey){
  return {
    type: types.UPDATE_KEY,
    payload: { prevKey },
  };
}


