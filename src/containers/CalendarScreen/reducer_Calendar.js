import update from 'immutability-helper';
import * as types from '../../constants/ActionTypes';

const initialState = {
    card_number: '9704221711247488',
    from_date: '',
    to_date: '',
    schedules:
    [ 
        {  
            date:"11/12/2017",
            date_name:"Thß╗⌐ hai",
            sessions:
            [ { name: 'Sáng', time: '07:30 - 12:00' },
              { name: 'Chiều', time: '07:30 - 12:00' } 
            ] 
        },
    ]
}

function getMonth(date){
    return date.split("/")[0];
}

export default function calendar(state = initialState, action) {
    switch (action.type) {
        case types.GET_CALENDAR_SUCCESS:
            console.log('REDUCER-GET_CALENDAR_SUCCESS');
            return {...state,
                        schedules: action.payload._response.schedules,
                    };
        default:
            return state;
    }
}