import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  Image,
  ActivityIndicator,
} from 'react-native';

import TitleInfoComponent from '../TitleInfoScreen/TitleInfoComponent';
import Header from '../../components/Header';
import { Grid, Col, Row } from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import AndroidBackButton from 'android-back-button';
import moment from 'moment-timezone';
import {
  getCardNoFromStudentCode,
} from '../../components/GlobalVarContainer'
import {
  moderateScale, scale, WIDTH
} from '../../components/ScaleRes';

var startOfWeek;
var endOfWeek;
var today;
var rowHeader = [];

function updateNewData(){
  startOfWeek = moment().tz("Asia/Ho_Chi_Minh").startOf('week');
  endOfWeek = moment().tz("Asia/Ho_Chi_Minh").endOf('week');
  today = moment().tz("Asia/Ho_Chi_Minh").date();
  rowHeader = [{Thu: 'HAI', Day: startOfWeek.add(1,'days').date()},
              {Thu: 'BA', Day: startOfWeek.add(1,'days').date()},
              {Thu: 'TƯ', Day: startOfWeek.add(1,'days').date()},
              {Thu: 'NĂM', Day: startOfWeek.add(1,'days').date()},
              {Thu: 'SÁU', Day: startOfWeek.add(1,'days').date()},
              {Thu: 'BẢY', Day: startOfWeek.add(1,'days').date()},
              {Thu: 'CN', Day: startOfWeek.add(1,'days').date()}];
}

export default class CalendarScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount(){
    console.log(moment().tz('Asia/Ho_Chi_Minh').format("DD/MM/YYYY HH:mm:ss"));
    updateNewData();
    // let fromdate = (startOfWeek.date() + 1) + '/' + (startOfWeek.month() + 1) + '/' + (startOfWeek.year());
    // let enddate = (endOfWeek.date() + 1) + '/' + (endOfWeek.month() + 1) + '/' + (endOfWeek.year());
    let fromdate = moment().tz("Asia/Ho_Chi_Minh").startOf('week').add(1,'days').format("DD/MM/YYYY");
    let enddate = endOfWeek.add(1,'days').format("DD/MM/YYYY");
    this.props.getCalendar(this.props.username, this.props.studentcode, fromdate, enddate, this.props.tokenid);
  }

  onRefresh(studentcode){
    updateNewData();
    // let fromdate = (startOfWeek.date() + 1) + '/' + (startOfWeek.month() + 1) + '/' + (startOfWeek.year());
    // let enddate = (endOfWeek.date() + 1) + '/' + (endOfWeek.month() + 1) + '/' + (endOfWeek.year());
    let fromdate = moment().tz("Asia/Ho_Chi_Minh").startOf('week').add(1,'days').format("DD/MM/YYYY");
    let enddate = endOfWeek.add(1,'days').format("DD/MM/YYYY");
    this.props.getCalendar(this.props.username, studentcode, fromdate, enddate, this.props.tokenid);
  }

  processTimeString(time){
    return time.replace('-','');
  }

  dateHeader(col) {
    let i = 0;
    return col.map((data) => {
      i++;  
      if(today === data.Day) 
        return (
                <Col size={11} style = {{alignItems: 'center', justifyContent: 'center', borderBottomWidth: 2, borderColor: '#17adec'}} key = {i}>
                  <Text style = {[styles.txtHeaderDate, {color: '#00a6ec'}]}>{data.Thu}</Text>
                  <Text style = {[styles.txtHeaderDay, {color: '#00a6ec'}]}>{data.Day}</Text>
                </Col>)  
      else 
        return (
                <Col size={11} style = {{alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1, borderColor: '#e0e9ed'}} key = {i}>
                  <Text style = {styles.txtHeaderDate}>{data.Thu}</Text>
                  <Text style = {styles.txtHeaderDay}>{data.Day}</Text>
                </Col>) 
    })
  }

  dateBody(col){
    let i = -1;
    return col.map((data) => {
      i++;        
      return (
        <Col size={11} style = {{borderColor: '#e0e9ed', borderLeftWidth: 1}} key = {i}>
          {this.itemBody(data.sessions)}
        </Col>
      )
    })
  }

  itemBody(col){
    let i = -1;
    return col.map((data) => {
      i++; 
      return (
        <Row style = {{borderColor: '#e0e9ed', borderBottomWidth: 1, alignItems: 'center', justifyContent: 'center'}} key = {i}>
          {
            data.status === 1 
              ? <Image style = {{width: 25, height: 25}} source = {require('./img/icoChecked.png')}/>
              : <View/>
          }
        </Row>
      )
    })
  }

  shiftHeader(row){
    let i = -1;
    return row.map((data) => {
      i++; 
      return (
        <Row style = {{borderColor: '#e0e9ed', borderBottomWidth: 1, alignItems: 'center', justifyContent: 'center'}}  key = {i}>
          <View>
            <Text style = {styles.txtCa}>{data.name}</Text>
            <View style = {{height: 3}}/>
            <Text style = {styles.txtTime}>{data.time}</Text>
          </View>
        </Row>
      )
    })
  }

  render() {
    return (     
      <View style = {{flex:1, backgroundColor: '#f2f7f9'}}> 
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <Header
          Left = 'back'
          Body = 'LỊCH HỌC'
          onPress = {() => {this.props.navigation.goBack()}}
        />
        <TitleInfoComponent onPress = {() => {this.props.navigation.navigate('ChooseCard', {onRefresh: this.onRefresh.bind(this)})}}/>
        <View style = {styles.bkgNote}>
          <Text style = {styles.txtNote}>TUẦN TỪ {(moment().tz("Asia/Ho_Chi_Minh").startOf('week').add(1, 'days').format("DD/MM")) + ' '} 
                                             ĐẾN {(moment().tz("Asia/Ho_Chi_Minh").endOf('week').add(1, 'days').format("DD/MM"))}</Text>
        </View>
        <View style = {{height: 50, backgroundColor: "#f2f7f9"}}>
          <Grid>
            <Col size={23} style ={{borderBottomWidth : 1, borderColor: "#e0e9ed", borderStyle: "solid", justifyContent: 'center' }}>
              <Text style = {styles.txtCaThu}>CA / THỨ</Text>
            </Col>
            {this.dateHeader(rowHeader)}
          </Grid>
        </View>
        <View style = {{height: 100, backgroundColor: 'white'}}>
          {
            this.props.isLoading === true 
              ? <View/>
              : <Grid>
                  <Col size={23} style = {{backgroundColor: "#f2f7f9"}}>
                    {this.shiftHeader(this.props.schedules[0].sessions)}
                  </Col>
                  {this.dateBody(this.props.schedules)}
                </Grid>
          }
        </View>
        {
          this.props.isLoading === true ? <ActivityIndicator
                                            animating = {true}
                                            size = 'large'
                                            style = {styles.loading}
                                            color = 'black'/> 
                                        : <View/>
        }
      </View> 
    );
  }
}

const styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },
  txtTime: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(10),
    letterSpacing: 0.13,
    textAlign: "center",
    color: "#898ea3"
  },
  txtCaThu: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: moderateScale(12),
    letterSpacing: 0.16,
    textAlign: "center",
    color: "#a8abb7"
  },
  txtHeaderDate: {
    opacity: 0.9,
    fontFamily: FontFamily.SFUIBold,
    fontWeight: "bold",
    fontSize: moderateScale(12),
    letterSpacing: 0.14,
    textAlign: "center",
    color: "#222f5c"
  },
  txtHeaderDay: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(12),
    letterSpacing: 0.13,
    textAlign: "center",
    color: "#a8abb7"
  },
  bkgNote:{
    height: 45,
    backgroundColor: "#fafbfc",
    alignItems: 'center',
    justifyContent: 'center'
  },
  txtNote: {
    fontFamily: FontFamily.SFUIMedium,
    fontSize: moderateScale(16),
    letterSpacing: 0.17,
    textAlign: "center",
    color: "#878b9a"
  },
  txtContentDate:{
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(12),
    letterSpacing: 0.13,
    textAlign: "center",
    color: "#8da2b6"
  },
  txtContentDateActivate:{
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(12),
    letterSpacing: 0.13,
    textAlign: "center",
    color: "#3bb9ee"
  },
  txtCa: {
    fontFamily: FontFamily.SFUIBold,
    fontSize: moderateScale(12),
    fontWeight: "bold",
    letterSpacing: 0.16,
    textAlign: "center",
    color: "#36426b"
  }
});
