import * as types from '../../constants/ActionTypes';
import {
  Alert,
  AlertIOS,
  Platform,
} from 'react-native';
import * as MesTypes from '../../constants/MesTypes';
import {errorProcess} from '../../components/GlobalVarContainer';

var {
    getCalendar,
} = require('../../services/ITECApi')

export function getCalendar(username, studentcode, fromDate, toDate, tokenid) {  
  console.log('ACTION-getCalendar');  
  return async (dispatch) => {
    dispatch({ type: types.GET_CALENDAR });
    try{
      let response = await getCalendar(username, studentcode, fromDate, toDate, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({
          type: types.GET_CALENDAR_SUCCESS,
          payload: { _response }
        });
      }else{
        dispatch({ type: types.GET_CALENDAR_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
        // switch(response.response_status){
        //   case 500:
        //     dispatch({ type: types.GO_MAIN }); break;
        //   case 1000:
        //     Alert.alert(
        //       'Thông báo',
        //       MesTypes.ERROR_NETWORK,
        //       [ {text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false }); break;
        //   case 410:
        //     Alert.alert(
        //       'Thông báo',
        //       _response.reason,
        //       [ {text: 'OK', onPress: () => dispatch({ type: types.GO_SPLASH})},],{ cancelable: false }); break;
        //   default:
        //     Alert.alert(
        //       'Thông báo',
        //       _response.reason,
        //       [ {text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false });break;
        // }
      }  
    }catch(error){  }
  }
}