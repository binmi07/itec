import { connect } from 'react-redux'
import CalendarScreen from './CalendarScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    getCalendar,
} from '../../actions'

const mapStateToProps = ( state, ownProps ) => {
    return {
        username: state.user.Username,
        studentcode: state.cards.CardActive.student_code,
        tokenid: state.user.TokenID,
        schedules: state.calendar.schedules,
        Cards: state.cards.Cards,
        isLoading: state.progressHud.isLoading,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    getCalendar: (username, studentcode, fromDate, toDate, tokenid) => dispatch(getCalendar(username, studentcode, fromDate, toDate, tokenid)),
})

const CalendarComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( CalendarScreen )

export default CalendarComponent