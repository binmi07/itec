import { connect } from 'react-redux'
import SplashScreen from './SplashScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    loadConfig,
    getNoti,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,
        TokenID: state.user.TokenID,
        Username: state.user.Username,
        isMain: state.user.isMain,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    getNoti : (username, studentcode, fromdate, todate, tokenid) => dispatch(getNoti(username, studentcode, fromdate, todate, tokenid)),
    loadConfig: () => dispatch(loadConfig()),
})

const SplashComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( SplashScreen )

export default SplashComponent