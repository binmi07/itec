import { connect } from 'react-redux'
import LoginScreen from './LoginScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    login,
  } from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        user: state.user,
        isLoading: state.progressHud.isLoading,
        isItec: state.user.isItec,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    goWelcome: () => {
        dispatch( {
            type: types.GO_WELCOME
        } )
    },
    login: (username, password, tokenid) => dispatch(login(username, password, tokenid)),
})

const LoginComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( LoginScreen )

export default LoginComponent