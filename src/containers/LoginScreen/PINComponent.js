import { connect } from 'react-redux'
import PINScreen from './PINScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    setPassword,
  } from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,
        Username: state.user.Username,
        TokenID: state.user.TokenID,
        isItec: state.user.isItec,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    setPassword: (username, password, tokenid) => dispatch(setPassword(username, password, tokenid)),
    onGoMain: () => dispatch( {type: types.GO_MAIN} ) ,
})

const PINComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( PINScreen )

export default PINComponent