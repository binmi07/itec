import React, { Component } from 'react';
import { Alert, StyleSheet, Image, TouchableOpacity, ImageBackground, View, Text, TextInput, StatusBar, ActivityIndicator, Keyboard, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, Dimensions } from 'react-native';
import {Grid, Col, Row} from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import LoginButton from '../../components/LoginButton';
import {scale, moderateScale, verticalScale, WIDTH} from '../../components/ScaleRes';
import md5 from 'react-native-md5';
import * as actionTypes from '../../constants/ActionTypes';
import * as config from '../../constants/Config';
import { callPhone, forgetPass } from '../../components/GlobalVarContainer';
import * as MesTypes from '../../constants/MesTypes';

export default class LoginScreen extends Component {
  constructor(props){
    super(props);
    this.onPressLogin = this.onPressLogin.bind(this);
    this.state = {
      password: '',
    }
  }
  componentWillMount(){
    
  }

  onPressLogin(){
    Keyboard.dismiss();
    if(this.state.password.length !== 6) {
      Alert.alert(
        'Thông báo',
        MesTypes.PASS_MUST_6_LETTERS,
        [ {text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false })
    }else{
      this.props.login(this.props.user.Username, this.state.password, this.props.user.TokenID);
    }
  }

  render() {
    // console.log(md5.hex_md5(password, AuthKey))
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style = {{flex: 1}}>
          <StatusBar
            backgroundColor="transparent" translucent
          />
          <ImageBackground 
            style = {styles.background} 
            source={require('./img/background.png')}>          
              <View 
                style = {{alignItems: 'center'}}>
                <Image 
                  style = {styles.logo} 
                  source={require('./img/loginLogo.png')}/>                  
                <Text style = {styles.txtName}>{this.props.user.Fullname}</Text>
                <Text style = {styles.txtPhoneNumber}>{this.props.user.PhoneNumber}</Text>
                <TextInput
                  style = {styles.input}
                  autoFocus = {true}
                  blurOnSubmit = {true}
                  secureTextEntry = {true}
                  maxLength = {6}
                  keyboardType = {Platform.OS === 'ios' ? 'phone-pad' : 'default'}
                  placeholder = "Mật khẩu"
                  underlineColorAndroid = 'transparent'
                  onChangeText = {(value) => this.setState({password: value})}
                  value={this.state.password}
                />
                <LoginButton                 
                  style = {styles.button}
                  text = 'ĐĂNG NHẬP'
                  width = {WIDTH * 0.84}
                  height = {WIDTH * 0.84 / 7}
                  textSize = {moderateScale(18)}
                  onPress = {() => {this.onPressLogin()}} 
                />     
                <View style = {{flexDirection: 'row', justifyContent: 'center'}}>             
                  <TouchableOpacity onPress = {() => forgetPass(config.HOT_LINE)}>
                    <Text style = {styles.txtForget}>Quên mật khẩu</Text>
                  </TouchableOpacity>
                  <Text style = {styles.txtForget}>| </Text>
                  <TouchableOpacity onPress = {() => {this.props.goWelcome()}}>
                    <Text style = {styles.txtForget}>Đăng nhập tài khoản khác</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <KeyboardAvoidingView style={{flex:1}} behavior="padding">
                {
                  this.props.isItec === true
                  ? undefined
                  : <TouchableOpacity 
                      onPress={() => callPhone(config.HOT_LINE)}
                      style = {{flexDirection: 'row', justifyContent: 'center', position: 'absolute', bottom: 20, left: 0, right: 0}}>                  
                        <Text style = {styles.txtHotline}>Hotline: </Text>
                        <Text style = {styles.txtPhone}>{config.HOT_LINE}</Text>                 
                    </TouchableOpacity>
                }     
              </KeyboardAvoidingView>
          </ImageBackground>
          {
            this.props.isLoading === true ? <ActivityIndicator
                                              animating = {true}
                                              size = 'large'
                                              style = {styles.loading}
                                              color = 'black'/> 
                                          : <View/>
          }
          
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

var styles = StyleSheet.create({
  txtForget: {
    color: '#ade4ff', 
    fontSize: moderateScale(16), 
    textAlign: 'center', 
    marginTop: 10, 
    fontFamily: FontFamily.SFUIRegular,
    backgroundColor: 'transparent',
  },
  txtPhone: {
    color: '#46c8ff', 
    fontSize: moderateScale(16), 
    marginLeft: 5, 
    fontFamily: FontFamily.SFUISemibold,
    backgroundColor: 'transparent'
  },
  txtHotline: {
    color: 'white', 
    fontSize: moderateScale(16), 
    fontFamily: FontFamily.SFUILight,
    backgroundColor: 'transparent'
  },
  txtPhoneNumber: {
    color: '#65deff', 
    fontSize: moderateScale(17), 
    textAlign: 'center', 
    marginTop: 5, 
    fontFamily: FontFamily.SFUIRegular,
    backgroundColor: 'transparent'
  },
  txtName:{
    color: 'white', 
    fontSize: moderateScale(24), 
    textAlign: 'center',
    fontFamily: FontFamily.SFUILight,
    backgroundColor: 'transparent'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },
  container: {
    //alignItems : 'center'
  },
  background: {
    flex:1, 
    //alignItems: 'center',
  },
  logo: {
    marginTop: moderateScale(60),
    width: moderateScale(218),
    height: moderateScale(121),
  },
  button: {
    marginTop: moderateScale(20),
    // backgroundColor: '#3cc2fd',
    // height: 45,
  },
  input: {
    marginTop: moderateScale(10),
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(16), 
    textAlign: 'center',
    width: WIDTH * 0.84,
    height: WIDTH * 0.84 / 7,
    borderRadius: 4,
    backgroundColor: 'white'
  }
});