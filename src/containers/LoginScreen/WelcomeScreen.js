import React, { Component } from 'react';
import {Alert, StyleSheet, Image, ImageBackground, Dimensions, View, StatusBar, Text, TextInput, TouchableOpacity, ActivityIndicator, Keyboard, KeyboardAvoidingView, Platform, TouchableWithoutFeedback } from 'react-native';
import {Grid, Col, Row} from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import LoginButton from '../../components/LoginButton';
import TextInputForm from '../../components/TextInputForm';
import * as actionTypes from '../../constants/ActionTypes';
import * as config from '../../constants/Config';
import * as MesTypes from '../../constants/MesTypes';
import { callPhone } from '../../components/GlobalVarContainer';

import {scale, moderateScale, verticalScale, WIDTH} from '../../components/ScaleRes';

export default class WelcomeScreen extends Component {
  constructor(props){
    super(props);
    this.onPressContinue = this.onPressContinue.bind(this);
    this.onPressReset = this.onPressReset.bind(this);
    this.state = {
      username: '',
    }
  }

  onPressTest(){
    //this.props.onLogin();
  }

  onPressReset(){
    // this.props.resetPassword('01686476291', '123456', 'E937594FB1283B929BBFC8AC9F0C0312');
  }

  onPressContinue(){
    this.onPressTest();
    Keyboard.dismiss();
    if(this.state.username === '') {
      Alert.alert(
        'Thông báo',
        MesTypes.NOT_INPUT_PHONENUMBER,
        [ {text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false })
    }else{
      this.props.getUser(this.state.username);
    }
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style = {{flex: 1}}>
          <StatusBar
            backgroundColor = "transparent" translucent
          />
          <ImageBackground 
            style = {styles.background} 
            source={require('./img/background.png')}>
              <View 
                style = {{alignItems: 'center'}}>
                <Image 
                  style = {styles.logo} 
                  source={require('./img/loginLogo.png')}/>     
                <TextInput
                  style = {styles.input}
                  autoFocus = {true}
                  blurOnSubmit = {true}
                  // keyboardType = 'phone-pad'
                  placeholder = "Email / Số điện thoại"
                  underlineColorAndroid = 'transparent'
                  autoCorrect = {false}
                  onChangeText = {(value) => this.setState({username: value})}
                  value={this.state.username}
                />
                <LoginButton 
                  disable = {this.state.username.length < 6 ? true : false}
                  style = {styles.button}
                  text = 'TIẾP TỤC'
                  width = {WIDTH * 0.84}
                  height = {WIDTH * 0.84 / 7}
                  textSize = {moderateScale(18)}
                  onPress = {() => {this.onPressContinue()}} 
                />
              </View>
              {/* <KeyboardAvoidingView style={{flex:1}} behavior="padding">
                {
                  this.props.isItec === true
                  ? undefined
                  : <TouchableOpacity 
                      onPress={() => callPhone(config.HOT_LINE)}
                      style = {{flexDirection: 'row', justifyContent: 'center', position: 'absolute', bottom: 20, left: 0, right: 0}}>                  
                        <Text style = {styles.txtHotline}>Hotline: </Text>
                        <Text style = {styles.txtPhone}>{config.HOT_LINE}</Text>                 
                    </TouchableOpacity>
                }                
              </KeyboardAvoidingView> */}
          </ImageBackground>
          {
            this.props.isLoading === true ? <ActivityIndicator
                                              animating = {true}
                                              size = 'large'
                                              style = {styles.loading}
                                              color = 'black'/> 
                                          : <View/>
          }
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

var styles = StyleSheet.create({
  txtPhone: {
    color: '#46c8ff', 
    fontSize: moderateScale(16), 
    marginLeft: 5, 
    fontFamily: FontFamily.SFUISemibold,
    backgroundColor: 'transparent'
  },
  txtHotline: {
    color: 'white', 
    fontSize: moderateScale(16), 
    fontFamily: FontFamily.SFUILight,
    backgroundColor: 'transparent'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },
  container: {
    //alignItems : 'center'
  },
  background: {
    flex:1, 
    // alignItems: 'center',
  },
  logo: {
    marginTop:60,   
    width: moderateScale(218),
    height: moderateScale(121),
  },
  form: {
    marginTop:35,
    alignItems: 'center',
  },
  button: {
    marginTop: 20,
    // backgroundColor: '#3cc2fd',
    // height: 45,
  },
  input: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(16),
    textAlign: 'center',
    width: WIDTH * 0.84,
    height: WIDTH * 0.84 / 7,
    borderRadius: 4,
    backgroundColor: 'white'
  }
});