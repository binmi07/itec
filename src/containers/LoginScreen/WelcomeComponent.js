import { connect } from 'react-redux'
import WelcomeScreen from './WelcomeScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    getUser,
    resetPassword
  } from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,
        isItec: state.user.isItec,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    onLogin: () => {
        dispatch( {
            type: types.SET_AUTOPAYMENT_SUCCESS
        } )
    },
    getUser: (username) => dispatch(getUser(username)),
    resetPassword: (username, password, tokenid) => dispatch(resetPassword(username, password, tokenid)),
})

const WelcomeComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( WelcomeScreen )

export default WelcomeComponent