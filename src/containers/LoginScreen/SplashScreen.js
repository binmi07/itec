import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native';

import Header from '../../components/Header';
import { Grid, Col, Row } from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import AndroidBackButton from 'android-back-button';

export default class SplashScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    //   let username = AsyncStorage.getItem('username');
    //   console.log(JSON.parse(username) )
    // this.props.getNoti('01686476291','010430000000007','','','46E10AC31D64F4DF3FD05077B1F0B3C4');
    if(!this.props.isMain){
      console.log('THANG SPLASH NAY DAY');            
      this.props.loadConfig();
    }
  }

  render() {
    return (     
      <View style = {{flex: 1, justifyContent: 'center'}}>
        <ActivityIndicator
          animating = {true}
          size = 'large'
          style = {styles.loading}
          color = 'black'/> 
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },
});
