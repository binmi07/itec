import React, { Component } from 'react';
import { 
  Alert, 
  StyleSheet, 
  Image,
  TouchableWithoutFeedback,
  View,
  TextInput, 
  ImageBackground,
  StatusBar,
  Text,
  Keyboard,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
  Dimensions,
} from 'react-native';
import {Grid, Col, Row} from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import {scale, moderateScale, verticalScale, WIDTH} from '../../components/ScaleRes';
import LoginButton from '../../components/LoginButton';
import * as actionTypes from '../../constants/ActionTypes';
import * as config from '../../constants/Config';
import { callPhone } from '../../components/GlobalVarContainer';

var interval;

export default class OTPScreen extends Component {  
  constructor(props){
    super(props);
    this.onPressSendOTP = this.onPressSendOTP.bind(this);
    this.onCallresetOtpCountdown = this.onCallresetOtpCountdown.bind(this);
    this.onOTPBoxPress = this.onOTPBoxPress.bind(this);
    this.onHiddenInputChangeText = this.onHiddenInputChangeText.bind(this);
  }

  componentWillMount() {
    this.props.resetOtpInput();
    this.props.resetOtpCountdown();
    this.props.requestSendActivate(this.props.Username, this.props.TokenID);
  }

  componentDidMount() {
    if(!this.props.isOtpCounting){
      interval = setInterval(() => {
        this.props.otpCountdown();
      }, 1000);
    }
  }

  onCallresetOtpCountdown(){
    this.props.requestResendActivate(this.props.Username, this.props.TokenID, this.props.OtpRetryTime);
    this.props.resetOtpCountdown();
  }
  
  onPressSendOTP(otp){
    this.props.activateUser(this.props.Username,
                            otp,
                            this.props.TokenID);
  }

  onHiddenInputChangeText(text) {
    let digits = text.match(/\d+/g);
    if (digits === null) {
      text = '';
    } else {
      text = digits.join([]);
    }
    this.props.changeInputOtpCode(text);    
    if (text.length === 4) {
      Keyboard.dismiss();
      console.log('VERIFY OTP NOW bbbbb');      
      //clearInterval(interval);    
      this.onPressSendOTP(text);
    } 
  }

  onConvertTimeToString(){
    let mm = Math.floor(this.props.OtpTimeInSecond / 60).toString();
    let ss = (this.props.OtpTimeInSecond % 60).toString();
    return (mm.length === 2 ? mm : '0' + mm) + ':' + (ss.length === 2 ? ss : '0' + ss);
  }

  onOTPBoxPress() {
    console.log('HAHAHA');
    this.refs.hiddenInput.focus();
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style = {{flex: 1}}>
          <StatusBar
            backgroundColor="transparent" translucent
          />
          <ImageBackground 
            style = {styles.background} 
            source={require('./img/background.png')}>
              <View 
                style = {{alignItems: 'center'}}>
                <Image 
                  style = {styles.logo} 
                  source={require('./img/loginLogo.png')}/>                  
                <Text style = {styles.txtRow1}>Mã xác minh đã được gửi đến số điện thoại</Text>
                <Text style = {styles.txtRow2}>{this.props.PhoneNumber}</Text>
                <Text style = {styles.txtRow1}>Hãy nhập mã xác minh để tiếp tục</Text>
                <TextInput
                  ref="hiddenInput"
                  style={styles.hiddenInput}
                  autoFocus={true}
                  keyboardType={'numeric'}
                  maxLength={4}
                  onChangeText={this.onHiddenInputChangeText}
                  onSubmitEditing={() => { this.refs.hiddenInput.blur(); }}
                  value={this.props.InputOtpCode}
                />
                <TouchableWithoutFeedback onPress = {this.onOTPBoxPress}>
                  <View style={styles.otpBox}>
                    {[...Array(4)].map((x, i) =>
                      <View style={styles.otpCodeView} key = {i}>
                        <Text
                          ref = {'otp_' + i}
                          style={styles.otpCode}>
                          {i < this.props.InputOtpCode.length ? this.props.InputOtpCode[i] : ''}
                        </Text>
                      </View>
                    )}
                  </View>
                </TouchableWithoutFeedback>
                <Text style = {styles.txtCountDown}>{this.onConvertTimeToString()}</Text>
                <LoginButton 
                  disable = {this.props.OtpTimeInSecond === 0 ? false:true}
                  style = {styles.button}
                  text = 'GỬI LẠI MÃ'
                  width = {WIDTH * 0.84}
                  height = {WIDTH * 0.84 / 7}
                  textSize = {moderateScale(18)}
                  onPress = {() => {this.onCallresetOtpCountdown()}} 
                />
                {/* <Button transparent onPress = {() => {this.props.onBackWelcome()}}><Text>Trở lại trang đăng nhập</Text></Button> */}
                
              </View>
              <KeyboardAvoidingView style={{flex:1}} behavior="padding">
                {
                  this.props.isItec === true
                  ? undefined
                  : <TouchableOpacity 
                      onPress={() => callPhone(config.HOT_LINE)}
                      style = {{flexDirection: 'row', justifyContent: 'center', position: 'absolute', bottom: 20, left: 0, right: 0}}>                  
                        <Text style = {styles.txtHotline}>Hotline: </Text>
                        <Text style = {styles.txtPhone}>{config.HOT_LINE}</Text>                 
                    </TouchableOpacity>
                }     
              </KeyboardAvoidingView>
          </ImageBackground>
          {
            this.props.isLoading === true ? <ActivityIndicator
                                              animating = {true}
                                              size = 'large'
                                              style = {styles.loading}
                                              color = 'black'/> 
                                          : <View/>
          }
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

var styles = StyleSheet.create({
  txtPhone: {
    color: '#46c8ff', 
    fontSize: moderateScale(16), 
    marginLeft: 5, 
    fontFamily: FontFamily.SFUISemibold,
    backgroundColor: 'transparent'
  },
  txtHotline: {
    color: 'white', 
    fontSize: moderateScale(16), 
    fontFamily: FontFamily.SFUILight,
    backgroundColor: 'transparent'
  },
  txtCountDown: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(18),
    letterSpacing: 0.73,
    textAlign: "center",
    color: 'white',
    backgroundColor: 'transparent'
  },
  txtRow1: {
    color: 'white', 
    fontSize: moderateScale(15), 
    textAlign: 'center', 
    fontFamily: FontFamily.SFUIRegular,
    backgroundColor: 'transparent'
  },
  txtRow2: {
    color: '#63dafc', 
    fontSize: moderateScale(28), 
    textAlign: 'center', 
    fontFamily: FontFamily.SFUIMedium,
    backgroundColor: 'transparent'
  },
  container: {
    //alignItems : 'center'
  },
  background: {
    flex:1, 
    //alignItems: 'center',
  },
  logo: {
    marginTop: 60,
    width: moderateScale(218),
    height: moderateScale(121),   
  },
  form: {
    marginTop:10,
  },
  button: {
    marginTop: 20,
    // backgroundColor: '#3cc2fd',
    // height: 45,
  },
  input: {
    // backgroundColor : 'white', 
    textAlign: 'center',
    // height: 45,
  },
  otpBox: {
    height: moderateScale(70),
    width: moderateScale(265),
    // paddingLeft: 5,
    // paddingRight: 5,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-around',
    // backgroundColor: 'black',
  },
  otpCodeView: {
    width: moderateScale(35),
    height: moderateScale(44),
    borderRadius: 4,
    backgroundColor: "#eefaff",
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
    paddingBottom: 5,
  },
  otpCode: {
    fontSize: moderateScale(18),
    letterSpacing: 0.17,
    textAlign: "center",
    //color: "#a5a8b3",
    fontFamily: FontFamily.SFUISemibold,
  },
  hiddenInput: {
    width: 0,
    height: 0.5,
    position: 'absolute',
    top: -20,
  },
});