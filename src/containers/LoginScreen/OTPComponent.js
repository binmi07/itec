import { connect } from 'react-redux'
import OTPScreen from './OTPScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    activateUser,
    changeInputOtpCode,
    requestSendActivate,
    requestResendActivate,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,
        isOtpCounting: state.user.isOtpCounting,
        Username: state.user.Username,
        TokenID: state.user.TokenID,
        InputOtpCode: state.user.InputOtpCode,
        PhoneNumber: state.user.PhoneNumber,
        OtpTimeInSecond: state.user.OtpTimeInSecond,
        OtpRetryTime: state.user.OtpRetryTime,
        isItec: state.user.isItec,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    resetOtpCountdown: () => {dispatch({type: types.RESET_OTP_COUNTDOWN})},
    otpCountdown: () => {dispatch({type: types.USER_OTP_COUNTDOWN})},
    resetOtpInput: () => {dispatch({type: types.RESET_OTP_INPUT})},
    activateUser: (username, otp, retrytime, tokenid) => dispatch(activateUser(username, otp, retrytime, tokenid)),
    requestSendActivate: (username, tokenid) => dispatch(requestSendActivate(username, tokenid)),
    requestResendActivate: (username, tokenid, retrytime) => dispatch(requestResendActivate(username, tokenid, retrytime)),
    changeInputOtpCode: (InputOtpCode) => dispatch(changeInputOtpCode(InputOtpCode)),
})

const OTPComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( OTPScreen )

export default OTPComponent