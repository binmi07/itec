import React, { Component } from 'react';
import { 
  Text, 
  TextInput, 
  Alert, 
  StyleSheet, 
  Image, 
  TouchableOpacity, 
  ImageBackground, 
  StatusBar, 
  View, 
  ActivityIndicator, 
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  Dimensions,
} from 'react-native';
import {Grid, Col, Row} from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import {scale, moderateScale, verticalScale, WIDTH} from '../../components/ScaleRes';
import LoginButton from '../../components/LoginButton';
import * as actionTypes from '../../constants/ActionTypes';
import * as config from '../../constants/Config';
import * as MesTypes from '../../constants/MesTypes';
import { callPhone } from '../../components/GlobalVarContainer';

export default class PINScreen extends Component {
  constructor(props){
    super(props);
    this.onPressAccept = this.onPressAccept.bind(this);
    this.state = {
      password: '',
      repassword: '',
    }
  }

  onPress_PassLate(){
    this.props.onGoMain();
  }

  onPressAccept(){
    if(this.state.password.length !== 6){
      Alert.alert(
        'Thông báo',
        MesTypes.PASS_MUST_6_LETTERS,
        [ {text: 'OK', onPress: () => console.log('OK Pressed')},],
        { cancelable: false }
      )
    }
    else if(this.state.password !== this.state.repassword){
      Alert.alert(
        'Thông báo',
        MesTypes.REPASS_NOT_CORRECT,
        [ {text: 'OK', onPress: () => console.log('OK Pressed')},],
        { cancelable: false }
      )
    }    
    else{
      // this.props.setPassword(this.props.Username, this.state.password, this.props.TokenID);
      this.props.setPassword(this.props.Username, this.state.password, this.props.TokenID);
    }
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style = {{flex: 1}}>
          <StatusBar
            backgroundColor="transparent" translucent
          />
          <ImageBackground 
            style = {styles.background} 
            source={require('./img/background.png')}>          
              <View style = {{alignItems: 'center'}}>
                <Image 
                  style = {styles.logo} 
                  source={require('./img/loginLogo.png')}/>
                <TextInput
                  style = {styles.input}
                  autoFocus = {true}
                  blurOnSubmit = {true}
                  secureTextEntry 
                  keyboardType = {Platform.OS === 'ios' ? 'phone-pad' : 'default'}
                  maxLength = {6}
                  placeholder = "Mật khẩu"
                  underlineColorAndroid = 'transparent'
                  onChangeText = {(value) => this.setState({password: value, repassword: this.state.repassword})}
                  value={this.state.password}
                />
                <TextInput
                  style = {styles.input}
                  blurOnSubmit = {true}
                  secureTextEntry 
                  keyboardType = {Platform.OS === 'ios' ? 'phone-pad' : 'default'}
                  maxLength = {6}
                  placeholder = "Nhập lại mật khẩu"
                  underlineColorAndroid = 'transparent'
                  onChangeText = {(value) => this.setState({password: this.state.password, repassword: value})}
                  value={this.state.repassword}
                />
                <LoginButton 
                  disable = {(this.state.password.length === 0 || this.state.repassword.length === 0)}
                  style = {styles.button}
                  text = 'XÁC NHẬN'
                  width = {WIDTH * 0.84}
                  height = {WIDTH * 0.84 / 7}
                  textSize = {moderateScale(18)}
                  onPress = {() => {this.onPressAccept()}} 
                />
                <TouchableOpacity onPress = {() => this.onPress_PassLate()}>
                  <Text style = {styles.txtPassLate}>Tạo mật khẩu sau</Text>
                </TouchableOpacity>
              </View>
              <KeyboardAvoidingView style={{flex:1}} behavior="padding">
                {
                  this.props.isItec === true
                  ? undefined
                  : <TouchableOpacity 
                      onPress={() => callPhone(config.HOT_LINE)}
                      style = {{flexDirection: 'row', justifyContent: 'center', position: 'absolute', bottom: 20, left: 0, right: 0}}>                  
                        <Text style = {styles.txtHotline}>Hotline: </Text>
                        <Text style = {styles.txtPhone}>{config.HOT_LINE}</Text>                 
                    </TouchableOpacity>
                }     
              </KeyboardAvoidingView>
          </ImageBackground>
          {
            this.props.isLoading === true ? <ActivityIndicator
                                              animating = {true}
                                              size = 'large'
                                              style = {styles.loading}
                                              color = 'black'/> 
                                          : <View/>
          }
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

var styles = StyleSheet.create({
  txtPassLate: {
    marginTop: 10,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(16),
    letterSpacing: 0.17,
    textAlign: "center",
    color: "#ade4ff",
    backgroundColor: 'transparent'
  },
  txtPhone: {
    color: '#46c8ff', 
    fontSize: moderateScale(16),
    marginLeft: 5, 
    fontFamily: FontFamily.SFUISemibold,
    backgroundColor: 'transparent'
  },
  txtHotline: {
    color: 'white', 
    fontSize: moderateScale(16),
    fontFamily: FontFamily.SFUILight,
    backgroundColor: 'transparent'
  },
  container: {
    //alignItems : 'center'
  },
  background: {
    flex:1, 
    //alignItems: 'center',
  },
  logo: {
    marginTop:60,   
    width: moderateScale(218),
    height: moderateScale(121),
  },
  form: {
    marginTop:0,
    alignItems: 'center',
  },
  button: {
    marginTop: 20,
  },
  input: {
    marginTop:10,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(16),
    textAlign: 'center',
    width: WIDTH * 0.84,
    height: WIDTH * 0.84 / 7,
    borderRadius: 4,
    backgroundColor: 'white'
  }
});