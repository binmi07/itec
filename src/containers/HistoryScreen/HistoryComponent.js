import { connect } from 'react-redux'
import HistoryScreen from './HistoryScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    getHistorys,
    getHistoryDetail
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        Username: state.user.Username,
        TokenID: state.user.TokenID,
        Cardnumber: state.cards.CardActive.card_number,
        // isLoading: state.progressHud.isLoading,
        // transactions: state.history.transactions,
        Cards: state.cards.Cards,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    getHistorys: (username, tokenid, cardnumber) => dispatch(getHistorys(username, tokenid, cardnumber)),
    // getHistoryDetail: (username, tokenid, cardnumber, transref) => dispatch(getHistoryDetail(username, tokenid, cardnumber, transref)),
})

const HistoryComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( HistoryScreen )

export default HistoryComponent