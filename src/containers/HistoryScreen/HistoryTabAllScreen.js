import React, { Component } from 'react';
import {Grid, Rol, Col} from 'react-native-easy-grid';
import { StyleSheet, FlatList, View, Text, TouchableOpacity } from 'react-native';
import * as FontFamily from '../../constants/FontFamily';
import Avatar from '../../components/Avatar';
import moment from 'moment-timezone';
import {
  numberWithCommas,
  getCardNoFromStudentCode,
} from '../../components/GlobalVarContainer';
import {moderateScale, scale, WIDTH} from '../../components/ScaleRes';

export default class HistoryTabAllScreen extends Component{
  constructor(props){
    super(props);
    this.state = {
      refresh : false,
    }
  }

  componentWillMount(){
    this.refreshList();
  }
  
  refreshList(){
    //-----------------------LOAD DATA VAO MANG----------------------------
    this.props.getHistorys(this.props.Username, this.props.TokenID, this.props.Cardnumber);
    //---------------------------------------------------------------------
  }

  onPressHistoryDetail(item){
    // this.props.resetPayment(this.props.Username, this.props.TokenID, item.amount, item.ref_number);
    this.props.getHistoryDetail(this.props.Username, 
                                this.props.TokenID,  
                                this.props.Cardnumber, 
                                item.ref_number);
  }

  updateList(student_code){    
    this.props.getHistorys(this.props.Username, 
                            this.props.TokenID, 
                            getCardNoFromStudentCode(this.props.Cards, student_code));
  }

  getDay(day){
    let string = day.split("/");
    return string[0] + '/'+ string[1];
  }

  getDate(day){
    let day1 = moment(day.split(" ")[0],"DD/MM/YYYY").day();
    return (day1 === 0 ? 'CN' : ('THỨ ' + (day1 + 1)));
  }

  getMonth(day){
    let string = day.split(" ");
    let string1 = string[0].split("/");
    return string1[1] + '/'+ string1[2];
  }

  render(){
    return(   
      <View style = {{flex:1}}> 
        <FlatList 
          refreshing = {this.props.isLoading}
          onRefresh = {() => {this.refreshList()}}
          data = {this.props.transactions}
          renderItem = {
            ({item}) => 
              <TouchableOpacity 
                onPress = {() => this.onPressHistoryDetail(item)}
                style = {{borderColor: '#dfecf1', borderBottomWidth: 1, backgroundColor: 'white'}}>
                <Grid style = {{height: 70}}>
                  <Col size={18} style = {{alignItems:'center', justifyContent: 'center'}}>
                    <Avatar 
                      width = {35} 
                      height = {35} 
                      url = {item.channel === 'Mobile' ? require('./img/icoBycash.png')
                                                      : require('./img/icoBycard.png')}
                    />
                  </Col>
                  <Col size={60} style = {{justifyContent: 'center'}}>
                    <Text style = {styles.textMoney}>{(item.status === 0 ? '- ' : '+ ') + numberWithCommas(item.amount)}</Text>
                    <Text style = {styles.textNote}>{item.description}</Text>
                  </Col>
                  <Col size={22} style = {{justifyContent: 'center',alignItems:'center'}}>
                      <Text style = {styles.textDay}>{this.getDate(item.date)}</Text>
                      <View style = {styles.bkgDate}>
                        <Text style = {styles.textDate}>{this.getDay(item.date)}</Text>
                      </View>
                  </Col>
                </Grid>
              </TouchableOpacity>
          }
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    alignItems : 'center'
  },
  background: {
    flex:1,
    flexDirection: 'row', 
    justifyContent:'space-around', 
    alignItems: 'center',  
    //alignItems: 'center',
  },
  textMoney: {
    fontFamily: FontFamily.SFUIMedium,
    fontSize: scale(16),
    color: "#222f5c"
  },
  textNote: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: scale(12),
    color: "#878b9a"
  },
  textDay: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: scale(13),
    color: "#878b9a"
  },
  textDate: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: scale(12),
    color: "white",    
    textAlign:'center',  
  },
  bkgDate: {
    width: 47,
    height: 20,
    borderRadius: 9.5,
    backgroundColor: "#6283a3",
    justifyContent: 'center', 
    alignItems: 'center',   
    overflow: 'hidden',
  }
});