import { connect } from 'react-redux'
import HistoryTabSuccessScreen from './HistoryTabSuccessScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    getHistorys,
    getHistoryDetail,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        Username: state.user.Username,
        TokenID: state.user.TokenID,
        Cardnumber: state.cards.CardActive.card_number,
        isLoading: state.progressHud.isLoading,
        transactions: state.history.transactions,
        Cards: state.cards.Cards,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    getHistoryDetail: (username, tokenid, cardnumber, transref) => dispatch(getHistoryDetail(username, tokenid, cardnumber, transref)),
    getHistorys: (username, tokenid, cardnumber) => dispatch(getHistorys(username, tokenid, cardnumber)),
})

const HistoryTabSuccessComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( HistoryTabSuccessScreen )

export default HistoryTabSuccessComponent