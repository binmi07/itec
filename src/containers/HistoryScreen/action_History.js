import * as types from '../../constants/ActionTypes';
import {
  Alert,
  AlertIOS,
  Platform,
  AsyncStorage
} from 'react-native';
var {
  getHistorys,
  getHistoryDetail,
} = require('../../services/ITECApi');
import * as MesTypes from '../../constants/MesTypes';
import {
  errorProcess
} from '../../components/GlobalVarContainer';

export function getHistorys(username, tokenid, cardnumber){
  console.log('ACTION-getHistorys');
  return async (dispatch) => {
    dispatch({ type: types.GET_HISTORYS });
    try{
      let response = await getHistorys(username, tokenid, cardnumber);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({ 
          type: types.GET_HISTORYS_SUCCESS,
          payload: { _response }
        });
      }else{  
        dispatch({ type: types.GET_HISTORYS_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
        // switch(response.response_status){
        //   case 500:
        //     dispatch({ type: types.GO_MAIN }); break;
        //   case 1000:
        //     Alert.alert(
        //       'Thông báo',
        //       MesTypes.ERROR_NETWORK,
        //       [ {text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false }); break;
        //   case 410:
        //     Alert.alert(
        //       'Thông báo',
        //       _response.reason,
        //       [ {text: 'OK', onPress: () => dispatch({ type: types.GO_SPLASH})},],{ cancelable: false }); break;
        //   default:
        //     Alert.alert(
        //       'Thông báo',
        //       _response.reason,
        //       [ {text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false });break;
        // }
      }      
    }catch(error){  }
  }
}

export function getHistoryDetail(username, tokenid, cardnumber, transref){
  console.log('ACTION-getHistorys');
  return async (dispatch) => {
    dispatch({ type: types.GET_HISTORY_DETAIL });
    try{
      let response = await getHistoryDetail(username, tokenid, cardnumber, transref);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({ 
          type: types.GET_HISTORY_DETAIL_SUCCESS,
          payload: { _response }
        });
        dispatch({type: types.GO_HISTORYDETAIL});
      }else{  
        dispatch({ type: types.GET_HISTORY_DETAIL_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
        // switch(response.response_status){
        //   case 500:
        //     dispatch({ type: types.GO_MAIN }); break;
        //   case 1000:
        //     Alert.alert(
        //       'Thông báo',
        //       MesTypes.ERROR_NETWORK,
        //       [ {text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false }); break;
        //   case 410:
        //     Alert.alert(
        //       'Thông báo',
        //       _response.reason,
        //       [ {text: 'OK', onPress: () => dispatch({ type: types.GO_SPLASH})},],{ cancelable: false }); break;
        //   default:
        //     Alert.alert(
        //       'Thông báo',
        //       _response.reason,
        //       [ {text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false }); break;
        // }
      }      
    }catch(error){  }
  }
}


