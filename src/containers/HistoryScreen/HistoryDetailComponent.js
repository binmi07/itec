import { connect } from 'react-redux'
import HistoryDetailScreen from './HistoryDetailScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    getHistoryDetail,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        transaction_detail : state.history.transaction_detail,
        Username: state.user.Username,
        TokenID: state.user.TokenID,
        Cardnumber: state.cards.CardActive.card_number,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    getHistoryDetail: (username, tokenid, cardnumber, transref) => dispatch(getHistoryDetail(username, tokenid, cardnumber, transref)),
})

const HistoryDetailComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( HistoryDetailScreen )

export default HistoryDetailComponent