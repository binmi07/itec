import React from 'react';
import { ScrollView, StyleSheet, View, Text } from 'react-native';
import {TabNavigator, TabBarTop} from 'react-navigation';
import {Dimensions} from 'react-native'
import HistoryTabAllComponent from './HistoryTabAllComponent';
import HistoryTabSuccessComponent from './HistoryTabSuccessComponent';
import * as FontFamily from '../../constants/FontFamily';
import {moderateScale} from '../../components/ScaleRes';

const indicatorStyle = (props, alignSelf) => ({
	backgroundColor: props.activeTintColor,
	alignSelf: 'flex-end',
});

var {height, width} = Dimensions.get('window');
var tagCount = 2;
let TabNavigatorConfig = {
    tabBarComponent: (props) => <TabBarTop {...props} indicatorStyle={indicatorStyle(props, 'flex-end')} />,
    tabBarPosition: 'top',
    lazy: true,    
    tabBarOptions: {
        activeTintColor: 'white',
        inactiveTintColor: '#bdcef9',
        inactiveBackgroundColor: '#353539',
        activeBackgroundColor: '#353539',
        showIcon: false,
        indicatorStyle: {
            borderBottomColor: '#ffffff',
            borderBottomWidth: 2,
            // alignSelf: 'flex-end',
        },
        tabStyle: {
            backgroundColor: '#1b3882',
            justifyContent: 'center',
            alignItems: 'center',
        },
        labelStyle: {
            fontFamily: FontFamily.SFUIMedium,
            justifyContent: 'center',
            alignItems: 'center',
            fontSize: moderateScale(14),
        },  
        style: {
            backgroundColor: '#1b3882',
        },
        allowFontScaling: false,
    }
}

const HistoryTabScreen = TabNavigator({
    History_All: {screen: HistoryTabAllComponent,
        navigationOptions: {
            tabBarLabel: 'Tất cả giao dịch',}
    },
    History_Success: {screen: HistoryTabSuccessComponent,
        navigationOptions: {
            tabBarLabel: 'Thành công',}
    },
}, TabNavigatorConfig);

export default HistoryTabScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});

