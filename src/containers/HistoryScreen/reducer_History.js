import * as types from '../../constants/ActionTypes';
import update from 'immutability-helper';
import {AsyncStorage} from 'react-native';

const initialState = {
    username: '01686476291',
    card_number: '0104300002',
    transactions:[ 
        // { 
        //     ref_number: '351000007',
        //     date: '17/12/2017 13:53',
        //     amount: '800000',
        //     fee: 0,
        //     channel: 'Mobile',
        //     description: 'Khoản thu tháng ',
        //     status: 0 
        // },
    ],
    transaction_detail: { 
        bill_number: '043171100002',
        bill_cycle: '11/2017',
        from_date: '01/11/2017',
        to_date: '30/11/2017',
        total_amount: 800000,
        total_fee: 800000,
        total_deduction: 0,
        expired_date: '10/12/2017',
        status: 1,
        item_details: 
        [ 
            { id: 3, item: 'B├ín tr├║', amount: 110000, show_receipt: 1 },
            { id: 1, item: 'Hß╗ìc ph├¡', amount: 100000, show_receipt: 1 },
            { id: 4, item: 'Tiß╗ün ─ân b├ín tr├║', amount: 440000, show_receipt: 1 },
            { id: 5, item: 'Hß╗ìc ph├¡ hß╗ìc 2 buß╗òi',amount: 150000, show_receipt: 1 } 
        ] 
    }
};

function storeUserInfo(type, data){
    switch(type){
        case 'username':
            AsyncStorage.setItem('username', data); break;
        case 'cardidactivate':
            AsyncStorage.setItem('cardidactivate', data); break;
    }    
}


export default function history(state = initialState, action) {
    switch (action.type) {
        // case types.GET_HISTORYS:
        //     return {
        //         ...state,
        //         transactions: initialState.transactions,
        //     }
        case types.GET_HISTORYS_SUCCESS:
            return {
                ...state,
                card_number: action.payload._response.card_number,
                transactions: action.payload._response.transactions,
            }
        case types.GET_HISTORY_DETAIL_SUCCESS:
            return {
                ...state,
                transaction_detail: action.payload._response.transaction_detail,
            }
        default:
            return state;
    }
}