import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Text, Platform, Dimensions } from 'react-native';
import HistoryTabScreen from './HistoryTabScreen';
import TitleInfoComponent from '../TitleInfoScreen/TitleInfoComponent';
import * as FontFamily from '../../constants/FontFamily';
import {Grid, Row, Col} from 'react-native-easy-grid';
import {
  getCardNoFromStudentCode,
} from '../../components/GlobalVarContainer';
const WIDTH = Dimensions.get('window').width;

export default class HistoryScreen extends Component{  
  componentWillMount(){
    this.props.navigation.setParams({
      tabActive: this._tabActive,
    });
  }

  _tabActive = () => {
                      this.props.getHistorys(this.props.Username, 
                                            this.props.TokenID, 
                                            this.props.Cardnumber);
  }

  updateList(studencode){
    this.props.getHistorys(this.props.Username, 
                            this.props.TokenID,
                            getCardNoFromStudentCode(this.props.Cards,studencode));
  }

  render(){
    return(
      <View style = {{flex: 1}}>
        <TitleInfoComponent 
          onPress = {() => {this.props.navigation.navigate('ChooseCard', {onRefresh: this.updateList.bind(this)})}}
          includeStatusBar = {true}/>
        
        <View style = {{flex: 1}}>
          <HistoryTabScreen/>
        </View>
      </View>
    );
  }
}