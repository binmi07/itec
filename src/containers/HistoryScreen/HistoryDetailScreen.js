import React, { Component } from 'react';
import { 
    Text, 
    TouchableOpacity, 
    StyleSheet, 
    Image,
    View, 
    Dimensions, 
    Button, 
    Platform
} from 'react-native';
import * as FontFamily from '../../constants/FontFamily';
import {Grid, Row, Col} from 'react-native-easy-grid';
import MainButton from '../../components/MainButton';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import IconClose from '../../components/IconClose';
import Avatar from '../../components/Avatar';
import AndroidBackButton from 'android-back-button'
import IconBin from '../../components/IconBin';
import {
    numberWithCommas,
} from '../../components/GlobalVarContainer'

export default class NotiDetailScreen extends Component {
    constructor(props){
        super(props);
    }

    componentWillMount(){
        // this.props.getHistoryDetail(this.props.Username, 
        //                                 this.props.TokenID,  
        //                                 this.props.Cardnumber, 
        //                                 this.props.navigation.state.params.ref_number);
    }

    lapsList(row) {
        let i = 0;
        return row.map((data) => {
        i++;
        return (
            <View style = {{flexDirection:'row', justifyContent: 'space-between', width: WIDTH * 0.9, height: 30}} key = {i}>
                <Text style = {styles.txtHeader}>{data.item}</Text>
                <Text style = {styles.txtMoney}>{numberWithCommas(data.amount)}</Text>
            </View>
        )
        })
    }

    render() {
        return (
            <View style={{position: 'absolute',bottom: 0}}>
                <AndroidBackButton
                    onPress={() => {
                        this.props.navigation.goBack();
                        return true;
                    }}
                />
                <View style = {styles.header}>
                    <Grid>
                        <Col size = {1}>
                        <TouchableOpacity onPress = {() => {this.props.navigation.goBack()}}>
                            <IconClose/>
                            {/* <Image source= {require('../../components/img/icoClose.png')}></Image> */}
                        </TouchableOpacity>
                        </Col>
                        <Col size = {5} style = {{alignItems:'center', justifyContent: 'center'}}>
                        <Text style = {styles.title}>CHI TIẾT GIAO DỊCH</Text>
                        </Col>
                        <Col size = {1}>
                            
                        </Col>
                    </Grid>
                </View>        
                <View style = {styles.body}>
                    <View style ={{marginTop: 20}}/>
                    <View>
                        {this.lapsList(this.props.transaction_detail.item_details)}
                    </View>
                    <View style ={{marginTop: 10}}/>
                    <View style = {{width: 335, height: 2, borderStyle: "solid", borderWidth: 1, borderColor: "#d8e9ee"}}/>
                    <View style ={{marginTop: 20}}/>
                    <View style = {{flexDirection:'row', justifyContent: 'space-between', width: WIDTH * 0.9, height: 30}}>
                        <Text style = {styles.txtSumHeader}>TỔNG</Text>
                        <Text style = {styles.txtSumMoney}>{numberWithCommas(this.props.transaction_detail.total_amount)}</Text>
                    </View>
                    {/* <Grid>
                        <Col>
                            <Text style = {styles.txtSumHeader}>TỔNG</Text>
                        </Col>
                        <Col>
                            <Text style = {styles.txtSumMoney}>{numberWithCommas(this.props.transaction_detail.total_amount)}</Text>
                        </Col>
                    </Grid> */}
                    <View style ={{marginTop: 20}}/>
                </View>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    txtSumHeader: {
        fontFamily: FontFamily.SFUISemibold,
        fontSize: 16,
        letterSpacing: 0.17,
        textAlign: "left",
        color: "#8d919f"
    },
    txtSumMoney: {
        fontFamily: FontFamily.SFUISemibold,
        fontSize: 16,
        letterSpacing: 0.17,
        textAlign: "right",
        color: "#222f5c"
    },
    txtMoney: {
        // marginRight: (width - (moderateScale(335))) / 2,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 14,
        letterSpacing: 0.15,
        textAlign: "right",
        color: "#222f5c"
    },
    txtHeader: {
        // marginLeft: (width - (moderateScale(335))) / 2,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 14,
        letterSpacing: 0.15,
        textAlign: "left",
        color: "#8d919f"
    },
    txtDetail: {
        marginTop: 12,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 12,
        lineHeight: 18,
        letterSpacing: 0.13,
        textAlign: "left",
        color: "#878b9a"
    },
    txtTime: {
        marginTop: 8,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 12,
        letterSpacing: 0.13,
        textAlign: "left",
        color: "#2061b3"
    },
    txtTitle: {
        fontFamily: FontFamily.SFUIMedium,
        fontSize: 16,
        letterSpacing: 0.17,
        textAlign: "left",
        color: "#222f5c"
    },
    container: {
        alignItems : 'center'
    },
    header: {
        flex:1,
        flexDirection: 'row',
        height: 50,
        width: WIDTH,
        backgroundColor: "#00a6ec",
    },
    title: {
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 18,
        letterSpacing: 0.19,
        textAlign: "center",
        color: 'white'
    },
    body: {
        width: WIDTH,
        //height: 440,
        backgroundColor: 'white',
        alignItems: 'center'
    },
});