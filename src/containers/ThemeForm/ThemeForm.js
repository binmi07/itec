import React, { Component } from 'react';
import {Grid, Col, Row} from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import Swiper from 'react-native-swiper';
import md5 from "react-native-md5"; 
import {Platform, Dimensions, View} from 'react-native';

const { width } = Dimensions.get('window')
const API_ROOT = 'http://125.212.207.225:8080/apigateway/services/';
const AUTH_KEY = "A308975422DBA81BCD99D6FE475159CD";

export default class ThemeForm extends Component {
  constructor(props){
    super(props);
    
  }

  callApi(endpoint, apiname, key, method){
    console.log('API-Call');
    // console.log(encodeURI(endpoint));
    //var params = "User=" + JSON.stringify(data) + ",AuthData=" + md5.hex_md5(JSON.stringify(data) + key);
    
    return fetch(API_ROOT + apiname + '?' + encodeURI(endpoint),{
      method: method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'AuthKey': key,
      },
    })
    // .then(response => response.json())
    // .then(responseJson => {
    //   console.log(responseJson);
    //   return responseJson;
    // });
    .then(response => {
      if(response.status === 200){
        return response._bodyText;
      }
      return response._bodyText;
    })
  }

  getUser(username) {
    console.log('API-getUser');
    const data =  { 
      Username:username,
      Device:{  
         DeviceID:"123456789123",
         Name:"iphone 5S",
         Model:"iphone 5S",
         IMEI:"3587123456",
         ICCID:"898412365478",
         MEID:"3587123456",
         Firmware:"8.01.00"
      },
      OS:{  
         Name:Platform.OS,
         Version:Platform.Version,
      }
    };
    return this.callApi("User=" + JSON.stringify(data) + "&AuthData=" + md5.hex_md5(JSON.stringify(data) + AUTH_KEY),
                   'getUser', 
                   AUTH_KEY, 
                   'GET');
  }

  componentDidMount () {
    return async (dispatch) => {
      try{
        let response = await this.getUser('0936381333');
        console.log(response);
      }catch(error){  
        console.log(error);
      }
    }
  }

  render() {
    return (     
      <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Switch 
                thumbTintColor = 'red' 
                onTintColor = 'blue'
                tintColor = 'yellow' />
      </View>
    );
  }
}

const styles = {
    container: {
      flex: 1
    },
  
    wrapper: {
      borderRadius: 10
    },
  
    slide: {
      flex: 1 ,
      justifyContent: 'center',
      backgroundColor: 'transparent'
    },
  
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB'
    },
  
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#97CAE5'
    },
  
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#92BBD9'
    },
  
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold'
    },
  
    image: {
      borderRadius: 4,
      width: width - 20,
      height: 100,
    }
  }