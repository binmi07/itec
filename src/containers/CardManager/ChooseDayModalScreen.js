import React, { Component } from 'react';
import { TouchableOpacity, StyleSheet, Image, Modal, View, Dimensions, Button, Text } from 'react-native';
import * as FontFamily from '../../constants/FontFamily';
import {Grid, Row, Col} from 'react-native-easy-grid';
import MainButton from '../../components/MainButton';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import IconClose from '../../components/IconClose';
import AndroidBackButton from 'android-back-button';

const row1 = [1,8,15,22,29];
const row2 = [2,9,16,23,30];
const row3 = [3,10,17,24,31];
const row4 = [4,11,18,25];
const row5 = [5,12,19,26];
const row6 = [6,13,20,27];
const row7 = [7,14,21,28];

export default class ChooseDayModalScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      chooseDay: this.props.navigation.state.params.Payday,
    }
  }

  onPressAccept(){
    // this.props.updateAutoPaymentDay(this.state.chooseDay);
    this.props.navigation.state.params.updatePayday(this.state.chooseDay);
    this.props.navigation.goBack();
  }

  onPressChooseDay(day){
    this.setState({chooseDay:day});
  }

  lapsList(row) {
    let i = 0;
    return row.map((data) => {
      i++;
      return (
        <TouchableOpacity onPress = {() => {this.onPressChooseDay(data)}} key = {i}>
          <View style = {this.state.chooseDay === data ? styles.borderDateActive
                                                      : styles.borderDate}>
            <Text style = {this.state.chooseDay === data ? styles.dateActive
                                                        : styles.date}>
              {data}
            </Text>
          </View>
        </TouchableOpacity>
      )
    })
  }

  render() {
    return (
      <View style={{position: 'absolute',bottom: 0}}>
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <View style = {styles.header}>
          <Grid>
            <Col size = {1}>
              <TouchableOpacity onPress = {() => {this.props.navigation.goBack()}}>
                <IconClose/>
                {/* <Image source= {require('../../components/img/icoClose.png')}></Image> */}
              </TouchableOpacity>
            </Col>
            <Col size = {5} style = {{alignItems:'center', justifyContent: 'center'}}>
              <Text style = {styles.title}>CHỌN NGÀY</Text>
            </Col>
            <Col size = {1}></Col>
          </Grid>
        </View>        
        <View style = {styles.body}>
          <View style = {styles.formDay}>
            <View style = {{alignItems: 'center', marginTop: -20}}>
              {this.lapsList(row1)}
            </View>
            <View style = {{alignItems: 'center', marginTop: -20}}>
              {this.lapsList(row2)}
            </View>
            <View style = {{alignItems: 'center', marginTop: -20}}>
              {this.lapsList(row3)}
            </View>
            <View style = {{alignItems: 'center', marginTop: -20}}>
              {this.lapsList(row4)}
            </View>
            <View style = {{alignItems: 'center', marginTop: -20}}>
              {this.lapsList(row5)}
            </View>
            <View style = {{alignItems: 'center', marginTop: -20}}>
              {this.lapsList(row6)}
            </View>
            <View style = {{alignItems: 'center', marginTop: -20}}>
              {this.lapsList(row7)}
            </View>
          </View>
          <Text style = {styles.note}>Lưu ý: Với những tháng không có ngày 31 hệ thống sẽ chọn mặc định là ngày cuối cùng của tháng</Text>
          <MainButton 
            style = {styles.buttonAccept}
            text = 'XÁC NHẬN'
            width = {WIDTH * 0.90}
            height = {WIDTH * 0.90 / 7}
            textSize = {moderateScale(18)}
            backgroundColor = '#2b58b7'
            onPress = {() => {this.onPressAccept()}} 
          />
          <View style = {{height: WIDTH * 0.05}}/>
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    alignItems : 'center'
  },
  header: {
    flex:1,
    flexDirection: 'row',
    height: 50,
    width: WIDTH,
    backgroundColor: "#00a6ec",
  },
  title: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 18,
    letterSpacing: 0.19,
    textAlign: "center",
    color: 'white'
  },
  body: {
    width: WIDTH,
    //height: 440,
    backgroundColor: 'white',
    alignItems: 'center'
  },
  buttonAccept: {
    //position: 'absolute',
    //bottom: 20,
    marginTop: 10,
    //paddingBottom: 20,
  },
  textButtonAccept: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 18,
    letterSpacing: 0.19,
    textAlign: "center",
    color: 'white'
  },
  note: {
    marginTop: 11,
    width: WIDTH * 0.9,
    // height: 48.7,
    opacity: 0.8,
    fontFamily: FontFamily.SFUIMedium,
    fontSize: 13,
    lineHeight: 20,
    letterSpacing: 0.14,
    textAlign: "left",
    color: "#878b9a"
  },
  formDay: {
    marginTop: 20,
    width: WIDTH - 40,
    // height: Dimensions.get('window').width - 120,
    borderRadius: 7,
    backgroundColor: 'white',
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#edf3f4",
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  date: {
    fontFamily: FontFamily.SFUIBold,
    fontSize: 14,
    textAlign: "center",
    color: "#8ba0b5",
  },
  dateActive: {
    fontFamily: FontFamily.SFUIBold,
    fontSize: 14,
    textAlign: "center",
    color: "white",
  },
  borderDate: {
    width: 30,
    height: 30,
    borderRadius: 6,
    backgroundColor: "white",
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  borderDateActive: {
    width: 30,
    height: 30,
    borderRadius: 6,
    backgroundColor: "#2dc7ff",
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
});