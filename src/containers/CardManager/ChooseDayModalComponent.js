import { connect } from 'react-redux'
import ChooseDayModalScreen from './ChooseDayModalScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    updateAutoPaymentDay,
} from '../../actions'

const mapStateToProps = ( state, ownProps ) => {
    return {
        // PayDay: state.user.PayDay
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    updateAutoPaymentDay: (PayDay) => dispatch(updateAutoPaymentDay(PayDay)),
})

const ChooseDayModalCompoment = connect(
    mapStateToProps,
    mapDispatchToProps
)( ChooseDayModalScreen )

export default ChooseDayModalCompoment