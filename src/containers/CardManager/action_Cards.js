import * as types from '../../constants/ActionTypes';
import {
  Alert,
  AlertIOS,
  Platform,
  AsyncStorage
} from 'react-native';
import * as MesTypes from '../../constants/MesTypes';
var { 
    setAutopayment,
} = require('../../services/ITECApi');
import {
  errorProcess
} from '../../components/GlobalVarContainer';

export function setAutopayment(username, cardnumber, tokenid, method, scheduletype, paymentdate){
  console.log('ACTION-setAutoPayment');
  return async (dispatch) => {
    dispatch({ type: types.SET_AUTOPAYMENT });
    try{
      let response = await setAutopayment(username, cardnumber, tokenid, method, scheduletype, paymentdate);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({ 
          type: types.SET_AUTOPAYMENT_SUCCESS,
          payload: { _response }
        });
        Alert.alert(
          'Thông báo',
          _response.reason,
          [{text: 'OK', onPress: () => console.log('OK Pressed')},],
          { cancelable: false }
        )
      }else{  
        dispatch({ type: types.SET_AUTOPAYMENT_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }      
    }catch(error){  }
  }
}

export function updateCardActive(CardId){
  return {
    type: types.UPDATE_CARD_ACTIVE,
    payload: { CardId },
  };
}

export function updateAutoPaymentDay(PayDay){
  return{
    type: types.UPDATE_AUTO_PAYMENT_DAY,
    payload: { PayDay },
  }
}

export function updateCardChoose(CardChoose){
  return {
    type: types.UPDATE_CARD_CHOOSE,
    payload: { CardChoose },
  }
}


