import React, { Component } from 'react';
import { 
  Image,
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  BackHandler,
  Dimensions,
} from 'react-native';
import {Grid, Col, Row} from 'react-native-easy-grid';
import Avatar from '../../components/Avatar';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import * as FontFamily from '../../constants/FontFamily'; 
import IconBack from '../../components/IconBack';
import IconGo from '../../components/IconGo';
import Header from '../../components/Header';
import AndroidBackButton from 'android-back-button';

export default class CardManagerScreen extends Component {
  constructor(props){
    super(props);
  }

  onPress_Nav_CardInfo(go){
    this.props.updateCardChoose(CardChoose);
  }

  onPress_Nav_AutoPayment(CardChoose){
    this.props.updateCardChoose(CardChoose);
  }

  onPressTest(){
  }

  convertNumberToString(no){
    return no.toString().length === 2 ? no.toString() : '0' + no.toString();
  }

  lapsList(row) {
    let i = 0;
    return row.map((data) => {
      i++;
      return (
        <View key={i} style = {{marginTop: i > 1 ? 30 : 0}}>
          <View style = {{height: 66, backgroundColor: '#0597e6'}}>
            <Grid>
              <Col size = {15} style = {{justifyContent: 'center'}}>
                <Text style = {styles.txtStt}>{this.convertNumberToString(i)}</Text>
              </Col>
              <Col size = {15} style = {{justifyContent: 'center'}}>
                <Avatar 
                  width = {moderateScale(45)}
                  height = {moderateScale(45)}
                  url = {{uri:data.url_image}}
                />
              </Col>
              <Col size = {70} style = {{justifyContent: 'center'}}>
                <Text style = {styles.txtName}>{data.student_name}</Text>
                <Text style = {styles.txtNote} numberOfLines ={1}>{data.class_name} - {data.school_name}</Text>
              </Col>
            </Grid>
          </View>
          <View style = {{borderColor: '#dfecf1', borderBottomWidth: 1, height: 75, backgroundColor: 'white'}}>
            <Grid>
              <Col size={5}/>
              <Col size={45} style = {{justifyContent: 'center'}}>
                <Text style = {styles.txtCardTitle}>Mã thẻ ITEC:</Text>
                <Text style = {[styles.txtCardTitle, {marginTop: 5}]}>Số dư trong thẻ:</Text>
              </Col>
              <Col size={45} style = {{justifyContent: 'center', alignItems: 'flex-end'}}>
                <Text style = {[styles.txtCardInfo, {color: '#222f5c'}]} numberOfLines ={1}>{data.card_number}</Text>
                <Text style = {[styles.txtCardInfo, {marginTop: 5}]} numberOfLines ={1}> </Text>
              </Col>
              <Col size={5}/>
            </Grid>
          </View>
          <TouchableOpacity 
            style = {{borderColor: '#dfecf1', borderBottomWidth: 1, height: 60, backgroundColor: 'white'}}
            onPress = {() => {this.props.navigation.navigate('CardInfo', {Card: data})}}>
            <Grid>
              <Col size={5}/>
              <Col size = {15} style = {{justifyContent: 'center'}}>
                <Image style ={{width: 38, height: 38}} source = {require('./img/icoCardSetting.png')}/>
              </Col>
              <Col size = {70} style = {{justifyContent: 'center'}}>
                <Text style = {styles.txtSetting}>Thông tin chi tiết</Text>
              </Col>
              <Col size = {5} style = {{justifyContent: 'center', alignItems: 'flex-end'}}>
                <IconGo/>
              </Col>
              <Col size={5}/>
            </Grid>
          </TouchableOpacity>
          {
            this.props.isItec === true
            ? undefined
            : <TouchableOpacity 
                style = {{borderColor: '#dfecf1', borderBottomWidth: 1, height: 60, backgroundColor: 'white'}}
                onPress = {() => {this.props.navigation.navigate('AutoPayment', {card: data})}}>
                <Grid>
                  <Col size={5}/>
                  <Col size = {15} style = {{justifyContent: 'center'}}>
                    <Image style ={{width: 38, height: 38}} source = {require('./img/icoAutopay.png')}/>
                  </Col>
                  <Col size = {70} style = {{justifyContent: 'center'}}>
                    <Text style = {styles.txtSetting}>Thanh toán tự động</Text>
                  </Col>
                  <Col size = {5} style = {{justifyContent: 'center', alignItems: 'flex-end'}}>
                    <IconGo/>
                  </Col>
                  <Col size={5}/>
                </Grid>
              </TouchableOpacity>
          }
        </View>
      )
    })
  }

  render() {
    return (
      <View style = {{flex:1, backgroundColor: '#f0f4f5'}}>
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <Header
          Left = 'back'
          Body = 'QUẢN LÝ THẺ ITEC'
          Right = {<View/>}
          onPress = {() => {this.props.navigation.goBack()}}
        />
        <ScrollView>
          {this.lapsList(this.props.Cards)}
        </ScrollView>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  txtStt: {
    opacity: 0.4,
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 25,
    letterSpacing: 0.26,
    textAlign: "center",
    color: "#1b3882"
  },
  txtName: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 16,
    letterSpacing: 0.17,
    textAlign: "left",
    color: 'white'
  },
  txtNote: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 11,
    letterSpacing: 0.12,
    textAlign: "left",
    color: "#d4fbff"
  },
  txtCardTitle: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 14,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "#8d919f"
  },
  txtSetting: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 16,
    letterSpacing: 0.17,
    textAlign: "left",
    color: "#878b9a"
  },
  txtCardInfo: {
    fontFamily: FontFamily.SFUIRegular,
    width: WIDTH * 0.45,
    fontSize: 14,
    letterSpacing: 0.15,
    textAlign: "right",
    color: "#222f5c"
  },
  txtHeader: {
    fontFamily: FontFamily.SFUIMedium,
    fontSize: 18,
    letterSpacing: 0.47,
    textAlign: "center",
    color: 'white'
  },
});

