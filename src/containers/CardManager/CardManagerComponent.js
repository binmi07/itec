import { connect } from 'react-redux'
import CardManagerScreen from './CardManagerScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    updateCardChoose,
} from '../../actions'

const mapStateToProps = ( state, ownProps ) => {
    return {
        Cards: state.cards.Cards,
        isItec: state.user.isItec,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    updateCardChoose: (CardChoose) => dispatch(updateCardChoose(CardChoose)),
})

const CardManagerComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( CardManagerScreen )

export default CardManagerComponent