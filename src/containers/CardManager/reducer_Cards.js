import * as types from '../../constants/ActionTypes';
import update from 'immutability-helper';
import {AsyncStorage} from 'react-native';
import {
    getCardActivate,
} from '../../components/GlobalVarContainer';

const initialState = {
    Cards: [
        // { 
        //     id: 7,
        //     card_number: '9704221711247488',
        //     card_holdername: 'HOANG MINH ANH',
        //     bank_name: 'MBBank',
        //     issue_date: '24/11/2017',
        //     student_code: '010430000000007',
        //     student_name: 'Ho├áng Minh Anh',
        //     date_of_birth: '16/08/2005',
        //     gender: null,
        //     school_name: 'Truong PTTH Lien Ha',
        //     class_code: '10A1',
        //     class_name: 'Lop 10A1',
        //     auto_payment:{ 
        //         auto_pay: 'false',
        //         registed_date: null,
        //         schedule_type: null,
        //         payment_date: null 
        //     },
        //     status: 0 
        // },
    ],
    CardIdActive: -1,
    CardActive:{
        
    }
};

function storeUserInfo(type, data){
    switch(type){
        case 'username':
            AsyncStorage.setItem('username', data); break;
        case 'cardidactivate':
            AsyncStorage.setItem('cardidactivate', data); break;
    }    
}


export default function cards(state = initialState, action) {
    switch (action.type) {
        case types.UPDATE_CARDS:
            console.log('REDUCER-UPDATE_CARDS');
            if(state.CardIdActive !== initialState.CardIdActive)                
                return {...state, 
                            Cards: action.payload.cards,
                            CardActive: getCardActivate(action.payload.cards, state.CardIdActive),
                        };
            else 
                return {...state, 
                            Cards: action.payload.cards,
                            CardIdActive: action.payload.cards[0].id,
                            CardActive: action.payload.cards[0],
                        };       
        //--------------------------------------------------
        case types.SET_AUTOPAYMENT_SUCCESS:
            console.log('REDUCER-SET_AUTOPAYMENT_SUCCESS');
            let check = state.Cards.findIndex(x => x.card_number == action.payload._response.card_number);
            let CardUpdate = update(state.Cards, {[check]: {auto_payment: {auto_pay: {$set: action.payload._response.auto_payment.auto_pay},
                                                                       schedule_type: {$set : action.payload._response.auto_payment.schedule_type},
                                                                       payment_date: {$set : action.payload._response.auto_payment.payment_date} 
                                                                      }
                                                        }
                                                  }
                                    );
            return {...state, Cards: CardUpdate};  
        //--------------------------------------------------      
        case types.UPDATE_CARD_ACTIVE:
            AsyncStorage.setItem('cardidactivate', action.payload.CardId.toString());
            let CardActive = getCardActivate(state.Cards, action.payload.CardId);
            return { 
                ...state, 
                CardIdActive: action.payload.CardId,
                CardActive: CardActive };
        case types.UPDATE_AUTO_PAYMENT_DAY:
            return { ...state, PayDay:action.payload.PayDay };
        case types.UPDATE_CARD_CHOOSE:
            return {...state, CardChoose: action.payload.CardChoose}
        default:
            return state;
    }
}