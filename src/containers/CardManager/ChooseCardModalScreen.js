import React, { Component } from 'react';
import { Text, TouchableOpacity, StyleSheet, Image, Modal, View, Dimensions, Button, ListView } from 'react-native';
import * as FontFamily from '../../constants/FontFamily';
import {Grid, Row, Col} from 'react-native-easy-grid';
import RadioButton from '../../components/RadioButton';
import IconClose from '../../components/IconClose';
import Avatar from '../../components/Avatar';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import AndroidBackButton from 'android-back-button';

export default class ChooseCardModalScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
        currentId: this.props.CardIdActive,
    }
  }

  onPressAccept(){
    
  }

  onPressChooseCard(CardId, Student_Code){
    if(CardId !== this.state.currentId) {
      this.props.updateCardActive(CardId);
      try{
        this.props.navigation.state.params.onRefresh(Student_Code);
      }
      catch(error){ }
    }
    this.props.navigation.goBack();
  }

  lapsList() {
    let i = 0;
    return this.props.Cards.map((data) => {
        i++;
        let url = require('../../../assets/images/robot-prod.png');
      return (
        <TouchableOpacity onPress = {() => {this.onPressChooseCard(data.id, data.student_code)}} key = {i}>
          <View style = {data.id === this.props.CardIdActive ? styles.bkgItemActivate 
                                                             : styles.bkgItemDefalt}>
            <Grid>
              <Col size ={20} style = {{justifyContent: 'center', alignItems: 'center'}}>
                <Avatar 
                  width = {moderateScale(45)}
                  height = {moderateScale(45)}
                  url = {{uri:data.url_image}}
                />
              </Col>
              <Col size ={67} style = {{justifyContent: 'center'}}>
                <Text style = {styles.name}>{data.student_name}</Text>
                <Text style = {styles.note} numberOfLines ={1}>{data.class_name} - {data.school_name}</Text>
              </Col>
              <Col size ={13} style = {{justifyContent: 'center'}}>
                <RadioButton active = {data.id === this.props.CardIdActive ? true : false}/>
              </Col>
            </Grid>
          </View>
        </TouchableOpacity>
      )
    })
  }

  render() {
    return (
      <View style={{position: 'absolute',bottom: 0}}>
        <AndroidBackButton
          onPress={() => {
              this.props.navigation.goBack();
              return true;
          }}
        />
        <View style = {styles.header}>
          <Grid>
            <Col size = {1}>
              <TouchableOpacity onPress = {() => {this.props.navigation.goBack()}}>
                <IconClose/>
              </TouchableOpacity>
            </Col>
            <Col size = {5} style = {{alignItems:'center', justifyContent: 'center'}}>
              <Text style = {styles.title}>DANH SÁCH THẺ</Text>
            </Col>
            <Col size = {1}></Col>
          </Grid>
        </View>
        <View style = {styles.body}>            
          {this.lapsList()}  
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  bkgItemActivate: {
    height: 65, 
    borderBottomWidth: 1, 
    borderColor: '#dfecf1',
    backgroundColor: '#f3f8fa'
  },
  bkgItemDefalt: {
    height: 65, 
    borderBottomWidth: 1, 
    borderColor: '#dfecf1',
    backgroundColor: 'white'
  },
  container: {
    alignItems : 'center'
  },
  header: {
    flex:1,
    flexDirection: 'row',
    height: 50,
    width: WIDTH,
    backgroundColor: "#00a6ec",
  },
  title: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(18),
    letterSpacing: 0.19,
    textAlign: "center",
    color: 'white'
  },
  body: {
    backgroundColor: 'white',
    //alignItems: 'center'
  },
  avatar: {
    width: 45,
    height: 45,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#adb5ba"
  },
  name: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: moderateScale(16),
    letterSpacing: 0.17,
    textAlign: "left",
    color: "#222f5c"
  },
  note: {   
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(11),
    letterSpacing: 0.12,
    textAlign: "left",
    color: "#878b9a",
    marginTop: 5,
  },
});