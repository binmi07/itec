import React, { Component } from 'react';
import { 
  Image,
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  BackHandler,
} from 'react-native';
import {Grid, Col, Row} from 'react-native-easy-grid';
import Avatar from '../../components/Avatar';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import * as FontFamily from '../../constants/FontFamily'; 
import SmallButton from '../../components/SmallButton';
import Header from '../../components/Header';
import IconBack from '../../components/IconBack';
import AndroidBackButton from 'android-back-button';
const SPACE_CARDTITLE = 8;

export default class CardInfoScreen extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <View style = {{flex:1, backgroundColor: '#f0f4f5'}}>
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <Header
          Left = 'back'
          Body = 'THÔNG TIN THẺ'
          // Right = {
          //   <SmallButton 
          //     style = {styles.button}
          //     text = 'CHUYỂN'
          //     textSize = {moderateScale(12)}
          //     textFont = {FontFamily.SFUIMedium}
          //     textColor = {'#122664'}
          //     width = {moderateScale(76)}
          //     height = {moderateScale(29)}
          //     onPress = {() => {}} 
          //   />
          // }
          onPress = {() => {this.props.navigation.goBack()}}
        />
        <ImageBackground 
            source = {require('./img/bgCardInffo.png')} 
            style = {{height: 190, alignItems: 'center', justifyContent: 'center'}}>
            <Avatar width = {90} height = {90} url = {{uri:this.props.navigation.state.params.Card.url_image}}/>
            <Text style = {styles.txtName}>{this.props.navigation.state.params.Card.student_name}</Text>            
            <Text style = {styles.txtNote}>{this.props.navigation.state.params.Card.class_name} - {this.props.navigation.state.params.Card.school_name}</Text>
        </ImageBackground>
        <ScrollView>
          <Text style = {styles.txtTitle}>Thông tin học sinh</Text>
          <View style = {{borderColor: '#dfecf1', borderBottomWidth: 1, height: 140, backgroundColor: 'white'}}>
            <Grid>
              <Col style = {{justifyContent: 'center'}}>
                <Text style = {styles.txtCardTitle}>Mã học sinh:</Text>
                <Text style = {[styles.txtCardTitle, {marginTop: SPACE_CARDTITLE}]}>Ngày sinh:</Text>
                <Text style = {[styles.txtCardTitle, {marginTop: SPACE_CARDTITLE}]}>Giới tính:</Text>
                <Text style = {[styles.txtCardTitle, {marginTop: SPACE_CARDTITLE}]}>Người giám hộ:</Text>
              </Col>
              <Col style = {{justifyContent: 'center'}}>
                <Text style = {[styles.txtCardInfo, {color: '#222f5c'}]} numberOfLines ={1}>{this.props.navigation.state.params.Card.student_code} </Text>
                <Text style = {[styles.txtCardInfo, {marginTop: SPACE_CARDTITLE}]}>{this.props.navigation.state.params.Card.date_of_birth} </Text>
                <Text style = {[styles.txtCardInfo, {marginTop: SPACE_CARDTITLE}]}>{this.props.navigation.state.params.Card.gender} </Text>
                <Text style = {[styles.txtCardInfo, {marginTop: SPACE_CARDTITLE}]}>{this.props.user.Fullname}</Text>
              </Col>
            </Grid>
          </View>
          <Text style = {styles.txtTitle}>Thông tin thẻ ITEC</Text>
          <View style = {{borderColor: '#dfecf1', borderBottomWidth: 1, height: 140, backgroundColor: 'white'}}>
            <Grid>
              <Col style = {{justifyContent: 'center'}}>
                <Text style = {styles.txtCardTitle}>Mã thẻ ITEC:</Text>
                <Text style = {[styles.txtCardTitle, {marginTop: SPACE_CARDTITLE}]}>Số dư trong thẻ:</Text>
                {/* <Text style = {[styles.txtCardTitle, {marginTop: SPACE_CARDTITLE}]}>Chủ thẻ:</Text> */}
                <Text style = {[styles.txtCardTitle, {marginTop: SPACE_CARDTITLE}]}>Ngày mở thẻ:</Text>
              </Col>
              <Col style = {{justifyContent: 'center'}}>
                <Text style = {styles.txtCardInfo} numberOfLines ={1}>{this.props.navigation.state.params.Card.card_number} </Text>
                <Text style = {[styles.txtCardInfo, {fontFamily: FontFamily.SFUIBold, color: '#ed2609', marginTop: SPACE_CARDTITLE}]}> </Text>
                {/* <Text style = {[styles.txtCardInfo, {marginTop: SPACE_CARDTITLE}]}>{this.props.user.Fullname}</Text> */}
                <Text style = {[styles.txtCardInfo, {marginTop: SPACE_CARDTITLE}]}>{this.props.navigation.state.params.Card.issue_date} </Text>
              </Col>
            </Grid>
          </View>
        </ScrollView>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  txtTitle: {
    marginTop: 10,
    marginLeft: WIDTH * 3 / 100,
    paddingBottom: 10
  },
  txtStt: {
    opacity: 0.4,
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 25,
    fontWeight: "600",
    fontStyle: "normal",
    letterSpacing: 0.26,
    textAlign: "center",
    color: "#1b3882"
  },
  txtName: {
    marginTop: 13,
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 21,
    letterSpacing: 0.22,
    textAlign: "center",
    color: 'white',
    backgroundColor: 'transparent'
  },
  txtNote: {
    marginTop: 8,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 11,
    letterSpacing: 0.12,
    textAlign: "center",
    color: "#d4fbff",
    backgroundColor: 'transparent'
  },
  txtCardTitle: {
    marginLeft: WIDTH * 6 / 100,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 14,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "#8d919f"
  },
  txtSetting: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 16,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0.17,
    textAlign: "left",
    color: "#878b9a"
  },
  txtCardInfo: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 14,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "#222f5c"
  },
  txtHeader: {
    fontFamily: FontFamily.SFUIMedium,
    fontSize: 18,
    lineHeight: 16,
    letterSpacing: 0.47,
    textAlign: "center",
    color: 'white'
  },
});

