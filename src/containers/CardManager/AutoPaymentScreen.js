import React, { Component } from 'react';
import { 
  Text, 
  TouchableOpacity, 
  Dimensions, 
  StyleSheet, 
  View, 
  Switch, 
  ActivityIndicator,
  Platform
} from 'react-native';
import {Grid, Col, Row} from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import RadioButton from '../../components/RadioButton';
import MainButton from '../../components/MainButton';
import {moderateScale, WIDTH, HEIGHT} from '../../components/ScaleRes';
import IconBack from '../../components/IconBack';
import Header from '../../components/Header';
import SmallButton from '../../components/SmallButton';
import AndroidBackButton from 'android-back-button';
import * as actionTypes from './constant';
import moment from 'moment-timezone';

export default class AutoPaymentScreen extends Component {
  
  constructor(props){
    super(props);
    
    this.onPressBack = this.onPressBack.bind(this);
    this.onPress_Accept = this.onPress_Accept.bind(this);
    this.onPress_Change = this.onPress_Change.bind(this);

    this.state = {
      FirstDay: this.props.navigation.state.params.card.auto_payment.schedule_type === actionTypes.BEGIN_OF_MONTH ? true : false,
      LastDay: this.props.navigation.state.params.card.auto_payment.schedule_type === actionTypes.END_OF_MONTH ? true : false,
      OtherDay: this.props.navigation.state.params.card.auto_payment.schedule_type === actionTypes.DATE_OF_MONTH ? true : false,
      AutoPay: (this.props.navigation.state.params.card.auto_payment.auto_pay.toLowerCase() === 'true'),
      PayDay: this.props.navigation.state.params.card.auto_payment.schedule_type !== actionTypes.DATE_OF_MONTH ? 15 : this.getDayFromString(this.props.navigation.state.params.card.auto_payment.payment_date),
    }
  }

  getDayFromString(date){
    return parseInt(date.split("-")[2]);
  }

  onUpdatePayDay(payday){
    
    this.setState({
      PayDay: payday
    })
  }

  componentWillMount(){
    if(this.state.FirstDay === this.state.LastDay === this.state.OtherDay === false) {
      this.onPress_Radio(actionTypes.BEGIN_OF_MONTH);
    }
  }

  onChangeAutoPay(){
    this.setState({
      AutoPay: !this.state.AutoPay,
    })
  }

  onPressBack(){
    this.props.onBack();
  }

  onPress_Radio(scheduletype){
    switch(scheduletype){
      case actionTypes.BEGIN_OF_MONTH:
        this.setState({
          FirstDay: true,
          LastDay: false,
          OtherDay: false,
        }); break;
      case actionTypes.END_OF_MONTH:
        this.setState({
          FirstDay: false,
          LastDay: true,
          OtherDay: false,
        }); break;
      case actionTypes.DATE_OF_MONTH:
        this.setState({
          FirstDay: false,
          LastDay: false,
          OtherDay: true,
        }); break;
    }
  }

  getDay(){
    return this.state.PayDay.toString().length === 1 ? ('0' + this.state.PayDay.toString()) : this.state.PayDay.toString();
  }

  getPayday(){
    let today = moment().tz("Asia/Ho_Chi_Minh");
    if(this.state.PayDay < today.date() + 1){
      return this.getDay() + '/' + today.add(1, 'month').format("MM/YYYY");
    }else{
      return this.getDay() + '/' + today.format("MM/YYYY");
    }
  }

  onPress_Accept(){
    console.log(this.state.PayDay);
    //this.props.navigation.goBack();
    let scheduletype;
    if(this.state.FirstDay === true) scheduletype = actionTypes.BEGIN_OF_MONTH;
    else if(this.state.LastDay === true) scheduletype = actionTypes.END_OF_MONTH;
    else scheduletype = actionTypes.DATE_OF_MONTH;

    this.props.setAutopayment(this.props.Username,
                              this.props.navigation.state.params.card.card_number,
                              this.props.TokenId,
                              this.state.AutoPay ? 'set' : 'unset',
                              scheduletype,
                              this.getDay());
  }

  onPress_Change(){

  }

  onDraw(title, checked, scheduletype){
    if(this.state.AutoPay === true)
      return(
        <TouchableOpacity 
          style = {[styles.item, {}]} 
          onPress = {() => {this.onPress_Radio(scheduletype)}}>
            <Grid>
              <Col size={4}/>
              <Col size={80} style = {{justifyContent: 'center'}}>
                <Text style = { checked === false ? styles.textRadio : styles.textRadioActivate}>{title}</Text>
              </Col>
              <Col size={12} style = {{justifyContent: 'center', alignItems: 'flex-end'}}>
                <RadioButton active = {checked}/>
              </Col>
              <Col size={4}/>
            </Grid>
        </TouchableOpacity>
      )
    else{
      return(
        <View 
          style = {[styles.item, {}]} >
            <Grid>
              <Col size={4}/>
              <Col size={80} style = {{justifyContent: 'center'}}>
                <Text style = { checked === false ? styles.textRadio : styles.textRadioActivate}>{title}</Text>
              </Col>
              <Col size={12} style = {{justifyContent: 'center', alignItems: 'flex-end'}}>
                <RadioButton active = {checked} disable = {true}/>
              </Col>
              <Col size={4}/>
            </Grid>
        </View>
      )
    }
  }

  render() {
    return (
      <View style = {{flex:1, backgroundColor:'#f0f4f5'}}>
        {
          Platform.OS === 'android' ? <AndroidBackButton
                                        onPress={() => {
                                          this.props.navigation.goBack();
                                          return true;
                                        }}
                                      />
                                    : <View/>
        }        
        <Header
          Left = 'back'
          Body = 'THANH TOÁN TỰ ĐỘNG'
          onPress = {() => {this.props.navigation.goBack()}}
        />
        <View style = {styles.bkgAutoPayment}>
          <Grid>
            <Col size = {4}/>
            <Col size = {70} style = {{justifyContent:'center'}}>
              <Text style = {styles.txtAutoPayment}>Thanh toán tự động</Text>
            </Col>
            <Col size = {22} style = {{justifyContent:'center', alignItems: 'flex-end'}}>
              <Switch value = {this.state.AutoPay} onValueChange = {() => {this.onChangeAutoPay()}}/>
            </Col>
            <Col size = {4}/>
          </Grid>
        </View>
        <Text style = {styles.title}>Chọn ngày thanh toán tự động</Text>
        <View style = {{height: 10}}></View>
        {this.onDraw('Ngày đầu tháng', this.state.FirstDay, actionTypes.BEGIN_OF_MONTH)}
        {this.onDraw('Ngày cuối tháng', this.state.LastDay, actionTypes.END_OF_MONTH)}
        {this.onDraw('Chọn ngày', this.state.OtherDay, actionTypes.DATE_OF_MONTH)}        
        <Text style = {styles.title}>Thời gian</Text>
        <View style = {[styles.item, {marginTop: 10}]}>
          <Grid>
            <Col size={4}/>
            <Col size={64} style = {{justifyContent: 'center'}}>
              <Text style = {styles.textRadio}>Ngày {this.state.PayDay} hàng tháng</Text>
            </Col>
            <Col size={28} style = {{justifyContent: 'center', alignItems: 'flex-end'}}>              
              <SmallButton 
                disable = {!(this.state.OtherDay && this.state.AutoPay)}
                text = 'Thay đổi'
                textSize = {moderateScale(13)}
                textFont = {FontFamily.SFUIRegular}
                textColor = {'white'}
                width = {moderateScale(76)}
                height = {moderateScale(29)}
                backgroundColor = '#00a6ec'
                onPress = {() => {this.props.navigation.navigate('ChooseDay',{Payday: this.state.PayDay, updatePayday: this.onUpdatePayDay.bind(this)})}} 
              />              
            </Col>
            <Col size={4}/>
          </Grid>
        </View>        
        <View style = {{position: 'absolute', bottom: WIDTH * 0.04, left: WIDTH * 0.04}}>
          <MainButton 
            disable = {!(this.state.FirstDay || this.state.LastDay || this.state.OtherDay)}
            style = {styles.buttonAccept}
            text = 'LƯU CÀI ĐẶT'
            width = {WIDTH * 0.92}
            height = {WIDTH * 0.92 / 7}
            textSize = {moderateScale(18)}
            backgroundColor = '#2b58b7'
            onPress = {() => {this.onPress_Accept()}} 
          />
          </View>
          {
            this.props.isLoading === true ? <ActivityIndicator
                                              animating = {true}
                                              size = 'large'
                                              style = {styles.loading}
                                              color = 'black'/> 
                                          : <View/>
          }
      </View>
    );
  }
}

var styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },
  txtAutoPayment: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 16,
    letterSpacing: 0.17,
    textAlign: "left",
    color: "#878b9a"
  },
  bkgAutoPayment: {
    height: 55,
    backgroundColor: 'white',
  },
  container: {
    alignItems : 'center'
  },
  background: {
    flex:1,
    flexDirection: 'row', 
    justifyContent:'space-around', 
    alignItems: 'center',  
    //alignItems: 'center',
  },
  textRadio: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 16,
    letterSpacing: 0.17,
    textAlign: "left",
    color: "#878b9a"
  },
  textRadioActivate: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 16,
    letterSpacing: 0.17,
    textAlign: "left",
    color: "#00a6ec"
  },
  title: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 14,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "#878b9a",
    marginLeft: WIDTH * 0.04,
    marginTop: 10,
  },
  firstList: {
    marginTop: 10,
  },
  buttonAccept: {    
    backgroundColor: '#2b58b7'
  },
  buttonChange: {
    width: 76,
    height: 30,
    borderRadius: 3,
    backgroundColor: "#00a6ec",
    alignItems:'center',
    //justifyContent:'center'
  },
  textButtonChange: {
    marginTop: 6,
    width: 54,
    height: 15,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 13,
    letterSpacing: 0.14,
    textAlign: "center",
    color: 'white'
  },
  input: {
    // backgroundColor : 'white', 
    textAlign: 'center',
    // height: 45,
  },
  textMainIcon: {
    fontSize: 12,
    lineHeight: 16,
    letterSpacing: 0.31,
    textAlign: "center",
    color: "#ade4ff",
    fontFamily: FontFamily.SFUIMedium,
  },
  image: {
    borderRadius: 4,
    height: 100,
  },
  item: {
    height: 55, 
    borderTopWidth: 1, 
    borderColor: '#e8eeef',
    backgroundColor: 'white'
  },
  txtHeader: {
    fontFamily: FontFamily.SFUIMedium,
    fontSize: 18,
    fontWeight: "500",
    fontStyle: "normal",
    lineHeight: 16,
    letterSpacing: 0.47,
    textAlign: "center",
    color: 'white'
  },
});