import { connect } from 'react-redux'
import ChooseCardModalScreen from './ChooseCardModalScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    updateCardActive,
    getBill,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        Cards: state.cards.Cards,
        CardIdActive: state.cards.CardActive.id,
        Username: state.user.Username,
        TokenID: state.user.TokenID,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    updateCardActive: (CardId) => dispatch(updateCardActive(CardId)),
    getBill: (username, studencode, billcycle, tokenid) => dispatch(getBill(username, studencode, billcycle, tokenid)),
})

const ChooseCardModalCompoment = connect(
    mapStateToProps,
    mapDispatchToProps
)( ChooseCardModalScreen )

export default ChooseCardModalCompoment