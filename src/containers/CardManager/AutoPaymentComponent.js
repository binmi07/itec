import { connect } from 'react-redux'
import AutoPaymentScreen from './AutoPaymentScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    setAutopayment,
} from '../../actions'

const mapStateToProps = ( state, ownProps ) => {
    return {
        // PayDay: state.user.PayDay,
        Username: state.user.Username,
        TokenId: state.user.TokenID,
        isLoading: state.progressHud.isLoading,
        
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    setAutopayment: (username, cardnumber, tokenid, method, scheduletype, paymentdate) => dispatch(setAutopayment(username, cardnumber, tokenid, method, scheduletype, paymentdate)),
})

const AutoPaymentComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( AutoPaymentScreen )

export default AutoPaymentComponent