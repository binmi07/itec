import { connect } from 'react-redux'
import CardInfoScreen from './CardInfoScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';

const mapStateToProps = ( state, ownProps ) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    // onAdd: () => {
    //     dispatch( {
    //         type: 'USER_LOGIN'
    //     } )
    // }
})

const CardInComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( CardInfoScreen )

export default CardInComponent