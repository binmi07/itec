import { connect } from 'react-redux'
import PaymentScreen from './PaymentScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    getBill,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,
        student_code: state.cards.CardActive.student_code,
        Username: state.user.Username,
        TokenID: state.user.TokenID,
        bill_data_notpay: state.payment.bill_data_notpay,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    getBill: (username, studencode, billcycle, tokenid) => dispatch(getBill(username, studencode, billcycle, tokenid)),
})

const PaymentComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( PaymentScreen )

export default PaymentComponent