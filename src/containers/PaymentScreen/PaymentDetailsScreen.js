import React from 'react';
import { 
  ScrollView, 
  StyleSheet, 
  View, 
  Text, 
  FlatList, 
  TouchableOpacity, 
  Image,
  Platform,
  Dimensions,
} from 'react-native';
import TitleInfoComponent from '../TitleInfoScreen/TitleInfoComponent';
import Header from '../../components/Header';
import AndroidBackButton from 'android-back-button'
import { Grid, Col } from 'react-native-easy-grid';
import IconGo from '../../components/IconGo';
import * as FontFamily from '../../constants/FontFamily'; 
import MainButton from '../../components/MainButton';
import {moderateScale, WIDTH, HEIGHT} from '../../components/ScaleRes';
import Avatar from '../../components/Avatar';
import {
  numberWithCommas,
} from '../../components/GlobalVarContainer';


class PaymentDetailsScreen extends React.Component {
  constructor(props){
    super(props);
    
  }

  getMonth(date){
    return date.split("/")[0];
  }

  lapsList(row) {
    let i = 0;
    let month;
    return row.map((data) => {  
      month = this.getMonth(data.bill_cycle); 
      return data.item_details.map((item) => {
        i++;        
        return(
          <View style = {{flexDirection:'row', justifyContent: 'space-between', width: moderateScale(335), height: 30}} key = {i}>
            <Text style = {styles.txtHeader}>{item.item} tháng {month}</Text>
            <Text style = {styles.txtMoney}>{numberWithCommas(item.amount)}</Text>
          </View>
        )
      })
    })
  }

  render() {    
    return (
      <View style = {styles.container}>
        <AndroidBackButton
            onPress={() => {
                console.log()
                this.props.navigation.goBack();
                return true;
            }}
        />
        <Header
          Left = 'back'
          Body = 'THANH TOÁN HỌC PHÍ'
          onPress = {() => {this.props.navigation.goBack()}}
        />
        {/* <TitleInfoComponent onPress = {() => {this.props.navigation.navigate('ChooseCard')}}/> */}
        <View style = {styles.bkgTongHoaDon}>
          <Grid>
            <Col size={5}/>
            <Col size={90} style = {{alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style = {styles.txtTongHoaDon}>TỔNG HÓA ĐƠN</Text>
              <Text style = {styles.txtTongHoaDonMoney}>{numberWithCommas(this.props.navigation.state.params.Sum)}</Text>
            </Col>
            <Col size={5}/>
          </Grid>
        </View>
        <View style = {{height: 40, justifyContent: 'center'}}>
          <Grid>
            <Col size={4}/>
            <Col size={94}>
              <Text style = {styles.txtNguonTien}>Nguồn tiền</Text>
            </Col>
          </Grid>
        </View>
        <View style = {[styles.bkgTongHoaDon]}>
          <Grid>
            <Col size={5}/>
            <Col size={90} style = {{alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between'}}>
              <Text style = {styles.txtTongHoaDon}>THẺ ITEC: 
                <Text style = {{color: '#2b58b7', 
                                fontFamily: FontFamily.SFUIBold,
                                fontWeight: "normal",
                                fontStyle: "normal",}}> {this.props.Card.card_number}
                </Text>
              </Text>
              <Text style = {[styles.txtTongHoaDonMoney,{color: '#ed2609'}]}> </Text>
            </Col>
            <Col size={5}/>
          </Grid>
        </View>
        <View style = {{height: 40, justifyContent: 'center'}}>
          <Grid>
            <Col size={4}/>
            <Col size={94}>
              <Text style = {styles.txtNguonTien}>Thông tin chi tiết</Text>
            </Col>
          </Grid>
        </View>        
        <View style = {{backgroundColor: 'white', width: WIDTH, alignItems: 'center'}}>
          <View style = {{height: 75, width: WIDTH}}>
            <Grid>
              <Col size = {20} style = {{justifyContent:'center', alignItems: 'center'}}>
                <Avatar 
                  width = {moderateScale(45)}
                  height = {moderateScale(45)}
                  // url = {require('../../../assets/images/robot-dev.png')}
                  url = {{uri:this.props.Card.url_image}}
                />
              </Col>
              <Col size = {80} style = {{justifyContent:'center'}}>
                <View>
                  <Text style = {styles.txtName}>{this.props.Card.card_holdername}</Text>
                  <Text style = {styles.txtNote}>{this.props.Card.class_name.toUpperCase()} - {this.props.Card.school_name.toUpperCase()}</Text>
                </View>
              </Col>
            </Grid>
          </View>
          <View style = {{width: 335, height: 1, borderStyle: "solid", borderWidth: 1, borderColor: "#d8e9ee"}}/>
          <ScrollView contentContainerStyle = {{flexGrow: 1}} scrollEnabled style = {{height: HEIGHT - 458, marginTop: 10}}>
            
            {this.lapsList(this.props.navigation.state.params.Items)}
          </ScrollView>
          <View style = {{width: 335, height: 1, borderStyle: "solid", borderWidth: 1, borderColor: "#d8e9ee"}}/>
          <View style = {{marginTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: moderateScale(335)}}>
            <Text style = {styles.txtSum}>TỔNG</Text>
            <Text style = {styles.txtSumMoney}>{numberWithCommas(this.props.navigation.state.params.Sum)}</Text>
          </View>
          <View style = {{height: 10}}/>
        </View>

        <View style = {styles.bkgSum}>
          <MainButton 
            text = 'TIẾP TỤC'
            width = {moderateScale(345)}
            height = {moderateScale(45)}
            textSize = {moderateScale(18)}
            backgroundColor = '#2b58b7'
            // onPress = {() => {this.onPressAccept()}} 
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({  
  txtMoney: {
    // marginRight: (width - (moderateScale(335))) / 2,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 14,
    letterSpacing: 0.15,
    textAlign: "right",
    color: "#222f5c"
  },
  txtHeader: {
    // marginLeft: (width - (moderateScale(335))) / 2,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 14,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "#8d919f"
  },
  txtName: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 16,
    letterSpacing: 0.17,
    textAlign: "left",
    color: "#222f5c"
  },
  txtNote: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 11,
    letterSpacing: 0.12,
    textAlign: "left",
    color: "#878b9a"
  },
  txtSum: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 16,
    letterSpacing: 0.17,
    textAlign: "left",
    color: "#8d919f"
  },
  txtSumMoney: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 16,
    letterSpacing: 0.17,
    textAlign: "right",
    color: "#222f5c"
  },
  txtCard: {
    fontFamily: FontFamily.SFUIBold,
    fontSize: 13,
    
    letterSpacing: 0.14,
    textAlign: "left",
    color: "#878b9a"
  },
  container: {
    flex: 1,
    backgroundColor: '#f2f7f9',
  },
  bkgTongHoaDon: {
    height: 50,
    backgroundColor: 'white',
    borderColor: '#dfecf1',
    borderBottomWidth: 1,
  },
  txtTongHoaDon: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 13,
    letterSpacing: 0.14,
    textAlign: "left",
    color: "#8d919f"
  },
  txtTongHoaDonMoney: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 22,
    letterSpacing: 0.23,
    textAlign: "right",
    color: "#222f5c"
  },
  txtNguonTien: {
    marginTop: 10,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 14,
    letterSpacing: 0.15,
    textAlign: "left",
    color: "#878b9a"
  },
  bkgCardInfo: {
    height: 70,
    backgroundColor: "#dfecf1"
  },
  bkgSum: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 75,
    opacity: 0.98,
    backgroundColor: 'white',
    ...Platform.select({
      ios: {
        overflow: 'hidden',
        shadowColor: "#1d56781c",
        shadowOffset: {
          width: 0,
          height: -4
        },
        shadowRadius: 7,
        shadowOpacity: 1
      },
      android: {
        elevation: 30,
      }
    }),
  },
});

export default PaymentDetailsScreen;