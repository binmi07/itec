import React, { Component } from 'react';
import { 
  Image,
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  TextInput,
  Keyboard,
  ActivityIndicator,
  Platform,
  Alert,
  TouchableWithoutFeedback,
  Dimensions
 } from 'react-native';
import Header from '../../components/Header';
import AndroidBackButton from 'android-back-button';
import TitleInfoComponent from '../../containers/TitleInfoScreen/TitleInfoComponent';
import IconGo from '../../components/IconGo';
import * as FontFamily from '../../constants/FontFamily'; 
import * as MesTypes from '../../constants/MesTypes';
import {Grid, Col, Row} from 'react-native-easy-grid';
import MainButton from '../../components/MainButton';
import {
  numberWithCommas,
} from '../../components/GlobalVarContainer';

import {
  moderateScale, WIDTH
} from '../../components/ScaleRes';
import {NavigationActions} from 'react-navigation';

export default class PasswordVerifyScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      refresh: false,
      password: '',
    }
  }

  componentWillMount(){
    
  }

  componentDidMount(){
    
  }

  onPressGoBackScreen(){
    this.props.navigation.goBack(this.props.prevKey);
  }

  onPressAccept(){
    // this.props.updateKey(this.props.navigation.state.key);
    // this.props.onGoPaySuccess();
    if(this.state.password.length !== 6){
      Alert.alert(
        'Thông báo',
        MesTypes.PASS_MUST_6_LETTERS,
        [ {text: 'OK', onPress: () => console.log('OK Pressed')},],
        { cancelable: false }
      )
    }else{
      Keyboard.dismiss();
      this.props.paymentVerify(this.props.Username,
                              this.props.bill_number,
                              this.props.student_code,
                              this.props.amount,
                              this.props.card_number,
                              this.props.verifymethod,
                              '',
                              this.state.password,
                              this.props.payment_ref_number,
                              this.props.TokenID,
                          )
    }
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style = {{backgroundColor: 'white', flex:1}}>
          <AndroidBackButton
            onPress={() => {
                this.onPressGoBackScreen();
                return true;
            }}
          />
          <Header
            Left = 'back'
            Body = 'XÁC NHẬN THANH TOÁN'
            onPress = {() => {this.onPressGoBackScreen()}}
          />
          <View style = {{alignItems: 'center'}}>
            <Text style= {styles.txtNote}>Nhập mã mật khẩu của bạn để hoàn tất giao dịch.</Text>
            <View style = {{height: 20}}/>
            <TextInput
              style = {styles.input}
              blurOnSubmit = {true}
              secureTextEntry 
              maxLength = {6}
              placeholder = "Mật khẩu"
              keyboardType = {Platform.OS === 'ios' ? 'phone-pad' : 'default'}
              maxLength = {6}
              underlineColorAndroid = 'transparent'
              autoCorrect={false}
              onChangeText = {(value) => this.setState({password: value})}
              value={this.state.password}
            />
            <View style = {{height: 20}}/>
            <MainButton 
              disable = {(this.state.password.length === 0)}
              text = 'XÁC NHẬN'
              width = {WIDTH * 0.9}
              height = {WIDTH * 0.9 / 7}
              textSize = {moderateScale(18)}
              backgroundColor = '#2b58b7'
              onPress = {() => {this.onPressAccept()}} 
            />
          </View>
          {
            this.props.isLoading === true ? <ActivityIndicator
                                              animating = {true}
                                              size = 'large'
                                              style = {styles.loading}
                                              color = 'black'/> 
                                          : <View/>
          }
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

var styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },
  txtNote: {
    marginTop: 20,
    fontFamily: FontFamily.SFUIRegular,
    width: WIDTH * 0.9,
    fontSize: 15,
    lineHeight: 24,
    letterSpacing: 0.16,
    textAlign: "left",
    color: "#8d919f"
  },
  input: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(16),
    textAlign: 'center',
    width: WIDTH * 0.9,
    height: WIDTH * 0.9 / 7,
    borderRadius: 4,
    backgroundColor: 'white',
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#dee5e8"
  }
})