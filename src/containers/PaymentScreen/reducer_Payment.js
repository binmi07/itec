import update from 'immutability-helper';
import * as types from '../../constants/ActionTypes';

const initialState = {
    student_code: "010430000000007",
    card_number: "9704221711247488",
    reason: '',
    bill_data: [],
    bill_data_notpay: [],
    bill_number: '043170900007',
    student_code: '010430000000007',
    amount: 800000,
    verify_method: 'password',
    payment_ref_number: '123456',
    isPaymenting: false,
}

function getMonth(date){
    return date.split("/")[0];
}

export default function payment(state = initialState, action) {
    switch (action.type) {
        case types.GET_BILL_SUCCESS:
            //create 2 array
            let bill_data_pay = [];
            let bill_data_notpay = [];
            //split to 2 array
            action.payload._response.bill_data.map((data) => {
                if(data.status === 1) bill_data_pay.push(data);
                else bill_data_notpay.push(data);
            })
            //sort 2 array
            bill_data_pay.sort(function (a, b) { return getMonth(b.bill_cycle) - getMonth(a.bill_cycle); });
            bill_data_notpay.sort(function (a, b) { return getMonth(b.bill_cycle) - getMonth(a.bill_cycle); });
            return {...state, 
                    student_code: action.payload._response.student_code, 
                    card_number: action.payload._response.card_number, 
                    reason: action.payload._response.reason, 
                    bill_data: bill_data_notpay.concat(bill_data_pay),
                    bill_data_notpay: bill_data_notpay,
                };
        case types.GET_BILL_FAILURE:            
            return {...state, 
                bill_data: initialState.bill_data,// action.payload._response.bill_data
            };
        case types.PAYMENT_REQUEST_SUCCESS:
            return {
                ...state,
                bill_number: action.payload._response.bill_number,
                student_code: action.payload._response.student_code,
                amount: action.payload._response.amount,
                verify_method: action.payload._response.verify_method,
                payment_ref_number: action.payload._response.payment_ref_number,
            };
        case types.PAYMENT_VERIFY_SUCCESS:
            let check = state.bill_data.findIndex(x => x.bill_number == action.payload._response.bill_number);
            let bill_data_update = update(state.bill_data, {[check]: {status: {$set:1}}});
            return {
                ...state,
                bill_data: bill_data_update,
            };
        default:
            return state;
    }
}