import { connect } from 'react-redux'
import PaymentDetailsScreen from './PaymentDetailsScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';

const mapStateToProps = ( state, ownProps ) => {
    return {
        user: state.user,
        Card: state.cards.CardActive,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    
})

const PaymentDetailsComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( PaymentDetailsScreen )

export default PaymentDetailsComponent