import React, { Component } from 'react';
import { 
  Image,
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  TextInput
 } from 'react-native';
 import Header from '../../components/Header';
 import AndroidBackButton from 'android-back-button';
 import TitleInfoComponent from '../../containers/TitleInfoScreen/TitleInfoComponent';
 import IconGo from '../../components/IconGo';
 import * as FontFamily from '../../constants/FontFamily'; 
 import {Grid, Col, Row} from 'react-native-easy-grid';
 import MainButton from '../../components/MainButton';
 import Avatar from '../../components/Avatar';
 import {
  numberWithCommas,
 } from '../../components/GlobalVarContainer';

 import {
    moderateScale, WIDTH
 } from '../../components/ScaleRes';

export default class PaymentSuccessScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      
    }
  }

  onPressGoHome(){
    this.props.onGoMain();
    // this.props.navigation.goBack();
  }

  onPress_Back(){
    
  }

  render() {
    return (
      <View style = {{backgroundColor: '#f0f4f5', flex: 1}}>
        <AndroidBackButton
          onPress={() => {
              this.props.navigation.goBack(this.props.prevKey);
              return true;
          }}
        />
        <Header
          Left = 'icon'
          LeftContent = {
            <TouchableOpacity onPress = {() => {this.onPressGoHome()}}>
              <Image
                style = {{width: 49, height: 49}}
                source={require('./img/icoHomeHeader.png')} />
            </TouchableOpacity>
          }
          Body = 'THANH TOÁN'
          onPress = {() => {this.props.navigation.goBack('PasswordVerify')}}
        />
        <View style = {{marginTop: 100, alignItems: 'center'}}>
          <Avatar
              height = {68}
              width = {68}
              url = {require('./img/icoSuccess.png')}
          />
          <Text style = {styles.txtSuccess}>Thanh toán thành công</Text>
          <Text style = {styles.txtContinue}>Bạn có muốn tiếp tục thanh toán</Text>
          <View style = {{height: 30}}/>
          <MainButton 
            text = 'XEM HỌC PHÍ'
            width = {moderateScale(162)}
            height = {moderateScale(45)}
            textSize = {moderateScale(18)}
            backgroundColor = '#2b58b7'
            onPress = {() => {this.props.navigation.goBack(this.props.prevKey)}} 
          />
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  txtSuccess: {
    marginTop: 30,
    fontFamily: 'Roboto',
    fontSize: 22,
    letterSpacing: 0,
    textAlign: "center",
    color: "#0597e6"
  },
  txtContinue: {
    marginTop: 7,
    fontFamily: "Roboto",
    fontSize: 14,
    letterSpacing: 0,
    textAlign: "center",
    color: "#1b3882"
  },
})