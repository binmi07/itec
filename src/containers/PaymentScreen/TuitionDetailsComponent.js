import { connect } from 'react-redux'
import TuitionDetailsScreen from './TuitionDetailsScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    paymentRequest,
    updateKey,
} from '../../actions'

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,
        Username : state.user.Username,
        TokenID : state.user.TokenID,
        CardNumber : state.payment.card_number,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    updateKey: (key) => dispatch(updateKey(key)),
    paymentRequest: (username, billnumber, studencode, amount, cardnumber, tokenid, paymentdes) => dispatch(paymentRequest(username, billnumber, studencode, amount, cardnumber, tokenid, paymentdes)),
})

const TuitionDetailsComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( TuitionDetailsScreen )

export default TuitionDetailsComponent