import { connect } from 'react-redux'
import TuitionScreen from './TuitionScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    getBill,
    paymentRequest,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,
        student_code: state.cards.CardActive.student_code,
        Username: state.user.Username,
        TokenID: state.user.TokenID,
        payment: state.payment,
        CardNumber : state.payment.card_number,
        Reason: state.payment.reason,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    getBill: (username, studencode, billcycle, tokenid) => dispatch(getBill(username, studencode, billcycle, tokenid)),
    paymentRequest: (username, billnumber, studencode, amount, cardnumber, tokenid) => dispatch(paymentRequest(username, billnumber, studencode, amount, cardnumber, tokenid)),
})

const TuitionComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( TuitionScreen )

export default TuitionComponent