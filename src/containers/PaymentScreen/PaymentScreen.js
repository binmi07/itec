import React from 'react';
import { ScrollView, StyleSheet, View, Text, FlatList, TouchableOpacity, Image, Platform } from 'react-native';
import TitleInfoComponent from '../TitleInfoScreen/TitleInfoComponent';
import Header from '../../components/Header';
import AndroidBackButton from 'android-back-button'
import { Grid, Col } from 'react-native-easy-grid';
import IconGo from '../../components/IconGo';
import IconNodata from '../../components/IconNodata';
import * as FontFamily from '../../constants/FontFamily'; 
import MainButton from '../../components/MainButton';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import {
  numberWithCommas,
} from '../../components/GlobalVarContainer';

class PaymentScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      refresh: false,
      chooseArray: [],
      chooseBill: [],
      SUM: 0,
    }
  }

  componentWillMount(){
    this.refreshList();
  }

  checkExistArray(item){
    return this.state.chooseArray.findIndex(x => x == item);
  }

  onPressItem(item){
    let result = this.checkExistArray(item.bill_number);
    let chooseArray_clone = this.state.chooseArray;
    let chooseBill_clone = this.state.chooseBill;
    let amount;
    if(result > -1) {
      chooseArray_clone.splice(result, 1);
      chooseBill_clone.splice(result, 1);
      amount = -item.total_amount;
    }
    else {
      chooseArray_clone.push(item.bill_number); 
      chooseBill_clone.push(item);
      amount = item.total_amount;
    }
    this.setState ({
      chooseArray: chooseArray_clone,
      chooseBill: chooseBill_clone,
      SUM: this.state.SUM + amount,
    })
  }

  onPressContinue(){
    this.props.navigation.navigate('PaymentDetail',{Sum: this.state.SUM,Items: this.state.chooseBill});
  }

  updateList(student_code){
    this.setState({
      chooseArray: [],
      chooseBill: [],
      SUM: 0,
    })
    this.props.getBill(this.props.Username, student_code, '', this.props.TokenID);
  }
  
  refreshList(){
    this.setState({
      chooseArray: [],
      chooseBill: [],
      SUM: 0,
    })
    //-----------------------LOAD DATA VAO MANG----------------------------
    this.props.getBill(this.props.Username, this.props.student_code, '', this.props.TokenID);
    //---------------------------------------------------------------------
  }

  getMonth(date){
    return date.split("/")[0];
  }

  render() {
    return (
      <View style = {styles.container}>
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <Header
          Left = 'back'
          Body = 'THANH TOÁN HỌC PHÍ'
          onPress = {() => {this.props.navigation.goBack()}}
        />
        <TitleInfoComponent onPress = {() => {this.props.navigation.navigate('ChooseCard', {onRefresh: this.updateList.bind(this)})}}/>
        {
          this.props.bill_data_notpay.length === 0  
          ?<View style = {{flex: 1, alignItems: 'center', marginTop: 100}}>
            <IconNodata width = {75} height = {75}/>
          </View>        
          :<FlatList 
            refreshing = {this.props.isLoading}
            onRefresh = {() => {this.refreshList()}}
            data = {this.props.bill_data_notpay}
            renderItem = {
              ({item}) =>                            
                <TouchableOpacity 
                  onPress = {() => {this.onPressItem(item)}}
                  style = {{borderColor: '#dfecf1', borderBottomWidth: 1, backgroundColor: 'white'}}>
                  <Grid style = {{height: 65}}>
                    <Col size={15} style = {{alignItems: 'center', justifyContent: 'center'}}>
                      <Image source = {this.checkExistArray(item.bill_number) === -1 ? require('./img/icoUncheck.png') : require('./img/icoChecked.png')}/>
                    </Col>
                    <Col size={32} style = {{justifyContent: 'center'}}>
                      <View style = {styles.bkgMonth}>
                        <Text style = {styles.txtMonth}>Tháng {this.getMonth(item.bill_cycle)}</Text>
                      </View>
                      <Text style = {styles.txtPay}>Hạn TT: {item.expired_date}</Text>
                    </Col>
                    <Col size={47} style = {{justifyContent: 'center'}}>
                      <Text style = {styles.textMoney}>{numberWithCommas(item.total_amount)}</Text>
                    </Col>
                    <Col size={7} style = {{justifyContent: 'center'}}>
                      <IconGo/>
                    </Col>
                  </Grid>
                </TouchableOpacity>
            }
            keyExtractor={(item, index) => index}
          />
        }
        <View style = {styles.bkgSum}>
          <Grid>
            <Col size={6} style = {{justifyContent: 'center'}}>
              <Text style = {styles.txtSum}>TỔNG</Text>
              <Text style = {styles.txtSumMoney}>{numberWithCommas(this.state.SUM)}</Text>
            </Col>
            <Col size={4} style = {{alignItems: 'center', justifyContent: 'center'}}>
              <MainButton 
                disable = {!(this.state.SUM > 0)}
                text = 'TIẾP TỤC'
                width = {moderateScale(133)}
                height = {moderateScale(45)}
                textSize = {moderateScale(18)}
                backgroundColor = '#2b58b7'
                onPress = {() => {this.onPressContinue()}} 
              />
            </Col>
          </Grid>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({  
  
  container: {
    flex: 1,
    backgroundColor: '#f2f7f9',
  },
  textMoney: {
    marginRight: 10,
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 20,
    // fontWeight: "600",
    // fontStyle: "normal",
    letterSpacing: 0.19,
    textAlign: "right",
    color: "#222f5c"
  },
  bkgMonth: {
    width: 70,
    height: 20,
    borderRadius: 4,
    backgroundColor: "#00a6ec",
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtMonth: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 12,
    letterSpacing: 0.13,
    textAlign: "center",
    color: 'white'
  },
  txtPay: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 10,
    letterSpacing: 0.11,
    textAlign: "left",
    color: "#878b9a"
  },
  txtNotPay: {
    marginTop: 7,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 10,
    letterSpacing: 0.11,
    textAlign: "left",
    color: '#ed2609'
  },
  bkgSum: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 75,
    opacity: 0.98,
    backgroundColor: 'white',
    ...Platform.select({
      ios: {
        overflow: 'hidden',
        shadowColor: "#1d56781c",
        shadowOffset: {
          width: 0,
          height: -4
        },
        shadowRadius: 7,
        shadowOpacity: 1
      },
      android: {
        elevation: 30,
      }
    }),
  },
  txtSum: {
    marginLeft: 24,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 13,
    letterSpacing: 0.14,
    textAlign: "left",
    color: "#8d919f"
  },
  txtSumMoney: {
    marginLeft: 24,
    fontFamily: FontFamily.SFUIBold,
    fontSize: 20,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0.21,
    textAlign: "left",
    color: "#222f5c"
  }
});

export default PaymentScreen;