import React, { Component } from 'react';
import { 
  Image,
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Modal,
  Dimensions,
  StatusBar
 } from 'react-native';
import Header from '../../components/Header';
import AndroidBackButton from 'android-back-button';
import TitleInfoComponent from '../TitleInfoScreen/TitleInfoComponent';
import IconGo from '../../components/IconGo';
import * as FontFamily from '../../constants/FontFamily'; 
import {Grid, Col, Row} from 'react-native-easy-grid';
import {
  numberWithCommas,
} from '../../components/GlobalVarContainer'
import IconClose from '../../components/IconClose'
import {
  moderateScale, WIDTH
} from '../../components/ScaleRes';
import MainButton from '../../components/MainButton';

export default class TuitionScreen extends Component {
  constructor(props){
    super(props);
  }

  componentWillMount(){
    this.refreshList();
  }

  onPress_TuitionDetail(bill){
    this.props.navigation.navigate('TuitionDetails',{Bill : bill, Month: this.getMonth(bill.from_date)})
    // this.props.navigation.navigate('PasswordVerify');
  }

  onPressAccept(){
    this.onPress_CloseModal(false);
    this.props.paymentRequest(this.props.Username, 
                              this.state.bill.bill_number,
                              this.state.bill.student_code,
                              this.state.bill.total_amount,
                              this.props.CardNumber,
                              this.props.TokenID);
    // this.props.navigation.navigate('PasswordVerify');
  }

  updateList(student_code){
    this.props.getBill(this.props.Username, student_code, '', this.props.TokenID);
  }
  
  refreshList(){
    //-----------------------LOAD DATA VAO MANG----------------------------
    this.props.getBill(this.props.Username, this.props.student_code, '', this.props.TokenID);
    //---------------------------------------------------------------------
  }

  getMonth(date){
    return date.split("/")[1];
  }

  lapsList(row) {
    let i = 0;
    return row.map((data) => {
      i++;
      return (
        <View style = {{flexDirection:'row', justifyContent: 'space-between', width: moderateScale(335), height: 30}} key = {i}>
            <Text style = {styles.txtHeader}>{data.item}</Text>
            <Text style = {styles.txtMoney}>{numberWithCommas(data.amount)}</Text>
        </View>
      )
    })
  }

  render() {
    return (
      <View style = {{backgroundColor: 'white', flex:1}}>
        <StatusBar
          backgroundColor = "transparent" translucent
        />
        <AndroidBackButton
          onPress={() => {
              this.props.navigation.goBack();
              return true;
          }}
        />
        <Header
          Left = 'back'
          Body = 'HỌC PHÍ'
          onPress = {() => {this.props.navigation.goBack()}}
        />
        <TitleInfoComponent onPress = {() => {this.props.navigation.navigate('ChooseCard', {onRefresh: this.updateList.bind(this)})}}/>
        {
          this.props.payment.bill_data.length === 0 
          ? <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Text style = {{fontSize: 12, width: WIDTH * 0.9}}>{this.props.Reason}</Text>
            </View>
          : <FlatList 
            refreshing = {this.props.isLoading}
            onRefresh = {() => {this.refreshList()}}
            data = {this.props.payment.bill_data}
            renderItem = {
              ({item}) => 
                <TouchableOpacity 
                  onPress = {() => {this.onPress_TuitionDetail(item)}}>
                  <View style = {{borderColor: '#dfecf1', borderBottomWidth: 1, backgroundColor: 'white'}}>
                    <Grid style = {{height: 65}}>
                      <Col size={5}/>
                      <Col size={28} style = {{justifyContent: 'center'}}>
                        <View style = {item.status === 0 ? styles.bkgMonthNotPay : styles.bkgMonthPay}>
                          <Text style = {styles.txtMonth}>{item.bill_cycle}</Text>
                        </View>
                        <Text style = {item.status === 1 ? styles.txtPay : styles.txtNotPay }>{item.status === 1 ? 'Đã thanh toán' : 'Chưa thanh toán'}</Text>
                      </Col>
                      <Col size={56} style = {{justifyContent: 'center'}}>
                        <Text style = {styles.textMoney}>{numberWithCommas(item.total_amount)}</Text>
                      </Col>
                      <Col size={5} style = {{justifyContent: 'center', alignItems: 'flex-end'}}>
                        <IconGo/>
                      </Col>
                      <Col size={6}/>
                    </Grid>
                  </View>
                </TouchableOpacity>
            }
            keyExtractor={(item, index) => index}
          />
        }
      </View>
    );
  }
}

var styles = StyleSheet.create({
  textMoney: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 20,
    letterSpacing: 0.19,
    textAlign: "right",
    color: "#222f5c"
  },
  bkgMonthPay: {
    width: 70,
    height: 20,
    opacity: 0.5,
    borderRadius: 4,
    backgroundColor: "#6283a3",
    alignItems: 'center',
    justifyContent: 'center',
  },
  bkgMonthNotPay: {
    width: 70,
    height: 20,
    borderRadius: 4,
    backgroundColor: "#00a6ec",
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtMonth: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 12,
    letterSpacing: 0.13,
    textAlign: "center",
    color: 'white'
  },
  txtPay: {
    marginTop: 7,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 10,
    letterSpacing: 0.11,
    textAlign: "left",
    color: '#8d919f'
  },
  txtNotPay: {
    marginTop: 7,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 10,
    letterSpacing: 0.11,
    textAlign: "left",
    color: '#ed2609'
  },
  header: {
    flex:1,
    flexDirection: 'row',
    height: 50,
    width: WIDTH,
    backgroundColor: "#00a6ec",
  },
  title: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 18,
    letterSpacing: 0.19,
    textAlign: "center",
    color: 'white'
  },
  body: {
    width: WIDTH,
    //height: 440,
    backgroundColor: 'white',
    alignItems: 'center'
  },
  txtMoney: {
    // marginRight: (width - (moderateScale(335))) / 2,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 14,
    letterSpacing: 0.15,
    textAlign: "right",
    color: "#222f5c"
  },
  txtHeader: {
      // marginLeft: (width - (moderateScale(335))) / 2,
      fontFamily: FontFamily.SFUIRegular,
      fontSize: 14,
      letterSpacing: 0.15,
      textAlign: "left",
      color: "#8d919f"
  },
  txtSumHeader: {
    marginLeft: (WIDTH - (moderateScale(335))) / 2,
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 16,
    fontWeight: "600",
    fontStyle: "normal",
    letterSpacing: 0.17,
    textAlign: "left",
    color: "#8d919f"
  },
  txtSumMoney: {
      marginRight: (WIDTH - (moderateScale(335))) / 2,
      fontFamily: FontFamily.SFUISemibold,
      fontSize: 16,
      fontWeight: "600",
      fontStyle: "normal",
      letterSpacing: 0.17,
      textAlign: "right",
      color: "#222f5c"
  },
})