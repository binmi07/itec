import { connect } from 'react-redux'
import OtpVerifyScreen from './OtpVerifyScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    paymentVerify,
    otpCountdown,
    resetOtpCountdown,
    changeInputOtpCode,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,

        Username: state.user.Username,
        bill_number: state.payment.bill_number,
        student_code: state.payment.student_code,
        amount: state.payment.amount,
        card_number: state.payment.card_number,
        verifymethod: state.payment.verify_method,
        payment_ref_number: state.payment.payment_ref_number,
        TokenID: state.user.TokenID,

        InputOtpCode: state.user.InputOtpCode,
        PhoneNumber: state.user.PhoneNumber,
        OtpTimeInSecond: state.user.OtpTimeInSecond,
        prevKey: state.user.prevKey,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    onGoBack: () => {
        dispatch( {
            type: types.GO_BACKSCREEN
        } )
    },
    paymentVerify : (username, billnumber, studencode, amount, cardnumber, verifymethod, otp, password, paymentrefnumber, tokenid) => dispatch(paymentVerify(username, billnumber, studencode, amount, cardnumber, verifymethod, otp, password, paymentrefnumber, tokenid)),
    otpCountdown: () => dispatch(otpCountdown()),
    resetOtpCountdown: () => dispatch(resetOtpCountdown()),
    changeInputOtpCode: (InputOtpCode) => dispatch(changeInputOtpCode(InputOtpCode)),
})

const OtpVerifyComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( OtpVerifyScreen )

export default OtpVerifyComponent