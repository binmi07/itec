import * as types from '../../constants/ActionTypes';
import {
  Alert,
  AlertIOS,
  Platform,
} from 'react-native';
import * as MesTypes from '../../constants/MesTypes';
import {
  errorProcess
} from '../../components/GlobalVarContainer';

var {
    getBill,
    paymentRequest,
    paymentVerify,
} = require('../../services/ITECApi')

export function getBill(username, studencode, billcycle, tokenid) {  
  console.log('ACTION-getBill');  
  return async (dispatch) => {
    dispatch({ type: types.GET_BILL });
    try{
      let response = await getBill(username, studencode, billcycle, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){        
        dispatch({
          type: types.GET_BILL_SUCCESS,
          payload: { _response }
        });
      }else{
        dispatch({ type: types.GET_BILL_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){  }
  }
}

export function paymentRequest(username, billnumber, studencode, amount, cardnumber, tokenid, paymentdes) {  
  console.log('ACTION-paymentRequest');  
  return async (dispatch) => {
    dispatch({ type: types.PAYMENT_REQUEST });
    try{
      let response = await paymentRequest(username, billnumber, studencode, amount, cardnumber, tokenid, paymentdes);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){        
        dispatch({
          type: types.PAYMENT_REQUEST_SUCCESS,
          payload: { _response }
        });
        if(_response.verify_method === 'otp') dispatch({ type: types.GO_OTPVERIFY});    
        else dispatch({ type: types.GO_PASSWORDVERIFY});        
      }else{
        dispatch({ type: types.PAYMENT_REQUEST_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }
    }catch(error){  }
  }
}

export function paymentVerify(username, billnumber, studencode, amount, cardnumber, verifymethod, otp, password, paymentrefnumber, tokenid) {  
  console.log('ACTION-paymentVerify');  
  return async (dispatch) => {
    dispatch({ type: types.PAYMENT_VERIFY });
    try{
      let response = await paymentVerify(username, billnumber, studencode, amount, cardnumber, verifymethod, otp, password, paymentrefnumber, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){ 
        dispatch({
          type: types.PAYMENT_VERIFY_SUCCESS,
          payload: { _response }
        });
        dispatch({ type: types.GO_PAYMENTSUCCESS});
      }else{     
        dispatch({ type: types.PAYMENT_VERIFY_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }
    }catch(error){ }
  }
}