import { connect } from 'react-redux'
import PaymentSuccessScreen from './PaymentSuccessScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';

const mapStateToProps = ( state, ownProps ) => {
    return {
        prevKey: state.user.prevKey,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    onGoMain: () => {
        dispatch( {
            type: types.GO_BACKSCREEN
        } )
    }
})

const PaymentSuccessComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( PaymentSuccessScreen )

export default PaymentSuccessComponent