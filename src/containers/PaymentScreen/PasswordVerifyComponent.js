import { connect } from 'react-redux'
import PasswordVerifyScreen from './PasswordVerifyScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    paymentVerify,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,

        Username: state.user.Username,
        bill_number: state.payment.bill_number,
        student_code: state.payment.student_code,
        amount: state.payment.amount,
        card_number: state.payment.card_number,
        verifymethod: state.payment.verify_method,
        payment_ref_number: state.payment.payment_ref_number,
        TokenID: state.user.TokenID,
        prevKey: state.user.prevKey,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    onGoPaySuccess: () => {
        dispatch( {
            type: types.GO_PAYMENTSUCCESS
        } )
    },
    paymentVerify : (username, billnumber, studencode, amount, cardnumber, verifymethod, otp, password, paymentrefnumber, tokenid) => dispatch(paymentVerify(username, billnumber, studencode, amount, cardnumber, verifymethod, otp, password, paymentrefnumber, tokenid)),
})

const PasswordVerifyComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( PasswordVerifyScreen )

export default PasswordVerifyComponent