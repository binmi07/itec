import React, { Component } from 'react';
import { 
  Image,
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  TextInput,
  Keyboard,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Dimensions
 } from 'react-native';
 import Header from '../../components/Header';
 import AndroidBackButton from 'android-back-button';
 import TitleInfoComponent from '../../containers/TitleInfoScreen/TitleInfoComponent';
 import IconGo from '../../components/IconGo';
 import * as FontFamily from '../../constants/FontFamily'; 
 import {Grid, Col, Row} from 'react-native-easy-grid';
 import MainButton from '../../components/MainButton';
 import {
  numberWithCommas,
 } from '../../components/GlobalVarContainer';

 import {
    moderateScale, WIDTH
 } from '../../components/ScaleRes';
 import {NavigationActions} from 'react-navigation';

export default class OtpVerifyScreen extends Component {
  constructor(props){
    super(props);
    this.onOTPBoxPress = this.onOTPBoxPress.bind(this);
    this.onHiddenInputChangeText = this.onHiddenInputChangeText.bind(this);
    this.state = {
      otp : ''
    }
  }

  componentWillMount(){
    
  }

  componentDidMount() {
    interval = setInterval(() => {
      this.props.otpCountdown();
    }, 1000);
  }

  onPressSendOTP(otp){
    if(otp.length === 6){
      Keyboard.dismiss();
      this.props.paymentVerify(this.props.Username,
                                this.props.bill_number,
                                this.props.student_code,
                                this.props.amount,
                                this.props.card_number,
                                this.props.verifymethod,
                                otp,
                                '',
                                this.props.payment_ref_number,
                                this.props.TokenID,
    )
    }
  }

  onHiddenInputChangeText(text) {
    let digits = text.match(/\d+/g);
    if (digits === null) {
      text = '';
    } else {
      text = digits.join([]);
    }
    this.setState({otp: text})
    // this.props.changeInputOtpCode(text);    
    if (text.length === 6) {
      Keyboard.dismiss();
      console.log('VERIFY OTP NOW bbbbb');      
      //clearInterval(interval);    
      this.onPressSendOTP(text);
    } 
  }

  onConvertTimeToString(){
    let mm = Math.floor(this.props.OtpTimeInSecond / 60).toString();
    let ss = (this.props.OtpTimeInSecond % 60).toString();
    return (mm.length === 2 ? mm : '0' + mm) + ':' + (ss.length === 2 ? ss : '0' + ss);
  }

  onOTPBoxPress() {
    console.log('HAHAHA');
    this.refs.hiddenInput.focus();
  }

  onPressGoBackScreen(){
    this.props.navigation.goBack(this.props.prevKey);
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style = {{backgroundColor: 'white', flex:1}}>
          <AndroidBackButton
            onPress={() => {
                this.onPressGoBackScreen();
                return true;
            }}
          />
          <Header
            Left = 'back'
            Body = 'XÁC NHẬN THANH TOÁN'
            onPress = {() => {this.onPressGoBackScreen()}}
          />
          <View style = {{alignItems: 'center'}}>
            <Text style = {styles.txtNote}>Mã xác nhận thanh toán đã được gửi tới số điện thoại của bạn. Nhập mã xác nhận để hoàn thất giao dịch.</Text>
            <TextInput
              ref="hiddenInput"
              style={styles.hiddenInput}
              autoFocus={true}
              keyboardType={'numeric'}
              maxLength={6}
              onChangeText={this.onHiddenInputChangeText}
              onSubmitEditing={() => { this.refs.hiddenInput.blur(); }}
              value={this.state.otp}
            />
            <TouchableWithoutFeedback onPress = {this.onOTPBoxPress}>
              <View style={styles.otpBox}>
                {[...Array(6)].map((x, i) =>
                  <View style={styles.otpCodeView} key = {i}>
                    <Text
                      ref = {'otp_' + i}
                      style={styles.otpCode}>
                      {i < this.state.otp.length ? this.state.otp[i] : ''}
                    </Text>
                  </View>
                )}
              </View>
            </TouchableWithoutFeedback>
            <Text style = {styles.txtCountDown}>{this.onConvertTimeToString()}</Text>
            <View style = {{height: 20}}/>
            <MainButton 
              //disable = {!(this.state.FirstDay || this.state.LastDay || this.state.OtherDay)}
              //style = {styles.buttonAccept}
              text = 'CẬP NHẬT'
              width = {WIDTH * 0.9}
              height = {WIDTH * 0.9 / 7}
              textSize = {moderateScale(18)}
              backgroundColor = '#2b58b7'
              onPress = {() => {this.onPressAccept()}} 
            />
          </View>
          {
            this.props.isLoading === true ? <ActivityIndicator
                                              animating = {true}
                                              size = 'large'
                                              style = {styles.loading}
                                              color = 'black'/> 
                                          : <View/>
          }
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

var styles = StyleSheet.create({
  otpBox: {
    height: moderateScale(70),
    width: WIDTH * 0.9,
    // paddingLeft: 5,
    // paddingRight: 5,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-around',
    // backgroundColor: 'black',
  },
  otpCodeView: {
    width: moderateScale(35),
    height: moderateScale(44),
    borderRadius: 4,
    backgroundColor: "#f0f4f5",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#dee5e8",
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
    paddingBottom: 5,
  },
  otpCode: {
    fontSize: moderateScale(18),
    letterSpacing: 0.17,
    textAlign: "center",
    //color: "#a5a8b3",
    fontFamily: FontFamily.SFUISemibold,
  },
  hiddenInput: {
    width: 0,
    height: 0.5,
    position: 'absolute',
    top: -20,
  },
  txtCountDown: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 18,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0.73,
    textAlign: "center",
    color: "#4c577b"
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },
  txtNote: {
    width: WIDTH * 0.9,
    marginTop: 20,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 15,
    lineHeight: 24,
    letterSpacing: 0.16,
    textAlign: "left",
    color: "#8d919f"
  },
  input: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: moderateScale(16),
    textAlign: 'left',
    width: WIDTH * 0.9,
    height: WIDTH * 0.9 / 7,
    borderRadius: 4,
    backgroundColor: 'white',
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#dee5e8"
  }
})