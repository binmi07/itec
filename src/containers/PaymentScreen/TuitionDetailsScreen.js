import React, { Component } from 'react';
import { Text, TouchableOpacity, StyleSheet, Image, Modal, View, Dimensions, Button, ActivityIndicator } from 'react-native';
import * as FontFamily from '../../constants/FontFamily';
import {Grid, Row, Col} from 'react-native-easy-grid';
import MainButton from '../../components/MainButton';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import IconClose from '../../components/IconClose';
import Avatar from '../../components/Avatar';
import AndroidBackButton from 'android-back-button'
import {
    numberWithCommas,
} from '../../components/GlobalVarContainer'

export default class TuitionDetailsScreen extends Component {
  constructor(props){
    super(props);
    console.disableYellowBox = true;
    this.state = {
      chooseDay: this.props.PayDay,
    }
  }

  onPressAccept(){
    this.props.updateKey(this.props.navigation.state.key);
    this.props.paymentRequest(this.props.Username, 
                        this.props.navigation.state.params.Bill.bill_number,
                        this.props.navigation.state.params.Bill.student_code,
                        this.props.navigation.state.params.Bill.total_amount,
                        this.props.CardNumber,
                        this.props.TokenID,
                        this.props.navigation.state.params.Bill.bill_info);    
    // this.props.navigation.navigate('PasswordVerify');
  }

  lapsList(row) {
    let i = 0;
    return row.map((data) => {
      i++;
      return (
        <View style = {{flexDirection:'row', justifyContent: 'space-between', width: WIDTH * 0.9, height: 30}} key = {i}>
            <Text style = {styles.txtHeader}>{data.item}</Text>
            <Text style = {styles.txtMoney}>{numberWithCommas(data.amount)}</Text>
        </View>
      )
    })
}

  onPressChooseDay(day){
    this.setState({chooseDay:day});
  }

  render() {
    return (
      <View style={{position: 'absolute',bottom: 0}}>
        <AndroidBackButton
            onPress={() => {
                console.log()
                this.props.navigation.goBack();
                return true;
            }}
        />
        <View style = {styles.header}>
          <Grid>
            <Col size = {1}>
              <TouchableOpacity onPress = {() => {this.props.navigation.goBack()}}>
                <IconClose/>
              </TouchableOpacity>
            </Col>
            <Col size = {5} style = {{alignItems:'center', justifyContent: 'center'}}>
              <Text style = {styles.title}>HỌC PHÍ THÁNG {this.props.navigation.state.params.Month}</Text>
            </Col>
            <Col size = {1}></Col>
          </Grid>
        </View>        
        <View style = {styles.body}>
            <View style ={{marginTop: 20}}/>
            <View>
                {this.lapsList(this.props.navigation.state.params.Bill.item_details)}
            </View>
            <View style ={{marginTop: 10}}/>
            <View style = {{width: 335, height: 2, borderStyle: "solid", borderWidth: 1, borderColor: "#d8e9ee"}}/>
            <View style ={{marginTop: 20}}/>
            <View style = {{flexDirection:'row', justifyContent: 'space-between', width: WIDTH * 0.9}}>
                <Text style = {styles.txtSumHeader}>TỔNG</Text>
                <Text style = {styles.txtSumMoney}>{numberWithCommas(this.props.navigation.state.params.Bill.total_amount)}</Text>
            </View>
            <View style ={{marginTop: 20}}/>
            <MainButton 
                disable = {this.props.navigation.state.params.Bill.status === 1}
                style = {styles.buttonAccept}
                text = {this.props.navigation.state.params.Bill.status === 1 ? 'ĐÃ THANH TOÁN' : 'THANH TOÁN HỌC PHÍ'}
                width = {WIDTH * 0.9}
                height = {WIDTH * 0.9 / 7}
                textSize = {moderateScale(18)}
                backgroundColor = '#2b58b7'
                onPress = {() => {this.onPressAccept()}} 
            />
            <View style ={{height: WIDTH * 0.05}}/>
        </View>
        {
          this.props.isLoading === true ? <ActivityIndicator
                                            animating = {true}
                                            size = 'large'
                                            style = {styles.loading}
                                            color = 'black'/> 
                                        : <View/>
        }
      </View>
    );
  }
}

var styles = StyleSheet.create({
    txtSumHeader: {
        fontFamily: FontFamily.SFUISemibold,
        fontSize: moderateScale(16),
        letterSpacing: 0.17,
        textAlign: "left",
        color: "#8d919f"
    },
    txtSumMoney: {
        fontFamily: FontFamily.SFUISemibold,
        fontSize: moderateScale(16),
        letterSpacing: 0.17,
        textAlign: "right",
        color: "#222f5c"
    },
    txtMoney: {
        fontFamily: FontFamily.SFUIRegular,
        fontSize: moderateScale(14),
        letterSpacing: 0.15,
        textAlign: "right",
        color: "#222f5c"
    },
    txtHeader: {
        fontFamily: FontFamily.SFUIRegular,
        fontSize: moderateScale(14),
        letterSpacing: 0.15,
        textAlign: "left",
        color: "#8d919f"
    },
    txtTime: {
        marginTop: 8,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: moderateScale(12),
        letterSpacing: 0.13,
        textAlign: "left",
        color: "#2061b3"
    },
    header: {
        flex:1,
        flexDirection: 'row',
        height: 50,
        width: WIDTH,
        backgroundColor: "#00a6ec",
    },
    title: {
        fontFamily: FontFamily.SFUIRegular,
        fontSize: moderateScale(18),
        letterSpacing: 0.19,
        textAlign: "center",
        color: 'white'
    },
    body: {
        width: WIDTH,
        //height: 440,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F5FCFF88',
    },
});