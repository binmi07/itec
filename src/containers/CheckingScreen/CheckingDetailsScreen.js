import React, { Component } from 'react';
import { Text, TouchableOpacity, StyleSheet, Image, Modal, View, Dimensions, Button, ScrollView } from 'react-native';
import * as FontFamily from '../../constants/FontFamily';
import {Grid, Row, Col} from 'react-native-easy-grid';
import MainButton from '../../components/MainButton';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import IconClose from '../../components/IconClose';
import Avatar from '../../components/Avatar';
import AndroidBackButton from 'android-back-button'
import IconBin from '../../components/IconBin';
import update from 'immutability-helper';
const row = [1];
var styles = StyleSheet.create({    
    container: {
        alignItems : 'center'
    },
    header: {
        flex:1,
        flexDirection: 'row',
        height: 50,
        width: WIDTH,
        backgroundColor: "#00a6ec",
    },
    title: {
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 18,
        letterSpacing: 0.19,
        textAlign: "center",
        color: 'white'
    },
    body: {
        width: WIDTH,
        //height: 440,
        backgroundColor: '#f2f7f9',
        //alignItems: 'center'
    },
    textName: {
        fontFamily: FontFamily.SFUISemibold,
        fontSize: 16,
        letterSpacing: 0.17,
        textAlign: "left",
        color: "#222f5c"
    },
    textNote: {
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 11,
        letterSpacing: 0.12,
        textAlign: "left",
        color: "#878b9a"
    },
    txtCarHeader: {
        fontFamily: FontFamily.SFUISemibold,
        fontSize: 16,
        letterSpacing: 0.17,
        textAlign: "left",
        color: "#8d919f"
    },
    txtCaContent: {
        fontFamily: FontFamily.SFUISemibold,
        fontSize: 16,
        letterSpacing: 0.17,
        textAlign: "right",
        color: "#00a6ec"
    },
    txtHeader: {
        marginTop: 13,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 14,
        letterSpacing: 0.15,
        textAlign: "left",
        color: "#8d919f"
    },
    txtContent: {
        marginTop: 13,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 14,
        letterSpacing: 0.15,
        textAlign: "right",
        color: "#222f5c"
    },
    containerItem: {
        borderTopWidth: 1, 
        borderBottomWidth: 1, 
        borderColor: "#d8e9ee", 
        backgroundColor: 'white', 
    }
});

export default class CheckingDetailScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      chooseDay: this.props.PayDay,
    }
  }

  lapsList(row) {
    let i = 0;
    return row.map((data) => {
      i++;
      return (
        <View style = {[styles.containerItem,{marginTop: i > 1 ? 20 : 0}]} key = {i}>
            <View style = {{height: 13}}/>
            <View>
                <Grid>
                    <Col size={5}></Col>
                    <Col size={35}>
                        <View style = {{flexDirection: 'row', alignItems: 'center'}}>
                            <Text style = {styles.txtCarHeader}>{data.shiftName}</Text>
                            <View style = {{width: 10, height: 10, borderRadius: 5, backgroundColor: data.color, marginLeft: 10}}/>
                        </View>
                    </Col>
                    <Col size={55}><Text style = {styles.txtCaContent}>{data.staticTime}</Text></Col>
                    <Col size={5}></Col>
                </Grid>
            </View>
            <View style = {{marginTop: 13, width: 335, height: 2, opacity: 0.5, borderStyle: "solid", borderWidth: 1, borderColor: "#d8e9ee"}}></View>
            <View>
                <Grid>
                    <Col size={5}></Col>
                    <Col size={35}>  
                        <Text style = {styles.txtHeader}>Giờ vào</Text>
                        <Text style = {styles.txtHeader}>Giờ ra</Text>
                        <Text style = {styles.txtHeader}>Tình trạng</Text>
                    </Col>
                    <Col size={55}>
                        <Text style = {styles.txtContent}>{data.inTime}</Text>
                        <Text style = {styles.txtContent}>{data.outTime}</Text>
                        <Text style = {styles.txtContent}>{data.status}</Text>
                    </Col>
                    <Col size={5}></Col>
                </Grid>
            </View>
            <View style = {{height: 13}}/>
        </View>  
      )
    })
  }

  onPressChooseDay(day){
    this.setState({chooseDay:day});
  }

  render() {
    return (
      <View style={{position: 'absolute',bottom: 0}}>
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <View style = {styles.header}>
          <Grid>
            <Col size = {1}>
              <TouchableOpacity onPress = {() => {this.props.navigation.goBack()}}>
                <IconClose/>
              </TouchableOpacity>
            </Col>
            <Col size = {5} style = {{alignItems:'center', justifyContent: 'center'}}>
              <Text style = {styles.title}>ĐIỂM DANH NGÀY {this.props.Date}</Text>
            </Col>
            <Col size = {1}>
                
            </Col>
          </Grid>
        </View>        
        <View style = {styles.body}>
            <View style = {{height: 70}}> 
                <Grid>
                    <Col size={2} style = {{justifyContent: 'center', alignItems: 'center'}}>
                        <Avatar 
                            width = {moderateScale(45)}
                            height = {moderateScale(45)}
                            url = {{uri:this.props.UrlImage}}
                        />
                    </Col>
                    <Col size={8} style = {{justifyContent: 'center'}}>
                        <Text style = {styles.textName}>{this.props.StudentName}</Text>
                        <Text style = {styles.textNote}>{this.props.Note}</Text>
                    </Col>
                </Grid>
            </View>
            <ScrollView>
                {this.lapsList(this.props.sessions)}
            </ScrollView>
        </View>
      </View>
    );
  }
}

