import * as types from '../../constants/ActionTypes';
import * as ColorTypes from './constant';
import update from 'immutability-helper';
import moment from 'moment-timezone';
import {AsyncStorage} from 'react-native';

const initialState = {
    attendances:{},
    sessions:
    [ 
        // { 
        //     session: { name: 'S├íng', start: '07:30', end: '11:50' },
        //     check: 'IN',
        //     at: '07:39',
        //     status: 2,
        //     late_minutes: 9 },
        // { 
        //     session: { name: 'S├íng', start: '07:30', end: '11:50' },
        //     check: 'OUT',
        //     at: '12:02',
        //     status: 1,
        //     late_minutes: 0 
        // } 
    ],
    date: '',
};

function storeUserInfo(type, data){
    switch(type){
        case 'username':
            AsyncStorage.setItem('username', data); break;
        case 'cardidactivate':
            AsyncStorage.setItem('cardidactivate', data); break;
    }    
}

function getStatusText(status){
    switch(status){
        case 1:
            return 'Đúng giờ';
        case 2:
            return 'Đi muộn';
        case 4:
            return 'Nghỉ không lý do';
        case 5:
            return 'Nghỉ có phép';
        case 6:
            return 'Ngày lễ';
    }
}

function convertDate(date){
    return moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD');
}

function getColorFromStatus(status){
    let color;
    switch(status){
        case 2:
            color = ColorTypes.LATE; break;
        case 4:
            color = ColorTypes.UNEXCUSED; break;
        case 5:
            color = ColorTypes.EXCUSED_ABSENCE; break;
        case 6:
            color = ColorTypes.HOLIDAY; break;
        default:
            color = ColorTypes.VALID; break;
    }
    return color;
}

export default function checking(state = initialState, action) {
    switch (action.type) {
        case types.GET_CHECKING_SUCCESS:
            console.log('REDUCER-GET_CHECKING_SUCCESS');
            let attendances = {};  
            action.payload._response.attendances.map((data) => {  
                let status = 1;
                data.sessions.map((item) => {
                    if(item.check === 'IN' && item.status > status) status = item.status;
                })
                if(status > 1) attendances[convertDate(data.date)] = {marked: true, dotColor: getColorFromStatus(status)};
            });
            console.log(attendances);
            return {
                ...state,
                attendances: attendances,
            }
        case types.GET_CHECKING_DETAIL_SUCCESS:
            console.log('REDUCER-GET_CHECKING_DETAIL_SUCCESS');
            let sessionsResult = [];
            let check;
            let count = -1;
            action.payload._response.sessions.map((data) => {
                check = sessionsResult.findIndex(x => x.shiftName == data.session.name);                
                if(check === -1){
                    count++;
                    sessionsResult.push({shiftName: data.session.name, 
                                        staticTime: data.session.start + ' - ' + data.session.end,
                                            inTime: '-', 
                                           outTime: '-', 
                                            status: 0,
                                            color: 'white'});
                    if(data.check === 'IN'){
                        sessionsResult = update(sessionsResult, {[count]: {inTime: {$set: data.at}}});
                        sessionsResult = update(sessionsResult, {[count]: {status: {$set: getStatusText(data.status)}}});
                        sessionsResult = update(sessionsResult, {[count]: {color: {$set: getColorFromStatus(data.status)}}});
                    } 
                    else sessionsResult1 = update(sessionsResult, {[count]: {outTime: {$set: data.at}}});
                }
                else{
                    if(data.check === 'IN') {
                        sessionsResult = update(sessionsResult, {[check]: {inTime: {$set: data.at}}});
                        sessionsResult = update(sessionsResult, {[check]: {status: {$set: getStatusText(data.status)}}});
                        sessionsResult = update(sessionsResult, {[check]: {color: {$set: getColorFromStatus(data.status)}}});
                    }                         
                    else sessionsResult = update(sessionsResult, {[check]: {outTime: {$set: data.at}}});
                }
            });
            console.log(sessionsResult);
            return {
                ...state,
                sessions: sessionsResult,
                date: action.payload._response.date,
            }
        default:
            return state;
    }
}