import * as types from '../../constants/ActionTypes';
import {
  Alert,
  Platform,
  AsyncStorage
} from 'react-native';
import * as MesTypes from '../../constants/MesTypes';

var {
  getChecking,
  getCheckingDetail
} = require('../../services/ITECApi')

import {
  errorProcess,
} from '../../components/GlobalVarContainer';

export function getChecking(username, tokenid, cardnumber, studencode, fromDate, toDate){
  console.log('ACTION-getChecking');
  return async (dispatch) => {
    dispatch({ type: types.GET_CHECKING });
    try{
      let response = await getChecking(username, tokenid, cardnumber, studencode, fromDate, toDate);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({ 
          type: types.GET_CHECKING_SUCCESS,
          payload: { _response }
        });
      }else{  
        dispatch({ type: types.GET_CHECKING_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }      
    }catch(error){  }
  }
}

export function getCheckingDetail(username, tokenid, cardnumber, studencode, date){
  console.log('ACTION-getCheckingDetail');
  return async (dispatch) => {
    dispatch({ type: types.GET_CHECKING_DETAIL });
    try{
      let response = await getCheckingDetail(username, tokenid, cardnumber, studencode, date);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({ 
          type: types.GET_CHECKING_DETAIL_SUCCESS,
          payload: { _response }
        });
        dispatch({ type: types.GO_CHECKINGDETAIL });
      }else{  
        dispatch({ type: types.GET_CHECKING_DETAIL_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }      
    }catch(error){  }
  }
}


