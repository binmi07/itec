import { connect } from 'react-redux'
import CheckingScreen from './CheckingScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    getChecking,
    getCheckingDetail,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        attendances: state.checking.attendances,
        username: state.user.Username,
        studentcode: state.cards.CardActive.student_code,
        cardnumber: state.cards.CardActive.card_number,
        tokenid: state.user.TokenID,
        isLoading: state.progressHud.isLoading,
        Cards: state.cards.Cards,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    getChecking: (username, tokenid, cardnumber, studencode, fromDate, toDate) => dispatch(getChecking(username, tokenid, cardnumber, studencode, fromDate, toDate)),
    getCheckingDetail: (username, tokenid, cardnumber, studencode, date) => dispatch(getCheckingDetail(username, tokenid, cardnumber, studencode, date)),
})

const CheckingComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( CheckingScreen )

export default CheckingComponent