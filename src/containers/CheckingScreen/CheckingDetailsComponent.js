import { connect } from 'react-redux'
import CheckingDetailsScreen from './CheckingDetailsScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';

const mapStateToProps = ( state, ownProps ) => {
    return {
        sessions: state.checking.sessions,
        StudentName: state.cards.CardActive.card_holdername,
        UrlImage: state.cards.CardActive.url_image,
        Note: state.cards.CardActive.class_name + ' - ' + state.cards.CardActive.school_name,
        Date: state.checking.date,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    
})

const CheckingDetailsComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( CheckingDetailsScreen )

export default CheckingDetailsComponent