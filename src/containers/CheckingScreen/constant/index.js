export const VALID = 'white'
export const UNEXCUSED = '#f22020';
export const EXCUSED_ABSENCE = '#1ae05b';
export const LATE = '#1B3882';
export const HOLIDAY = '#f39416';