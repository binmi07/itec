import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Platform,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import {Agenda, Calendar} from 'react-native-calendars';

import TitleInfoComponent from '../TitleInfoScreen/TitleInfoComponent';
import Header from '../../components/Header';
import {Grid, Col, Row} from 'react-native-easy-grid';
import AndroidBackButton from 'android-back-button';
import * as FontFamily from '../../constants/FontFamily';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import moment from 'moment-timezone';
import * as ColorTypes from './constant';
import {
  getCardNoFromStudentCode,
} from '../../components/GlobalVarContainer'

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  },
  bkgCalendar: {
    width: WIDTH * 0.92,
    height: WIDTH * 0.85,
    borderRadius: 7,
    backgroundColor: 'white',
    ...Platform.select({
      ios: {
        overflow: 'hidden',
        shadowColor: "#dae5ef",
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 7,
        shadowOpacity: 1
      },
      android: {
        elevation: 3,
      }
    }),    
  },
  txtMonth: {
    fontFamily: FontFamily.SFUIMedium,
    fontSize: 16,
    fontStyle: "normal",
    letterSpacing: 0.17,
    textAlign: "center",
    color: "#878b9a"
  },
  txtNote: {
    marginLeft: 10,
    fontFamily: FontFamily.SFUIMedium,
    fontSize: 14,
    fontStyle: "normal",
    letterSpacing: 0.15,
    textAlign: "left",
    color: "#6283a3"
  },
  loading: {
    zIndex: 10,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },
});

export default class CheckingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { items: {} };
    this.onDayPress = this.onDayPress.bind(this);
  }

  componentWillMount(){
    let today = moment().tz("Asia/Ho_Chi_Minh");
    let fromdate = '01' + '/' + (today.month() + 1) + '/' + (today.year());
    let todate = today.format('DD/MM/YYYY');
    console.log(todate);
    this.props.getChecking(this.props.username,
                            this.props.tokenid,
                            this.props.cardnumber,
                            this.props.studentcode,
                            fromdate,
                            todate);
  }

  onRefresh(studentcode){
    let today = moment().tz("Asia/Ho_Chi_Minh");
    let fromdate = '01' + '/' + (today.month() + 1) + '/' + (today.year());
    let todate = today.format('DD/MM/YYYY');
    this.props.getChecking(this.props.username,
                            this.props.tokenid,
                            getCardNoFromStudentCode(this.props.Cards, studentcode),
                            // this.props.cardnumber,
                            studentcode,
                            fromdate,
                            todate);
  }

  onDayPress(day) { 
    // console.log(day.day + '/' + day.month);
    if(day.month !== moment().month() + 1) return;
    let chooseDay = day.day + '/' + day.month + '/' + day.year;
    console.log(chooseDay) ;
    this.props.getCheckingDetail(this.props.username,
                                  this.props.tokenid,
                                  this.props.cardnumber,
                                  this.props.studentcode,
                                  chooseDay);
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }

  render() {
    return (     
      <View style = {{flex: 1}}> 
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <Header
          Left = 'back'
          Body = 'ĐIỂM DANH'
          onPress = {() => {this.props.navigation.goBack()}}
        />
        <TitleInfoComponent onPress = {() => {this.props.navigation.navigate('ChooseCard', {onRefresh: this.onRefresh.bind(this)})}}/>
        <View style = {{height: 50}}>
          <Grid>
            <Col size={2} style = {{alignItems: 'center', justifyContent: 'center'}}>
              <Image source = {require('./img/icoCalendar.png')}/>
            </Col>
            <Col size={6} style = {{alignItems: 'center', justifyContent: 'center'}}>
              <Text style = {styles.txtMonth}>Tháng {moment().month() + 1} năm {moment().year()}</Text>
            </Col>
            <Col size={2}>
            </Col>
        </Grid>
        </View>
        <View style = {{alignItems: 'center'}}>
          <View style={styles.bkgCalendar}>
            <Calendar
              onDayPress={this.onDayPress}
              style={styles.calendar}
              theme={{
                // backgroundColor: '#ffffff',
                // calendarBackground: '#ffffff',
                // textSectionTitleColor: '#b6c1cd',
                // selectedDayBackgroundColor: '#00adf5',
                // selectedDayTextColor: '#ffffff',
                // todayTextColor: '#00adf5',
                dayTextColor: '#8ba0b5',
                // textDisabledColor: '#d9e1e8',
                // dotColor: '#00adf5',
                // selectedDotColor: '#ffffff',
                // arrowColor: 'orange',
                // monthTextColor: 'blue',
                textDayFontFamily: FontFamily.SFUIBold,
                // textMonthFontFamily: 'monospace',
                // textDayHeaderFontFamily: 'monospace',
                textDayFontSize: 14,
                // textMonthFontSize: 16,
                // textDayHeaderFontSize: 16
              }}
              current={moment().tz("Asia/Ho_Chi_Minh").format("YYYY-MM-DD")}
              firstDay={1}
              markedDates={this.props.attendances}
              hideArrows={true}
            />
          </View>
        </View>
        <View style = {{height: 50}}>
          <Grid>
            <Col>
              <View style = {{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                <View style = {{width: 20, height: 20, borderRadius: 10, backgroundColor: ColorTypes.UNEXCUSED, marginLeft: WIDTH * 0.04}}/>
                <Text style = {styles.txtNote}>Nghỉ không phép</Text>
              </View>
              <View style = {{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                <View style = {{width: 20, height: 20, borderRadius: 10, backgroundColor: ColorTypes.LATE, marginLeft: WIDTH * 0.04}}/>
                <Text style = {styles.txtNote}>Đi muộn</Text>
              </View>
            </Col>
            <Col>
              <View style = {{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                <View style = {{width: 20, height: 20, borderRadius: 10, backgroundColor: ColorTypes.EXCUSED_ABSENCE, marginLeft: WIDTH * 0.04}}/>
                <Text style = {styles.txtNote}>Nghỉ có phép</Text>
              </View>
              <View style = {{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                <View style = {{width: 20, height: 20, borderRadius: 10, backgroundColor: ColorTypes.HOLIDAY, marginLeft: WIDTH * 0.04}}/>
                <Text style = {styles.txtNote}>Nghỉ lễ</Text>
              </View>
            </Col>
          </Grid>
        </View>
        {
          this.props.isLoading === true ? <ActivityIndicator
                                            animating = {true}
                                            size = 'large'
                                            style = {styles.loading}
                                            color = 'black'/> 
                                        : <View/>
        }
      </View>  
    );
  }
}
