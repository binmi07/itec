import React, { Component } from 'react';
import { 
  ImageBackground, 
  StyleSheet, 
  Image, 
  Modal, 
  TouchableHighlight, 
  View, 
  ShadowPropTypesIOS,
  Text,
  Dimensions,
  Platform,
} from 'react-native';
import * as FontFamily from '../../constants/FontFamily';
import {Grid, Col, Row} from 'react-native-easy-grid';
import SmallButton from '../../components/SmallButton';
import {moderateScale, verticalScale, WIDTH, HEIGHT} from '../../components/ScaleRes';
import Avatar from '../../components/Avatar';
import PropTypes from 'prop-types';

export default class TitleInfoScreen extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    includeStatusBar: PropTypes.bool,
  };

  static defaultProps = {
    onPress: null,
    includeStatusBar: false,
  };

  onPress(){
    this.props.onPress();
  }

  componentDidMount(){
    if(this.props.CardIdActive === -1) this.props.updateCardActive(this.props.Card.id);
  }

  render() {
    return (
      <View>
      {
        this.props.includeStatusBar === true
          ? Platform.OS === 'android' 
              ? (Platform.Version < 21 
                  ? <View/> 
                  : <Image style = {{width: WIDTH}} source = {require('./img/bgTabbar.png')}/>) 
              : <Image style = {{width: WIDTH, height: HEIGHT === 812 ? 44 : 20}} source = {require('./img/bgTabbar.png')}/>
          : <View/>
      }
      <ImageBackground 
        style = {styles.background} 
        source={require('./img/bgHeaderNotification.png')}>
        <Grid>
          <Col size = {19} style = {{justifyContent:'center', alignItems: 'center'}}>
            <Avatar 
              width = {moderateScale(45)}
              height = {moderateScale(45)}
              // url = {require('../../../assets/images/robot-dev.png')}
              url = {{uri:this.props.Card.url_image}}
            />
          </Col>
          <Col size = {57} style = {{justifyContent:'center'}}>
            <View>
              <Text style = {styles.textName} numberOfLines ={1}>{this.props.Card.student_name}</Text>
              <Text style = {styles.textNote} numberOfLines ={1}>{this.props.Card.class_name} - {this.props.Card.school_name}</Text></View>
          </Col>
          <Col size = {24} style = {{justifyContent:'center'}}>
            {
              this.props.NumberOfCard > 1 ?
                <SmallButton
                  text = 'CHUYỂN'
                  textSize = {moderateScale(12)}
                  textFont = {FontFamily.SFUIMedium}
                  textColor = {'#122664'}
                  width = {moderateScale(76)}
                  height = {moderateScale(29)}
                  onPress = {() => {this.onPress()}} 
                /> : <View/>
            }
          </Col>
        </Grid>
      </ImageBackground>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  background: {
    // width: moderateScale(375),
    height: moderateScale(66),
    //alignItems: 'center',
  },
  textName: {
    color:'white', 
    fontSize: moderateScale(16), 
    fontFamily: FontFamily.SFUISemibold,
    backgroundColor: 'transparent'
  },
  textNote: {
    color:'#d4fbff', 
    fontSize: moderateScale(11), 
    fontFamily: FontFamily.SFUIRegular,
    backgroundColor: 'transparent'
  },
});
