import { connect } from 'react-redux'
import TitleInfoScreen from './TitleInfoScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';

import {
    updateCardActive,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        Card: state.cards.CardActive,
        NumberOfCard: state.cards.Cards.length,
        CardIdActive: state.cards.CardIdActive,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    updateCardActive: (CardId) => dispatch(updateCardActive(CardId)),
})

const TitleInfoComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( TitleInfoScreen )

export default TitleInfoComponent