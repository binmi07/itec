import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';

import Header from '../../components/Header';
import { Grid, Col, Row } from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import * as Config from '../../constants/Config';
import AndroidBackButton from 'android-back-button';
import { callPhone, sendEmail } from '../../components/GlobalVarContainer';
import { moderateScale, WIDTH } from '../../components/ScaleRes';

export default class SupportScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (     
      <View style = {{flex:1, backgroundColor: 'white'}}> 
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <Header
          Left = 'back'
          Body = 'HỖ TRỢ - LIÊN HỆ'
          onPress = {() => {this.props.navigation.goBack()}}
        />
        <Grid>
            <Col size={6.5}></Col>
            <Col size={87}>
                <View style = {styles.bkg}>
                    {
                        this.props.isItec === true
                        ? undefined
                        :   <Text style = {styles.txtHotline}>Hotline: {Config.HOT_LINE}</Text>
                    }
                    {
                        this.props.isItec === true
                        ? undefined
                        :   <View style = {{flexDirection: 'row', marginTop: 20}}>
                                <Image style = {styles.icon} source = {{uri: Config.ICON_4}}/>
                                <TouchableOpacity onPress = {() => callPhone(Config.CONTACT_PHONE)}>
                                    <Text style = {styles.txtContent}>{Config.CONTACT_PHONE}</Text>
                                </TouchableOpacity>
                            </View>
                    }     
                    
                    <View style = {{flexDirection: 'row', marginTop: 20}}>
                        <Image source = {require('./img/icoSmallMail.png')}/>
                        <TouchableOpacity onPress = {() => sendEmail(Config.CONTACT_EMAIL)}>
                            <Text style = {styles.txtContent}>{Config.CONTACT_EMAIL}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style = {{flexDirection: 'row', marginTop: 20,}}>
                        <Image source = {require('./img/icoSmallAdd.png')}/>
                        <Text style = {styles.txtContent}>{Config.ADDRESS}</Text>
                    </View>
                </View>
                {
                    this.props.isItec === true
                    ? undefined
                    :   <View style = {{flexDirection: 'row', marginTop: 30, justifyContent: 'space-around'}}>
                            <Image source = {require('./img/icoFb.png')}/>
                            <Image source = {require('./img/icoFbCopy.png')}/>
                            <Image source = {require('./img/icoFbCopy2.png')}/>
                        </View>
                }                
            </Col>
            <Col size={6.5}></Col>
        </Grid>
      </View> 
    );
  }
}

const styles = StyleSheet.create({
    icon: {
        width: moderateScale(21),
        height: moderateScale(20),
      },
    bkg: {
        height: 200,
        // backgroundColor: 'white',
        justifyContent: 'center'
    },
    txtHotline: {
        fontFamily: FontFamily.SFUIMedium,
        fontSize: 20,
        letterSpacing: 0.21,
        textAlign: "left",
        color: "#1b3882"
    },
    txtContent: {
        flex: 1,
        marginLeft: 14,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 14,
        letterSpacing: 0.15,
        textAlign: "left",
        color: "#a2a5b0"
    },
});
