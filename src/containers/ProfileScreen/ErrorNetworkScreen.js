import React, { Component } from 'react';
import { 
  Image,
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  TextInput
 } from 'react-native';
 import Header from '../../components/Header';
 import AndroidBackButton from 'android-back-button';
 import TitleInfoComponent from '../../containers/TitleInfoScreen/TitleInfoComponent';
 import IconGo from '../../components/IconGo';
 import * as FontFamily from '../../constants/FontFamily'; 
 import {Grid, Col, Row} from 'react-native-easy-grid';
 import MainButton from '../../components/MainButton';
 import Avatar from '../../components/Avatar';
 import {
  numberWithCommas,
 } from '../../components/GlobalVarContainer';

 import {
    moderateScale,
 } from '../../components/ScaleRes';

export default class ErrorNetworkScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      
    }
  }

  onPressGoHome(){
    this.props.onGoMain();
  }

  componentWillMount(){
    
  }

  onPressAccept(){
    
  }

  render() {
    return (
      <View style = {{backgroundColor: '#f0f4f5', flex: 1}}>
        <AndroidBackButton
          onPress={() => {
            //   this.props.navigation.goBack();
              return true;
          }}
        />
        <Header
          
          Body = 'TITLE'
          onPress = {() => {this.props.navigation.goBack()}}
        />
        <View style = {{marginTop: 100, alignItems: 'center'}}>
            <Image style = {{width: 71, height: 56}}
                source = {require('./img/icoErrorNetwork.png')}
            />
            <Text style = {styles.txtSuccess}>Không có kết nối</Text>
            <Text style = {styles.txtContinue}>Vui lòng kiểm tra lại kết nối của bạn</Text>
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  txtSuccess: {
    marginTop: 30,
    fontFamily: 'Roboto',
    fontSize: 22,
    letterSpacing: 0,
    textAlign: "center",
    color: "#0597e6"
  },
  txtContinue: {
    marginTop: 7,
    fontFamily: "Roboto",
    fontSize: 14,
    letterSpacing: 0,
    textAlign: "center",
    color: "#1b3882"
  },
})