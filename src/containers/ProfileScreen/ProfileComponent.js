import { connect } from 'react-redux'
import ProfileScreen from './ProfileScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    logout,
    resetPassword,
    resetPayment
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        user: state.user,
        about: state.user.about,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    onUpdateNotiSetting: () => {
        dispatch( {
            type: types.UPDATE_NOTI_SETTING
        } )
    },
    logout: (username, tokenid, isLogin) => dispatch(logout(username, tokenid, isLogin)),
    resetPayment: (username, tokenid, amount, transref) => dispatch(resetPayment(username, tokenid, amount, transref)),
    resetPassword: (username, password, tokenid) => dispatch(resetPassword(username, password, tokenid)),
})

const ProfileComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( ProfileScreen )

export default ProfileComponent