import { connect } from 'react-redux'
import ProfileInfoScreen from './ProfileInfoScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';

const mapStateToProps = ( state, ownProps ) => {
    return {
        user: state.user,
        cards: state.cards,
        isItec: state.user.isItec,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({    
    
})

const ProfileInfoComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( ProfileInfoScreen )

export default ProfileInfoComponent