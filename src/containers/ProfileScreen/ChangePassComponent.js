import { connect } from 'react-redux'
import ChangePassScreen from './ChangePassScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    changePassword,
    setPasswordInMain
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,
        TokenID: state.user.TokenID,
        Username: state.user.Username,
        isLogin: state.user.isLogin,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({    
    setPassword: (username, password, tokenid) => dispatch(setPasswordInMain(username, password, tokenid)),
    changePassword: (username, oldPassword, newPassword, tokenid) => dispatch(changePassword(username, oldPassword, newPassword, tokenid)),
})

const ChangePassComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( ChangePassScreen )

export default ChangePassComponent