import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';

import Header from '../../components/Header';
import { Grid, Col, Row } from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import AndroidBackButton from 'android-back-button';

export default class ProfileInfoScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (     
      <View style = {{flex:1, backgroundColor: '#f0f4f5'}}> 
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <Header
          Left = 'back'
          Body = 'THÔNG TIN CÁ NHÂN'
          onPress = {() => {this.props.navigation.goBack()}}
        />
        <View style = {styles.bkgName}>
            <Text style = {styles.txtName}>{this.props.user.Fullname}</Text>
            <Text style = {styles.txtPhone}>{this.props.user.PhoneNumber}</Text>
        </View>
        <View style = {{height: 200, backgroundColor: 'white', justifyContent: 'center'}}>
            <View style = {{height: 10}}/>
            <Grid>
                <Row>
                    <Col size={1}><Text style = {styles.txtTitle}>CMND</Text></Col>
                    <Col size={1}><Text style = {styles.txtContent}>{this.props.isItec === true?'0313367277':' '} </Text></Col>
                </Row>
                <Row>
                    <Col size={1}><Text style = {styles.txtTitle}>Email</Text></Col>
                    <Col size={1}><Text style = {styles.txtContent}>{this.props.isItec === true?'vuvan@gmail.com':' '}</Text></Col>
                </Row>
                <Row>
                    <Col size={1}><Text style = {styles.txtTitle}>Quan hệ với HS</Text></Col>
                    <Col size={1}><Text style = {styles.txtContent}>{this.props.isItec === true?'Cha':' '}</Text></Col>
                </Row>
                <Row>
                    <Col size={1}><Text style = {styles.txtTitle}>Địa chỉ</Text></Col>
                    <Col size={1}><Text style = {styles.txtContent}>{this.props.isItec === true?'20A2 Lach Tray':' '}</Text></Col>
                </Row>
                <Row>
                    <Col size={1}><Text style = {styles.txtTitle}>Số lượng thẻ</Text></Col>
                    <Col size={1}><Text style = {styles.txtContent}>{this.props.cards.Cards.length}</Text></Col>
                </Row>
            </Grid>
        </View>
      </View> 
    );
  }
}

const styles = StyleSheet.create({
    txtContent: {
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 14,
        letterSpacing: 0.15,
        textAlign: "left",
        color: "#222f5c"
    },
    txtTitle: {
        marginLeft: 20,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 14,
        letterSpacing: 0.15,
        textAlign: "left",
        color: "#8d919f"
    },
    txtName: {
        fontFamily: FontFamily.SFUIMedium,
        fontSize: 20,
        letterSpacing: 0.21,
        textAlign: "center",
        color: "#1b3882"
    },
    txtPhone: {
        marginTop: 6,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 17,
        letterSpacing: 0.18,
        textAlign: "center",
        color: "#9699a6"
    },
    bkgName:{
        height: 84, 
        backgroundColor: 'white', 
        alignItems: 'center', 
        justifyContent: 'center', 
        borderBottomWidth: 1, 
        borderColor: '#e8eeef'
    }
});
