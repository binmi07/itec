import React, { Component } from 'react';
import {Switch, Text, StyleSheet, Image, TouchableOpacity, ImageBackground, View, ScrollView, Platform, Alert, Dimensions} from 'react-native';
import {Grid, Col, Row} from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import IconGo from '../../components/IconGo';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import DeviceInfo from 'react-native-device-info';

export default class ProfileScreen extends Component {
  constructor(props){
    super(props);
    this.onChangeNoti = this.onChangeNoti.bind(this);
    this.onPress_Logout = this.onPress_Logout.bind(this);
  }

  componentWillMount(){
    console.log('Profile');
  }

  onPress_AppInfo(){
    Alert.alert(
      '',
      this.props.about.name + ' version: ' + this.props.about.version +'\nDesign & Copyright by ITE JSC',
      // DeviceInfo.getDeviceId() + ' ' + DeviceInfo.getDeviceName() + ' ' + DeviceInfo.getModel(),
      [
        {text: 'OK', onPress: () => {}},
      ],
      { cancelable: true }
    )
    // if(this.props.user.Username === '01686476291')
    // this.props.resetPassword(this.props.user.Username, '123456', this.props.user.TokenID);
  }

  onPress_Logout(){
    this.props.logout(this.props.user.Username, this.props.user.TokenID, this.props.user.isLogin);
  }

  onChangeNoti(){
    this.props.onUpdateNotiSetting();
  }

  render() {
    return (      
      <View 
        style = {styles.container}>
        <ImageBackground 
          style = {styles.background} 
          source={require('./img/bg.png')}>
          <View style = {{height: Platform.OS === 'ios' ? 20 : (Platform.Version >= 21 ? 20 : 0)}}/>
          <View>
            <Text style = {styles.txtName}>{this.props.user.Fullname}</Text>
            <Text style = {styles.txtPhone}>{this.props.user.PhoneNumber}</Text>
          </View>
        </ImageBackground>
        <ScrollView>
          <TouchableOpacity onPress = {() => {this.props.navigation.navigate('CardManager')}}>
            <DrawItem url = {require('./img/icoCardSetting.png')} title = "Quản lý thẻ ITEC" right = { <IconGo style = {styles.iconGo}/>}/>
          </TouchableOpacity>
          <TouchableOpacity onPress = {() => {this.props.navigation.navigate('ProfileInfo')}}>
            <DrawItem url = {require('./img/icoProfile.png')} title = "Thông tin cá nhân" right = { <IconGo style = {styles.iconGo}/>}/>
          </TouchableOpacity>
          <TouchableOpacity onPress = {() => {this.props.navigation.navigate('ChangePass')}}>
            <DrawItem url = {require('./img/icoPassword.png')} title = {this.props.user.isLogin ? "Đổi mật khẩu" : "Tạo mật khẩu"} right = { <IconGo style = {styles.iconGo}/>}/>
          </TouchableOpacity>
            <DrawItem url = {require('./img/icoNotification.png')} title = "Nhận thông báo" 
                            right = {
                              <Switch style= {{marginRight: WIDTH * 0.06}} value = {this.props.user.isReceiveNoti} onValueChange = {() => {this.onChangeNoti()}}/>
                            }/>
          <View style = {{marginTop: 20}}/>
          <TouchableOpacity onPress = {() => {this.props.navigation.navigate('Support')}}>
            <DrawItem url = {require('./img/icoContact.png')} title = "Hỗ trợ - liên hệ" right = { <IconGo style = {styles.iconGo}/>}/>
          </TouchableOpacity>
          <TouchableOpacity onPress = {() => {this.onPress_AppInfo()}}>
            <DrawItem url = {require('./img/icoInfo.png')} title = "Thông tin ứng dụng" right = { <IconGo style = {styles.iconGo}/>}/>
          </TouchableOpacity>
          <TouchableOpacity onPress = {() => {this.onPress_Logout()}}>
            <DrawItem url = {require('./img/icoLogout.png')} title = "Đăng xuất" right = { <IconGo style = {styles.iconGo}/>}/>
          </TouchableOpacity>
        </ScrollView>
      </View>      
    );
  }
}

export class DrawItem extends Component{
  render(){
    return(
      <View style = {styles.form}>
        <Grid style = {{backgroundColor: 'white'}}>
            <Row>
              <Col size ={2} style = {{justifyContent:'center'}}>
                <Image style = {styles.icon} source = {this.props.url}/>
              </Col>
              <Col size ={6} style = {{justifyContent:'center'}}>
                <Text style = {styles.textItem}>{this.props.title}</Text>
              </Col>
              <Col size ={2} style = {{alignItems:'flex-end', justifyContent:'center'}}>
                {this.props.right}
              </Col>
            </Row>
        </Grid>     
      </View>
    );
  }
}

var styles = StyleSheet.create({
  txtName: {
    fontFamily: FontFamily.SFUIMedium, 
    fontSize: 20, 
    textAlign: "center", 
    color: 'white',
    backgroundColor: 'transparent'
  },
  txtPhone: {
    fontFamily: FontFamily.SFUIRegular, 
    fontSize: 17, 
    textAlign: "center", 
    color: '#65deff',
    backgroundColor: 'transparent'
  },
  container: {
    //alignItems : 'center'
    backgroundColor: '#f0f4f5'
  },
  background: {
    height: moderateScale(100) - (Platform.OS === 'ios' ? 0 : (Platform.Version >= 21 ? 0 : 20)),
    justifyContent:'center'
  },
  logo: {
    marginTop:60,   
  },
  form: {
    height:58,
    borderBottomWidth: 1,
    borderBottomColor: '#f0f4f5'
  },
  formList: {
    //backgroundColor: 'white'
  },
  button: {
    marginTop: 20,
    // backgroundColor: '#3cc2fd',
    // height: 45,
  },
  input: {
    // backgroundColor : 'white', 
    textAlign: 'center',
    // height: 45,
  },
  icon: {
    marginLeft: WIDTH * 0.06,
  },
  iconGo: {
    marginRight: WIDTH * 0.06,
  },
  textItem: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 16,
    lineHeight: 20,
    letterSpacing: 0.17,
    textAlign: "left",
    color: "#878b9a"
  }
});
