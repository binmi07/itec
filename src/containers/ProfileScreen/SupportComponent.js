import { connect } from 'react-redux'
import SupportScreen from './SupportScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    logout,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        user: state.user,
        isItec: state.user.isItec,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({    
    logout: (username, tokenid) => dispatch(logout(username, tokenid)),
})

const SupportComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( SupportScreen )

export default SupportComponent