import { connect } from 'react-redux'
import ErrorNetworkScreen from './ErrorNetworkScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';

const mapStateToProps = ( state, ownProps ) => {
    return {

    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    onGoMain: () => {
        dispatch( {
            type: types.GO_MAIN
        } )
    }
})

const ErrorNetworkComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( ErrorNetworkScreen )

export default ErrorNetworkComponent