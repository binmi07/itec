import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  TextInput,
  Alert,
  Keyboard,
  Platform,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import Header from '../../components/Header';
import { Grid, Col, Row } from 'react-native-easy-grid';
import * as FontFamily from '../../constants/FontFamily';
import * as MesTypes from '../../constants/MesTypes';
import AndroidBackButton from 'android-back-button';
import MainButton from '../../components/MainButton';
import {moderateScale, WIDTH} from '../../components/ScaleRes';

export default class SupportScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        oldPass : '',
        newPass : '',
        newPassConfirm : '',
    }
  }

  onPressUpdate(){
    Keyboard.dismiss();
    if(this.props.isLogin){
      if(this.state.oldPass.length !== 6 || this.state.newPass.length !== 6) {
        Alert.alert(
          'Thông báo',
          MesTypes.PASS_MUST_6_LETTERS,
          [ {text: 'OK', onPress: () => console.log('OK Pressed')}, ],
          { cancelable: false }
        )      
      }
      else if(this.state.newPass !== this.state.newPassConfirm){
        Alert.alert(
          'Thông báo',
          MesTypes.REPASS_NOT_CORRECT,
          [ {text: 'OK', onPress: () => console.log('OK Pressed')}, ],
          { cancelable: false }
        )
      }
      else{
        this.props.changePassword(this.props.Username, this.state.oldPass, this.state.newPass, this.props.TokenID);
      }
    }else{
      if(this.state.newPass.length !== 6) {
        Alert.alert(
          'Thông báo',
          MesTypes.PASS_MUST_6_LETTERS,
          [ {text: 'OK', onPress: () => console.log('OK Pressed')}, ],
          { cancelable: false }
        )      
      }
      else if(this.state.newPass !== this.state.newPassConfirm){
        Alert.alert(
          'Thông báo',
          MesTypes.REPASS_NOT_CORRECT,
          [ {text: 'OK', onPress: () => console.log('OK Pressed')}, ],
          { cancelable: false }
        )
      }
      else{
        this.props.setPassword(this.props.Username, this.state.newPass, this.props.TokenID);
      }
    }
  }

  render() {
    return (   
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>  
        <View style = {{flex:1, backgroundColor: '#f0f4f5', }}> 
          <AndroidBackButton
              onPress={() => {
                  this.props.navigation.goBack();
                  return true;
              }}
          />
          <Header
            Left = 'back'
            Body = {this.props.isLogin ? 'ĐỔI MẬT KHẨU' : 'TẠO MẬT KHẨU'}
            onPress = {() => {this.props.navigation.goBack()}}
          />
          <View style = {styles.bkg}>
            {this.props.isLogin 
              ? <View>
                  <View style = {{height: 15}}/>
                    <TextInput
                        style = {styles.input}
                        blurOnSubmit = {true}
                        keyboardType = {Platform.OS === 'ios' ? 'phone-pad' : 'default'}
                        secureTextEntry 
                        maxLength = {6}
                        placeholder = {this.props.isLogin ? "Mật khẩu cũ" : "Mật khẩu"}
                        underlineColorAndroid = 'transparent'
                        onChangeText = {(value) => this.setState({oldPass: value})}
                        value={this.state.oldPass}
                    /> 
                  </View> 
              : <View/>}
              <View style = {{height: 20}}/>
              <TextInput
                  style = {styles.input}
                  blurOnSubmit = {true}
                  keyboardType = {Platform.OS === 'ios' ? 'phone-pad' : 'default'}
                  secureTextEntry 
                  maxLength = {6}
                  placeholder = {this.props.isLogin ? "Mật khẩu mới" : "Mật khẩu"}
                  underlineColorAndroid = 'transparent'
                  onChangeText = {(value) => this.setState({newPass: value})}
                  value={this.state.newPass}
              />
              <View style = {{height: 20}}/>
              <TextInput
                  style = {styles.input}
                  blurOnSubmit = {true}
                  keyboardType = {Platform.OS === 'ios' ? 'phone-pad' : 'default'}
                  secureTextEntry 
                  maxLength = {6}
                  placeholder = {this.props.isLogin ? "Nhập lại mật khẩu mới" : "Nhập lại mật khẩu"}
                  underlineColorAndroid = 'transparent'
                  onChangeText = {(value) => this.setState({newPassConfirm: value})}
                  value={this.state.newPassConfirm}
              />
              <View style = {{height: 30}}/>
              <MainButton 
                  disable = {this.props.isLogin ? (this.state.oldPass.length === 0 || this.state.newPass.length === 0 || this.state.newPassConfirm.length === 0)
                                                : (this.state.newPass.length === 0 || this.state.newPassConfirm.length === 0)}
                  //disable = {!(this.state.FirstDay || this.state.LastDay || this.state.OtherDay)}
                  //style = {styles.buttonAccept}
                  text = 'CẬP NHẬT'
                  width = {WIDTH * 0.84}
                  height = {WIDTH * 0.84 / 7}
                  textSize = {moderateScale(18)}
                  backgroundColor = '#2b58b7'
                  onPress = {() => {this.onPressUpdate()}} 
              />
          </View>
          {
            this.props.isLoading === true ? <ActivityIndicator
                                              animating = {true}
                                              size = 'large'
                                              style = {styles.loading}
                                              color = 'black'/> 
                                          : <View/>
          }
        </View> 
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF88',
  },
  bkg: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center'
  },
  input: {
      fontFamily: FontFamily.SFUIRegular,
      fontSize: moderateScale(16),
      textAlign: 'center',
      width: WIDTH * 0.84,
      height: WIDTH * 0.84 / 7,
      borderRadius: 4,
      backgroundColor: 'white',
      borderStyle: "solid",
      borderWidth: 1,
      borderColor: "#dee5e8"
  }
});
