import React, { Component } from 'react';
import { TouchableOpacity, StyleSheet, Image, Modal, View, Dimensions, Button, Text, WebView, Platform, ScrollView } from 'react-native';
import * as FontFamily from '../../constants/FontFamily';
import {Grid, Row, Col} from 'react-native-easy-grid';
import MainButton from '../../components/MainButton';
import {moderateScale, WIDTH, HEIGHT} from '../../components/ScaleRes';
import IconClose from '../../components/IconClose';
import AndroidBackButton from 'android-back-button';

export default class ChooseDayModalScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      chooseDay: this.props.navigation.state.params.Payday,
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <AndroidBackButton
            onPress={() => {
                console.log()
                this.props.navigation.goBack();
                return true;
            }}
        />
        <View style = {styles.header}>
          <Grid>
            <Col size = {1}>
              <TouchableOpacity onPress = {() => {this.props.navigation.goBack()}}>
                <IconClose/>
              </TouchableOpacity>
            </Col>
            <Col size = {5} style = {{alignItems:'center', justifyContent: 'center'}}>
              <Text style = {styles.title}>{this.props.navigation.state.params.title}</Text>
            </Col>
            <Col size = {1}></Col>
          </Grid>
        </View>  
        {
          this.props.navigation.state.params.isItec === true
          ? <ScrollView style = {{backgroundColor: 'white', flex: 1}}>
              <Image 
                style = {{ width: WIDTH, height: this.props.navigation.state.params.title === 'Hướng dẫn' ? WIDTH / 754 * 2981 : WIDTH}}
                resizeMode = 'contain'
                source = {this.props.navigation.state.params.title === 'Hướng dẫn' 
                          ? require('./img/huongdan.png') 
                          : require('./img/gioithieu.jpg')} />
            </ScrollView>
          : <WebView
              source={{uri: this.props.navigation.state.params.uri}}
              style = {{flex: 1}}
            />
        }      
        
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    alignItems : 'center'
  },
  header: {
    flexDirection: 'row',
    height: 50,
    width: WIDTH,
    backgroundColor: "#00a6ec",
    marginTop:  Platform.OS === 'ios' 
                ? HEIGHT === 812 
                  ? 44
                  : 20
                : 20,
  },
  title: {
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 18,
    letterSpacing: 0.19,
    textAlign: "center",
    color: 'white'
  },
});