import { connect } from 'react-redux'
import HomeScreen from './HomeScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import{
    getNotiFirst,
} from '../../actions'

const mapStateToProps = ( state, ownProps ) => {
    return {
        user: state.user,
        cards: state.cards,
        username: state.user.Username,
        tokenid: state.user.TokenID,
        isFirstStart: state.user.isFirstStart,
        isItec: state.user.isItec,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    updateFirstStart: () => dispatch({type: types.UPDATE_FIRST_START}),
    getNotiFirst: (username, studentcode, fromdate, todate, tokenid) => dispatch(getNotiFirst(username, studentcode, fromdate, todate, tokenid)),
})

const HomeCompoment = connect(
    mapStateToProps,
    mapDispatchToProps
)( HomeScreen )

export default HomeCompoment