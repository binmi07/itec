import { connect } from 'react-redux'
import HomeScreen from './HomeScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';

const mapStateToProps = ( state, ownProps ) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    onLogin: () => {
        dispatch( {
            type: types.USER_LOGIN
        } )
    }
})

const HomeCompoment = connect(
    mapStateToProps,
    mapDispatchToProps
)( HomeScreen )

export default HomeCompoment