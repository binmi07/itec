import React, {Component} from 'react';
import { Text, ImageBackground, Image, TouchableOpacity, StyleSheet, Dimensions, View, StatusBar, Keyboard, WebView, Alert, ScrollView, Platform} from 'react-native';
import TitleInfoComponent from '../TitleInfoScreen/TitleInfoComponent';
import * as FontFamily from '../../constants/FontFamily';
import * as actionTypes from '../../constants/ActionTypes';
import * as config from '../../constants/Config';
import {Grid, Row, Col} from 'react-native-easy-grid';
import SlideShow from '../../components/SlideShow';
import AndroidBackButton from 'android-back-button';
import { callPhone } from '../../components/GlobalVarContainer';
import {moderateScale, scale, WIDTH} from '../../components/ScaleRes';

export default class HomeScreen extends Component{
  constructor(props){
    super(props);
    this.onPress_Nav = this.onPress_Nav.bind(this);
  }

  componentWillMount(){
    console.log(FontFamily.SFUIBold);
    Keyboard.dismiss();
    if(this.props.isFirstStart){
      this.props.updateFirstStart();
      this.props.getNotiFirst(this.props.username, 
        '',
        '27/12/2017',
        '',
        this.props.tokenid);
    }
  }

  onPress_Nav(go){
    this.props.navigation.navigate(go);
  }

  onPressGoWeb(go){
    switch(go){
      case 'huongdan':  
        this.props.navigation.navigate('WebView',{uri: config.GUIDE_LINK, title: 'Hướng dẫn', isItec: this.props.isItec}); break;
      case 'gioithieu':
        this.props.navigation.navigate('WebView',{uri: config.INTRO_LINK, title: 'Giới thiệu', isItec: this.props.isItec}); break;
      case 'hotro':
        callPhone(config.CONTACT_PHONE); break;
    }
  }

  getPosOfCardActive(Cards, CardId){
    let result = Cards.findIndex(x => x.id == CardId);
    return Cards[result > -1 ? result : 0];
  }

  render(){
    return(
      <View style = {{backgroundColor: '#f3f8fa', flex: 1}}>        
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <StatusBar backgroundColor = "transparent" translucent />
        {/* {
          Platform.OS === 'android' 
            ? (Platform.Version < 21 
                ? <View/> 
                : <Image style = {{width: WIDTH}} source = {require('./img/bgTabbar.png')}/>) 
            : <Image style = {{width: WIDTH}} source = {require('./img/bgTabbar.png')}/>
        } */}
        <TitleInfoComponent onPress = {() => {this.onPress_Nav('ChooseCard')}} includeStatusBar = {true}/>
        {/*---------------------------------------------------------------*/}
        <ImageBackground 
          style = {styles.background} 
          source={require('./img/bgFeature.png')}>
          <Grid>
            <Col size = {1} style = {{justifyContent:'center'}}>
              <TouchableOpacity 
                style = {{alignItems: 'center'}} 
                onPress = {() => {this.props.navigation.navigate('CardInfo',{Card: this.getPosOfCardActive(this.props.cards.Cards, this.props.cards.CardIdActive)})}}>
                <Image style={styles.iconMain} source={require('./img/icoCard.png')} />
                <Text style = {styles.textMainIcon}>THÔNG TIN{'\n'}THẺ</Text>
              </TouchableOpacity>  
            </Col>
            <Col size = {1} style = {{justifyContent:'center'}}>
              <View style = {{alignItems:'center'}}>
                <Image style={styles.iconMain} source={require('./img/icoBank.png')} />
                <Text style = {styles.txtMoney}> </Text>
                <Text style = {styles.textMainIcon}>VND</Text>
              </View>
            </Col>
            {
              this.props.isItec === true
              ? undefined
              : <Col size = {1} style = {{justifyContent:'center'}}>
                  <View 
                    // onPress = {() => {this.onPress_Nav('Payment')}}
                    style = {{alignItems: 'center'}}>
                    {/* <Image style={styles.iconMain} source={require('./img/icoTuitionPayment.png')} /> */}
                    <Image style={styles.iconMain} source={{uri: config.ICON_1}} />
                    <Text style = {styles.textMainIcon}>THANH TOÁN{'\n'}HỌC PHÍ</Text>
                  </View> 
                </Col>
            }            
          </Grid>
        </ImageBackground>
        {/*---------------------------------------------------------------*/}
        <ScrollView>
          <Grid>
            <Row style = {{justifyContent:'center', alignItems: 'center', backgroundColor:'white', height: WIDTH / 3.2}}>
              {
                this.props.isItec === true
                ? undefined
                : <Col size = {1}>
                    <TouchableOpacity 
                      style = {{alignItems:'center', borderRightWidth: 2, borderColor: '#e3ebee'}}
                      onPress = {() => {this.onPress_Nav('Tuition')}}>
                      {/* <Image style = {styles.iconMinor1} source={require('./img/icoTuition.png')} /> */}
                      <Image style = {styles.iconMinor1} source={{uri: config.ICON_2}} />
                      <Text style = {styles.textMinorIcon1}>Học phí{'\n'}</Text>
                    </TouchableOpacity> 
                  </Col>
              }   
              <Col size = {1}>
                <TouchableOpacity 
                  style = {{alignItems:'center'}}
                  onPress = {() => {this.onPress_Nav('Checking')}}>
                    <Image style = {styles.iconMinor1} source={require('./img/icoCheckin.png')} />
                    <Text style = {styles.textMinorIcon1}>Thông tin{'\n'}điểm danh</Text>
                </TouchableOpacity> 
              </Col>
              <Col size = {1}>
                <TouchableOpacity  
                  style = {{alignItems:'center', borderLeftWidth: 2, borderColor: '#e3ebee'}}
                  onPress = {() => {this.onPress_Nav('Calendar')}}>
                    <Image style = {styles.iconMinor1} source={require('./img/icoCalander.png')} />
                    <Text style = {styles.textMinorIcon1}>Lịch học{'\n'}</Text>
                </TouchableOpacity>
              </Col>
            </Row>
            <Row style = {{justifyContent: 'center'}}>
              <View style = {{width: WIDTH * 0.04}}/>
              <SlideShow banners = {this.props.user.Banners}/>
            </Row>
            <Row style = {{justifyContent:'center', alignItems: 'center', height: WIDTH / 3.6}}>
              <Col size = {1}>
                <TouchableOpacity 
                  style = {{alignItems:'center'}}
                  onPress = {() => {this.onPressGoWeb('huongdan')}}> 
                    <Image style = {styles.iconMinor2} source={require('./img/icoGuide.png')} />
                    <Text style = {styles.textMinorIcon2}>Hướng dẫn</Text>
                </TouchableOpacity> 
              </Col>
              {
                this.props.isItec === true
                ? undefined
                : <Col size = {1}>
                    <TouchableOpacity 
                      style = {{alignItems:'center'}}
                      onPress = {() => {this.onPressGoWeb('hotro')}}> 
                      {/* <Image style = {styles.iconMinor2} source={require('./img/icoContact.png')} /> */}
                      <Image style = {styles.iconMinor2} source={{uri: config.ICON_3}} />
                      <Text style = {styles.textMinorIcon2}>Hỗ trợ</Text>
                    </TouchableOpacity> 
                  </Col>
              } 
              <Col size = {1}>
                <TouchableOpacity 
                  style = {{alignItems:'center'}}
                  onPress = {() => {this.onPressGoWeb('gioithieu')}}> 
                    <Image style = {styles.iconMinor2} source={require('./img/icoAbout.png')} />
                    <Text style = {styles.textMinorIcon2}>Giới thiệu</Text>
                </TouchableOpacity>
              </Col>
            </Row>
          </Grid>    
        </ScrollView>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  iconMain: {
    width: moderateScale(70),
    height: moderateScale(54),
  },
  iconMinor1:{
    width: moderateScale(40),
    height: moderateScale(41),
  },
  iconMinor2:{
    width: moderateScale(40),
    height: moderateScale(41),
  },
  container: {
    alignItems : 'center'
  },
  background: {
    width: WIDTH,
    height: WIDTH / 2.5,
  },
  logo: {
    marginTop:60,   
  },
  form: {
    marginTop:35,
  },
  txtMoney:{
    fontFamily: FontFamily.SFUISemibold,
    fontSize: scale(16),
    lineHeight: 15,
    letterSpacing: 0.17,
    textAlign: "center",
    color: 'white',
    backgroundColor: 'transparent'
  },
  textMainIcon: {
    fontSize: scale(12),
    letterSpacing: 0.31,
    textAlign: "center",
    color: "#ade4ff",
    fontFamily: FontFamily.SFUIMedium,
    backgroundColor: 'transparent'
  },
  textMinorIcon1: {
    marginTop: 6,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: scale(15),
    letterSpacing: 0.16,
    textAlign: "center",
    color: "#222f5c",
    backgroundColor: 'transparent'
  },
  textMinorIcon2: {
    marginTop: 6,
    fontFamily: FontFamily.SFUIRegular,
    opacity: 0.78,
    fontSize: scale(15),
    letterSpacing: 0.16,
    textAlign: "center",
    color: "#6283a3",
    backgroundColor: 'transparent'
  }
});