import React, { Component } from 'react';
import TitleInfoComponent from '../TitleInfoScreen/TitleInfoComponent';
import { 
  StyleSheet, 
  Image, 
  View,
  Text,
  FlatList,
  TouchableOpacity,
  NetInfo,
} from 'react-native';
import * as FontFamily from '../../constants/FontFamily';
import {Grid, Row, Col} from 'react-native-easy-grid';
import Avatar from '../../components/Avatar';
import Header from '../../components/Header';
import IconNodata from '../../components/IconNodata';
import AndroidBackButton from 'android-back-button';
import update from 'immutability-helper';

export default class NotiScreen extends Component{
  constructor(props){
    super(props);
  }

  componentWillMount(){
    this.props.navigation.setParams({
      tabActive: this._tabActive,
    });
    this.refreshList();
  }

  _tabActive = () => {
                      this.props.getNoti(this.props.username, 
                                          '',
                                          '01/12/2017',
                                          '',
                                          this.props.tokenid);
  }
  
  refreshList(){
    //-----------------------LOAD DATA VAO MANG----------------------------
    this.props.getNoti(this.props.username, 
                        '',
                        '01/12/2017',
                        '',
                        this.props.tokenid);
    //---------------------------------------------------------------------    
  }
  onPress_GoNotiDetail(item){
    if(item.status != 2) this.props.readNoti(this.props.username, 
                                              item.student_code, 
                                              item.id, 
                                              this.props.tokenid);
    this.props.navigation.navigate('NotiDetail', {Noti: item});
  }

  onPressReadAll(){
    let cards = [];
    let cardnumber = '';
    if(this.props.cards.length === 1) cardnumber = this.props.cards[0].card_number;
    else {
      this.props.cards.map((data) => {
        cards.push(data.card_number);
      });
    }
    this.props.readNotiAll(this.props.username, cardnumber, cards, this.props.tokenid);
  }

  render(){
    return(
      <View style = {{flex:1, backgroundColor: 'white'}}>
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <Header
          Left = 'none'
          Body = 'THÔNG BÁO'
          Right = {
            <TouchableOpacity onPress = {() => {this.onPressReadAll()}}>
              <Image
                style = {styles.iconMarkRead}
                source={require('./img/icoReadAll.png')} />
            </TouchableOpacity>
          }
        />
        {/* <TitleInfoComponent onPress = {() => {this.props.navigation.navigate('ChooseCard')}} /> */}
        <View style = {{flex:1}}>
        {
          this.props.noti.notifications.length === 0
          ?<View style = {{flex: 1, alignItems: 'center', marginTop: 100}}>
            <IconNodata width = {75} height = {75}/>
          </View> 
          :<FlatList 
            refreshing = {this.props.isLoading}
            onRefresh = {() => {this.refreshList()}}
            data = {this.props.noti.notifications}
            renderItem = {
              ({item}) => 
                <TouchableOpacity 
                  onPress={() => {this.onPress_GoNotiDetail(item)}}
                  style = {{borderColor: '#dfecf1', borderBottomWidth: 1}}>
                  <Grid style = {item.status === 2 ? styles.bkgRead : styles.bkgNotRead}>
                    <Col size={18} style = {{alignItems:'center', justifyContent: 'center'}}>
                      <Avatar 
                        width = {35} 
                        height = {35} 
                        url = {require('./img/icoBycash.png')}
                      />
                    </Col>
                    <Col size={82} style = {{justifyContent: 'center'}}>
                      <Text style = {styles.textTitle}>{item.subject}</Text>
                      <Text style = {styles.textBrief}>{item.title}</Text>
                      <Text style = {styles.textTime}>{item.send_date}</Text>
                      <View style = {{height: 3}}/>
                    </Col>
                  </Grid>
                </TouchableOpacity>
            }
            keyExtractor={(item, index) => index}
          />
        }
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  bkgRead: {
    // height: 110,    
  },
  bkgNotRead: {
    // height: 110,
    backgroundColor: '#eaf8fc'
  },
  container: {
    alignItems : 'center'
  },
  background: {
    flex:1,
    flexDirection: 'row', 
    justifyContent:'space-around', 
    alignItems: 'center',  
    //alignItems: 'center',
  },
  iconMarkRead: {
    width: 35, 
    height: 19, 
  },
  title: {
    textAlign: 'center', 
    color: 'white', 
    fontSize: 18, 
    fontFamily: FontFamily.SFUIMedium, 
    marginTop: 30,
  },
  icon: {
    width:35,
    height:35   
  },
  textTitle: {
    marginTop: 3,
    fontFamily: FontFamily.SFUIMedium,
    fontSize: 16,
    color: "#222f5c"
  },
  textBrief: {
    marginTop: 3,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 12,
    color: "#878b9a"
  },
  textTime: {
    marginTop: 3,
    fontFamily: FontFamily.SFUIRegular,
    fontSize: 12,
    color: "#2061b3"
  },
  textDate: {
    fontFamily: FontFamily.SFUISemibold,
    fontSize: 12,
    color: "white",
    width: 47,
    height: 19,
    borderRadius: 9.5,
    backgroundColor: "#6283a3",
    textAlign:'center',
    justifyContent: 'center'
  },
});