import React, { Component } from 'react';
import { Text, TouchableOpacity, StyleSheet, Image, Modal, View, Dimensions, Button } from 'react-native';
import * as FontFamily from '../../constants/FontFamily';
import {Grid, Row, Col} from 'react-native-easy-grid';
import MainButton from '../../components/MainButton';
import {moderateScale, WIDTH} from '../../components/ScaleRes';
import IconClose from '../../components/IconClose';
import Avatar from '../../components/Avatar';
import AndroidBackButton from 'android-back-button'
import IconBin from '../../components/IconBin';
export default class NotiDetailScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      chooseDay: this.props.PayDay,
    }
  }
  
  onDelete(){    
    let ids = [this.props.navigation.state.params.Noti.id];
    this.props.delNoti(this.props.Username,
                        this.props.navigation.state.params.Noti.student_code,
                        ids,
                        this.props.TokenID);
    this.props.navigation.goBack();
  }

  onPressChooseDay(day){
    this.setState({chooseDay:day});
  }

  render() {
    return (
      <View style={{position: 'absolute',bottom: 0}}>
        <AndroidBackButton
            onPress={() => {
                this.props.navigation.goBack();
                return true;
            }}
        />
        <View style = {styles.header}>
          <Grid>
            <Col size = {1}>
              <TouchableOpacity onPress = {() => {this.props.navigation.goBack()}}>
                <IconClose/>
                {/* <Image source= {require('../../components/img/icoClose.png')}></Image> */}
              </TouchableOpacity>
            </Col>
            <Col size = {5} style = {{alignItems:'center', justifyContent: 'center'}}>
              <Text style = {styles.title}>CHI TIẾT THÔNG BÁO</Text>
            </Col>
            <Col size = {1}>
                <TouchableOpacity onPress = {() => {this.onDelete()}}>
                    <IconBin/>
                </TouchableOpacity>
            </Col>
          </Grid>
        </View>        
        <View style = {styles.body}>
            <View style ={{marginTop: 20}}/>
            <View>
                <Grid>
                    <Col size = {17} style = {{alignItems: 'center'}}>
                        <Avatar 
                            width = {35} 
                            height = {35} 
                            url = {require('./img/icoBycash.png')}
                        />
                    </Col>
                    <Col size = {78}>
                        <Text style = {styles.txtTitle}>{this.props.navigation.state.params.Noti.subject}</Text>
                        <Text style = {styles.txtTime}>{this.props.navigation.state.params.Noti.send_date}</Text>
                        <Text style = {styles.txtDetail}>{this.props.navigation.state.params.Noti.content}</Text>
                    </Col>
                    <Col size = {5}>
                    </Col>
                </Grid>
            </View>
            <View style ={{marginTop: 20}}/>
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
    txtDetail: {
        marginTop: 12,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 12,
        lineHeight: 18,
        letterSpacing: 0.13,
        textAlign: "left",
        color: "#878b9a"
    },
    txtTime: {
        marginTop: 8,
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 12,
        letterSpacing: 0.13,
        textAlign: "left",
        color: "#2061b3"
    },
    txtTitle: {
        fontFamily: FontFamily.SFUIMedium,
        fontSize: 16,
        letterSpacing: 0.17,
        textAlign: "left",
        color: "#222f5c"
    },
    container: {
        alignItems : 'center'
    },
    header: {
        flex:1,
        flexDirection: 'row',
        height: 50,
        width: WIDTH,
        backgroundColor: "#00a6ec",
    },
    title: {
        fontFamily: FontFamily.SFUIRegular,
        fontSize: 18,
        letterSpacing: 0.19,
        textAlign: "center",
        color: 'white'
    },
    body: {
        width: WIDTH,
        //height: 440,
        backgroundColor: 'white',
        //alignItems: 'center'
    },
});