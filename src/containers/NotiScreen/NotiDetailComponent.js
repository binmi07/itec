import { connect } from 'react-redux'
import NotiDetailScreen from './NotiDetailScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    delNoti,
} from '../../actions';

const mapStateToProps = ( state, ownProps ) => {
    return {
        Username: state.user.Username,
        TokenID: state.user.TokenID,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    delNoti:(username, studentcode, ids, tokenid) => dispatch(delNoti(username, studentcode, ids, tokenid)),
})

const NotiDetailComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( NotiDetailScreen )

export default NotiDetailComponent