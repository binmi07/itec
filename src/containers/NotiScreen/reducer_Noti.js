import * as types from '../../constants/ActionTypes';
import update from 'immutability-helper';
import moment from 'moment-timezone';
import {
  AsyncStorage
} from 'react-native';

const initialState = {  
  username: '',
  notiDate: '',
  notifications: [
    // {  
    //   id:3,
    //   sender:"Truong PTTH Lien Ha",
    //   student_code:"010430000000002",
    //   subject:"Thông báo học phí",
    //   title:"Thông báo học phí",
    //   send_date:"29/11/2017 16:30",
    //   content:"Về việc nộp học phí tháng 11/2017: Nội dung thông báo của Nhà trường",
    //   status:1
    // },
  ],
  TotalNotRead: 0,
};

function unique(a){
  for(var i = 0; i < a.length; ++i) {
      for(var j = i + 1; j < a.length; ++j) {
          if(a[i].id === a[j].id)
              a.splice(j--, 1);
      }
  }
  return a;
};

function getDate(date){
  return date.split(" ")[0];
}

export default function noti(state = initialState, action) {  
  switch (action.type) {
    case types.PAYMENT_VERIFY_SUCCESS:
      return { ...state,
        TotalNotRead: state.TotalNotRead + 1,
      };
    case types.GET_NOTI_FIRST_SUCCESS:
      console.log('REDUCER-GET_NOTI_FIRST_SUCCESS');  
      let total1 = 0;
      action.payload.notifications.map((data) => { if(data.status === 1) total1++; });      
      return { ...state,
                notifications: action.payload.notifications,
                TotalNotRead: total1,
              };
    case types.GET_NOTI_SUCCESS:
      console.log('REDUCER-GET_NOTI_SUCCESS');
      let notifications = unique(action.payload._response.notifications.concat(state.notifications));
      notifications.sort(function (a, b) 
      { 
        let c  = moment(a.send_date, "DD/MM/YYYY HH:mm"); 
        let d  = moment(b.send_date, "DD/MM/YYYY HH:mm");
        return d - c
      });
      AsyncStorage.setItem(state.username + 'noti', JSON.stringify(notifications));
      //lưu lại local ngày reqest noti gần nhất
      AsyncStorage.setItem(state.username + 'lastRequestNoti', getDate(action.payload._response.to_date));
      let total = 0;
      notifications.map((data) => { if(data.status === 1) total++; });      
      return { ...state,
                notifications: notifications,
                TotalNotRead: total,
              };
    case types.READ_NOTI_SUCCESS:
      console.log('REDUCER-READ_NOTI_SUCCESS'); 
      let check = state.notifications.findIndex(x => x.id == action.payload._response.id); 
      let NotiUpdate = update(state.notifications, {[check]: {status: {$set: 2}}});
      AsyncStorage.setItem(state.username + 'noti', JSON.stringify(NotiUpdate));
      return { ...state,
                notifications: NotiUpdate,
                TotalNotRead: state.TotalNotRead - 1,
              };
    case types.READ_NOTI_ALL_SUCCESS:
      console.log('REDUCER-READ_NOTI_ALL_SUCCESS'); 
      let NotiReadAllUpdate = state.notifications;
      for(i = 0; i < state.notifications.length; i++){
        NotiReadAllUpdate = update(NotiReadAllUpdate, {[i]: {status: {$set: 2}}});
      }
      AsyncStorage.setItem(state.username + 'noti', JSON.stringify(NotiReadAllUpdate));
      return { ...state,
                notifications: NotiReadAllUpdate,
                TotalNotRead: 0,
              };        
    case types.DEL_NOTI_SUCCESS:
      console.log('REDUCER-DEL_NOTI_SUCCESS'); 
      let checkDel = state.notifications.findIndex(x => x.id == action.payload._response.ids[0]);    
      let NotiDel = update(state.notifications, {$splice: [[checkDel, 1]]});
      AsyncStorage.setItem(state.username + 'noti', JSON.stringify(NotiDel));
      return { ...state, 
                notifications: NotiDel 
              };
    case types.GET_USER_SUCCESS:
      return { ...state, username: action.payload._response.username 
              };
  }
  return state;
}
