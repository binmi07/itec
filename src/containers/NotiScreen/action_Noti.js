import * as types from '../../constants/ActionTypes';
import * as MesTypes from '../../constants/MesTypes';
import moment from 'moment-timezone';
import {
  Alert,
  AlertIOS,
  Platform,
  AsyncStorage,
} from 'react-native';
var {
    getNoti,
    readNoti,
    delNoti,
    readNotiAll,
} = require('../../services/ITECApi')
import {
  errorProcess
} from '../../components/GlobalVarContainer';

function unique(a){
  for(var i = 0; i < a.length; ++i) {
      for(var j = i + 1; j < a.length; ++j) {
          if(a[i].id === a[j].id)
              a.splice(j--, 1);
      }
  }
  return a;
};

function getAll(){
  //lay tu 180 ngay truoc nua ke tu ngay hien tai neu lan dau su dung tai khoan nay
  return moment().subtract(178, 'days').format('DD/MM/YYYY');
}

function getDate(date){
  return date.splice(" ")[0];
}

export function getNotiFirst(username, studentcode, fromdate, todate, tokenid) {  
  console.log('ACTION-getNotiFirst');  
  return async (dispatch) => {
    dispatch({ type: types.GET_NOTI });
    try{
      const username = await AsyncStorage.getItem('username');
      //lấy ngày gần nhất request get noti để làm from_date
      let lastRequestNoti = await AsyncStorage.getItem(username + 'lastRequestNoti');
      //nếu không có thì lấy 180 ngày trước nữa
      if(lastRequestNoti === null) lastRequestNoti = getAll();
      let response = await getNoti(username, studentcode, lastRequestNoti, todate, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        //lấy danh sách noti lưu ở local      
        const dataLocal = await AsyncStorage.getItem(username + 'noti').then(res => JSON.parse(res));
        //nếu không có thì tạo 1 mảng rỗng
        if(dataLocal === null) dataLocal = [];
        //ghép data local với data lấy từ server vào với nhau, loại bỏ cái noti có id trùng nhau
        let notifications = unique(_response.notifications.concat(dataLocal));
        //lưu lại local danh sách noti
        AsyncStorage.setItem(username + 'noti', JSON.stringify(notifications));
        //lưu lại local ngày reqest noti gần nhất
        AsyncStorage.setItem(username + 'lastRequestNoti', lastRequestNoti);
        if(notifications.length === 0) dispatch({type: types.GET_NOTI_NULL_SUCCESS});
        else dispatch({type: types.GET_NOTI_FIRST_SUCCESS, payload: { notifications }});
      }else{
        dispatch({ type: types.GET_NOTI_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){ console.log(error) }
  }
}

export function getNoti(username, studentcode, fromdate, todate, tokenid) {  
  console.log('ACTION-getNoti');  
  return async (dispatch) => {
    dispatch({ type: types.GET_NOTI });
    try{
      let response = await getNoti(username, studentcode, fromdate, todate, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        if(_response.notifications.length === 0) dispatch({type: types.GET_NOTI_NULL_SUCCESS});
        else dispatch({type: types.GET_NOTI_SUCCESS, payload: { _response }});
      }else{
        dispatch({ type: types.GET_NOTI_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){ console.log(error) }
  }
}



export function readNotiAll(username, cardnumber, cards, tokenid){
  console.log('ACTION-readNotiAll');  
  return async (dispatch) => {
    dispatch({ type: types.READ_NOTI_ALL });
    try{
      let response = await readNotiAll(username, cardnumber, cards, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({
          type: types.READ_NOTI_ALL_SUCCESS,
          payload: { _response }
        });
      }else{
        dispatch({ type: types.READ_NOTI_ALL_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){  }
  }
}  

export function readNoti(username, studencode, notiId, tokenid){
  console.log('ACTION-readNoti');  
  return async (dispatch) => {
    dispatch({ type: types.READ_NOTI });
    try{
      let response = await readNoti(username, studencode, notiId, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({
          type: types.READ_NOTI_SUCCESS,
          payload: { _response }
        });
      }else{
        dispatch({ type: types.READ_NOTI_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){  }
  }
}

export function delNoti(username, studentcode, ids, tokenid){
  console.log('ACTION-delNoti');  
  return async (dispatch) => {
    dispatch({ type: types.DEL_NOTI });
    try{
      let response = await delNoti(username, studentcode, ids, tokenid);
      console.log(response);
      let _response = response.data._55;
      if(response.response_status === 200){
        dispatch({
          type: types.DEL_NOTI_SUCCESS,
          payload: { _response }
        });
      }else{
        dispatch({ type: types.DEL_NOTI_FAILURE });
        errorProcess(response.response_status, _response, dispatch);
      }  
    }catch(error){  }
  }
}

export function updateUsername(username){
  dispatch({ type: types.DEL_NOTI_FAILURE });
  errorProcess(response.response_status, _response, dispatch);
}