import { connect } from 'react-redux'
import NotiScreen from './NotiScreen'
import { StyleSheet, View, Text, Alert } from "react-native";
import * as types from '../../constants/ActionTypes';
import {
    getNoti,
    readNoti,
    readNotiAll
} from '../../actions'

const mapStateToProps = ( state, ownProps ) => {
    return {
        isLoading: state.progressHud.isLoading,
        username: state.user.Username,
        tokenid: state.user.TokenID,
        noti: state.noti,
        cards: state.cards.Cards,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    getNoti: (username, studentcode, fromdate, todate, tokenid) => dispatch(getNoti(username, studentcode, fromdate, todate, tokenid)),
    readNoti: (username, studencode, notiId, tokenid) => dispatch(readNoti(username, studencode, notiId, tokenid)),
    readNotiAll: (username, cardnumber, cards, tokenid) => dispatch(readNotiAll(username, cardnumber, cards, tokenid)),
})

const NotiComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( NotiScreen )

export default NotiComponent