import md5 from "react-native-md5"; 
import {Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';

const API_ROOT = 'http://125.212.207.225:8080/apigateway/services/';
const GET_USER = 'get_user_profile';
const USER_ACTIVATE_REQUEST = 'user_activate_request';
const USER_ACTIVATE_RESEND = 'user_activate_code_resend';
const ACTIVATE = 'user_activate';
const LOGIN = 'login';
const SET_AUTOPAYMENT = 'card_set_auto_payment';
const NEW_PASSWORD = 'set_password';
const RESET_PASSWORD = 'user_reset_account';
const CANCEL_PAYMENT = 'user_payment_cancel';
const CHANGE_PASSWORD = 'change_password';
const GET_CARD_INFO = 'user_get_card_info';
const LOGOUT = 'logout';
const READ_NOTI = 'user_notification_read';
const READ_NOTI_ALL = 'user_notification_read_all';
const GET_NOTI = 'user_get_notification';
const DEL_NOTI = 'user_notification_remove';
const GET_BILL = 'user_get_bill_data';
const PAYMENT_REQUEST = 'user_payment_request';
const PAYMENT_VERIFY = 'user_payment_verify';
const GET_CALENDAR = 'school_get_schedule';
const GET_HISTORY = 'card_get_transaction_history';
const GET_HISTORY_DETAIL = 'get_transaction_details';
const GET_CHECKING = 'student_get_attendance_history';
const GET_CHECKING_DETAIL = 'student_get_attendance_detail';

const AUTH_KEY = "A308975422DBA81BCD99D6FE475159CD";

import * as types from '../constants/ActionTypes';

// Fetches an API response and normalizes the result JSON according to schema.
// This makes every API response have the same shape, regardless of how nested it was.
/*global fetch:false*/
function callApi(endpoint, apiname, key, method){
  console.log('API-Call');
  console.log(endpoint);
  //var params = "User=" + JSON.stringify(data) + ",AuthData=" + md5.hex_md5(JSON.stringify(data) + key);
  
  return fetch(API_ROOT + method + '?' + encodeURI(endpoint),{
    method: method === 'postdata' ? 'POST' : 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=UTF-8',
      'AuthKey': key,
    },
  })
  .then(response => {
    // console.log(response);
    return {"response_status": response.status, "data": response.json()};
  }).catch(error => {
    return {"response_status": 1000, "data": ''}
  })
}

// api services

function getUser(username) {
  console.log('API-getUser');
  const data =  { 
    username:username,
    device:getDevice(),
    os:getOs()
  };
  return callApi('action=' + GET_USER + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + AUTH_KEY),
                 GET_USER, 
                 AUTH_KEY, 
                 'request');
}

function activateUser(username, otp, tokenid) {
  console.log('API-activateUser');
  const data =  { 
    username:username,
    otp:md5.hex_md5(otp + AUTH_KEY),
    token:tokenid, 
    device:getDevice(),
    os:getOs()
  };
  return callApi('action=' + ACTIVATE + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                 ACTIVATE, 
                 tokenid, 
                 'request');
}

function requestSendActivate(username, tokenid){
  console.log('API-requestSendActivate');
  const data =  { 
    username:username,
    token:tokenid, 
    device:getDevice(),
    os:getOs()
  };
  return callApi('action=' + USER_ACTIVATE_REQUEST + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  USER_ACTIVATE_REQUEST, 
                  tokenid, 
                  'request');
}

function requestResendActivate(username, tokenid, retrytime){
  console.log('API-requestResendActivate');
  const data =  { 
    username:username,
    token:tokenid, 
    retrytime:retrytime,
    device:getDevice(),
    os:getOs()
  };
  return callApi('action=' + USER_ACTIVATE_RESEND + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  USER_ACTIVATE_RESEND, 
                  tokenid, 
                  'request');
}

function setPassword(username, password, tokenid){
  console.log('API-newPassword');
  const data = {
    username:username,
    token:tokenid,
    password:md5.hex_md5(password + AUTH_KEY),
    device:getDevice(),
    os:getOs()
  };
  return callApi('action=' + NEW_PASSWORD + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  NEW_PASSWORD, 
                  tokenid, 
                  'request');
}

function login(username, password, tokenid) {
  console.log('API-login');
  const data = { 
    username:username,
    password:md5.hex_md5(password + AUTH_KEY),
    token:tokenid,
    device:getDevice(),
    os:getOs()
  };
  return callApi('action=' + LOGIN + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + AUTH_KEY), 
                 LOGIN, 
                 AUTH_KEY, 
                 'request');
}

function changePassword(username, oldPassword, newPassword, tokenid){
  console.log('API-changePassword');
  const data = { 
    username: username,
    token: tokenid,
    old_password: md5.hex_md5(oldPassword + AUTH_KEY),
    new_password: md5.hex_md5(newPassword + AUTH_KEY),
    device: getDevice(),
    os: getOs()
  };
  return callApi('action=' + CHANGE_PASSWORD + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  CHANGE_PASSWORD, 
                  tokenid, 
                  'request');
}

function logout(username, tokenid){
  console.log('API-logout');
  let data = { 
    username: username,
    token: tokenid,
    device: getDevice(),
    os: getOs()
  };
  return callApi('action=' + LOGOUT + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  LOGOUT, 
                  tokenid, 
                  'request');
}

function resetPassword(username, password, tokenid){
  console.log('API-resetPassword');
  const data = {
    username:username,
    token:tokenid,
    password:md5.hex_md5(password + AUTH_KEY),
    device:getDevice(),
    os:getOs()
  };
  return callApi('action=' + RESET_PASSWORD + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  RESET_PASSWORD, 
                  tokenid, 
                  'request');
}

function setAutopayment(username, cardnumber, tokenid, method, scheduletype, paymentdate){
  console.log('API-setAutopayment');
  const data = {
    username:username,
    token:tokenid,
    card_number:cardnumber,
    action:method,
    device:getDevice(),
    os:getOs(),
    auto_payment:{
      schedule_type:scheduletype,
      payment_date:paymentdate
    }
  };
  return callApi('action=' + SET_AUTOPAYMENT + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  SET_AUTOPAYMENT, 
                  tokenid, 
                  'request');
}

function getCardInfo(username, cardnumber, tokenid){
  console.log('API-getCardInfo');
  const data = {
    username:username,
    token:tokenid,
    card_number:cardnumber,
    device:getDevice(),
    os:getOs(),
  };
  return callApi('action=' + GET_CARD_INFO + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  GET_CARD_INFO, 
                  tokenid, 
                  'request');
}

function readNoti(username, studencode, notiId, tokenid){
  console.log('API-readNoti');
  const data = {
    username:username,
    token:tokenid,
    student_code:studencode,
    id:notiId,
    device:getDevice(),
    os:getOs(),
  };
  return callApi('action=' + READ_NOTI + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  READ_NOTI, 
                  tokenid, 
                  'postdata');
}

function readNotiAll(username, cardnumber, cards, tokenid){
  console.log('API-readNoti');
  let data = cards.length === 0
              ? {
                  username:username,
                  token:tokenid,
                  card_number:cardnumber,
                  device:getDevice(),
                  os:getOs(),
                }
              : {
                  username:username,
                  token:tokenid,
                  cards:cards,
                  device:getDevice(),
                  os:getOs(),
                }
  
  return callApi('action=' + READ_NOTI_ALL + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  READ_NOTI_ALL, 
                  tokenid, 
                  'postdata');
}

function getNoti(username, studentcode, fromDate, toDate, tokenid){
  console.log('API-getNoti');
  const data = {
    username:username,
    token:tokenid,
    student_code:studentcode,
    from_date:fromDate,
    to_date:toDate,
    device:getDevice(),
    os:getOs(),
  };
  return callApi('action=' + GET_NOTI + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  GET_NOTI, 
                  tokenid, 
                  'request');
}

function delNoti(username, studentcode, ids, tokenid){
  console.log('API-delNoti');
  const data = {
    username:username,
    token:tokenid,
    student_code:studentcode,
    ids:ids,
    device:getDevice(),
    os:getOs(),
  };
  return callApi('action=' + DEL_NOTI + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  DEL_NOTI, 
                  tokenid, 
                  'postdata');
}

function getBill(username, studencode, billcycle, tokenid){
  console.log('API-getBill');
  const data = {
    username:username,
    token:tokenid,
    student_code:studencode,
    bill_cycle:billcycle,
    device:getDevice(),
    os:getOs(),
  };
  return callApi('action=' + GET_BILL + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  GET_BILL, 
                  tokenid, 
                  'request');
}

function paymentRequest(username, billnumber, studencode, amount, cardnumber, tokenid, paymentdes) {  
  console.log('API-paymentRequest');
  const data = {
    username: username,
    token: tokenid,
    bill_number: billnumber,
    student_code: studencode,
    amount: amount,
    payment_description: paymentdes,
    card_number: cardnumber,
    device:getDevice(),
    os:getOs(),
  };
  return callApi('action=' + PAYMENT_REQUEST + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  PAYMENT_REQUEST, 
                  tokenid, 
                  'postdata');
}

function paymentVerify(username, billnumber, studencode, amount, cardnumber, verifymethod, otp, password, paymentrefnumber, tokenid) {  
  console.log('API-paymentVerify' + otp);
  let data;
  if(verifymethod === 'password')
    data = {
      username:username,
      token:tokenid,
      bill_number:billnumber,
      student_code:studencode,
      amount:amount,
      card_number:cardnumber,
      verify_method:verifymethod,
      password:md5.hex_md5(password + AUTH_KEY),
      payment_ref_number:paymentrefnumber,
      device:getDevice(),
      os:getOs(),
    };
  else
    data = {
      username:username,
      token:tokenid,
      bill_number:billnumber,
      student_code:studencode,
      amount:amount,
      card_number:cardnumber,
      verify_method:verifymethod,
      otp:md5.hex_md5(otp + AUTH_KEY),
      payment_ref_number:paymentrefnumber,
      device:getDevice(),
      os:getOs(),
    };  
  return callApi('action=' + PAYMENT_VERIFY + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  PAYMENT_VERIFY, 
                  tokenid, 
                  'postdata');
}

function getCalendar(username, studentcode, fromDate, toDate, tokenid) {  
  console.log('API-getCalendar');
  let data = {
      username:username,
      token:tokenid,
      student_code:studentcode,
      from_date:fromDate,
      to_date:toDate,
      device:getDevice(),
      os:getOs(),
    };
  return callApi('action=' + GET_CALENDAR + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  GET_CALENDAR, 
                  tokenid, 
                  'request');
}

function resetPayment(username, tokenid, amount, transref){
  console.log('API-resetPayment');
  let data = {
      username:username,
      token:tokenid,
      amount:amount,
      transaction_ref_number:transref,
      device:getDevice(),
      os:getOs(),
    };
  return callApi('action=' + CANCEL_PAYMENT + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  CANCEL_PAYMENT, 
                  tokenid, 
                  'postdata');
}

function getHistorys(username, tokenid, cardnumber){
  console.log('API-getHistorys');
  let data = {
      username:username,
      token:tokenid,
      card_number:cardnumber,
      device:getDevice(),
      os:getOs(),
    };
  return callApi('action=' + GET_HISTORY + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  GET_HISTORY, 
                  tokenid, 
                  'request');
}

function getHistoryDetail(username, tokenid, cardnumber, transref){
  console.log('API-resetPayment');
  let data = {
      username:username,
      token:tokenid,
      card_number:cardnumber,
      transaction_ref_number: transref,
      device:getDevice(),
      os:getOs(),
    };
  return callApi('action=' + GET_HISTORY_DETAIL + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  GET_HISTORY_DETAIL, 
                  tokenid, 
                  'request');
}

function getChecking(username, tokenid, cardnumber, studencode, fromDate, toDate){
  console.log('API-getHistorys');
  let data = {
      username:username,
      token:tokenid,
      card_number:cardnumber,
      student_code:studencode,
      from_date:fromDate,
      to_date:toDate,
      device:getDevice(),
      os:getOs(),
    };
  return callApi('action=' + GET_CHECKING + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  GET_CHECKING, 
                  tokenid, 
                  'request');
}

function getCheckingDetail(username, tokenid, cardnumber, studencode, date){
  console.log('API-getHistorys');
  let data = {
      username:username,
      token:tokenid,
      card_number:cardnumber,
      student_code:studencode,
      date:date,
      device:getDevice(),
      os:getOs(),
    };
  return callApi('action=' + GET_CHECKING_DETAIL + '&data=' + JSON.stringify(data) + '&sigature=' + md5.hex_md5(JSON.stringify(data) + tokenid), 
                  GET_CHECKING_DETAIL, 
                  tokenid, 
                  'request');
}

function getDevice(){
  return {
    deviceID: (Platform.OS === 'ios') ? DeviceInfo.getUniqueID() : DeviceInfo.getDeviceId(),
    name:DeviceInfo.getDeviceName(),
    model:DeviceInfo.getModel(),
    imei:"",
    iccid:"",
    meid:"",
    firmware:""
  }
}

// jsonDevice.addProperty("DeviceID", "123456789123");
// jsonDevice.addProperty("Name", "iphone 5S");
// jsonDevice.addProperty("Model", "iphone 5S");
// jsonDevice.addProperty("IMEI", "3587123456");
// jsonDevice.addProperty("ICCID", "898412365478");
// jsonDevice.addProperty("MEID", "3587123456");
// jsonDevice.addProperty("Firmware", "8.01.00");

function getOs(){
  return {
    name:Platform.OS,
    version:Platform.Version,
  }
}

function callApi2(endpoint, apiname, key) {
  console.log('API-Call');
  console.log(encodeURI(endpoint));
  var http = new XMLHttpRequest();
  //var params = "User=" + JSON.stringify(data) + ",AuthData=" + AUTH_KEY;
  
  http.open("GET", API_ROOT + apiname + '?' + encodeURI(endpoint), true);    
  http.setRequestHeader("Accept", "application/json");
  http.setRequestHeader("AuthKey", key);
  
  http.onreadystatechange = function() {
      if(http.readyState === 4 && http.status === 200) {
        console.log(http.responseText);
        return http.responseText;
      }else{
        return Promise.reject(http.responseText);
      }
  }

  http.send(null);
}

function callApi1(endpoint) {
  const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? `${API_ROOT}${endpoint}` : `{endpoint}`;
  console.log(fullUrl);
  return fetch(fullUrl)
    .then(response =>
      response.json().then(json => ({ json, response }))
    ).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      if (json.status !== 0) {
        return Promise.reject(json);
      }
      return json;
    });
}

module.exports = {
  getUser,
  activateUser,
  requestSendActivate,
  requestResendActivate,
  setPassword,
  login,
  logout,
  changePassword, 
  resetPassword,
  setAutopayment,
  getNoti,
  readNoti,
  readNotiAll,
  delNoti,
  getBill,
  paymentRequest,
  paymentVerify,
  getCalendar,
  resetPayment,
  getHistorys,
  getHistoryDetail,
  getChecking,
  getCheckingDetail,
};


