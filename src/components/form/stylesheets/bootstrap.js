/**
* @Author: Hoang Pham Huu <hoangph>
* @Date:   11-07-2016 4:22
* @Email:  phamhuuhoang@gmail.com
* @Last modified by:   hoangph
* @Last modified time: 15/07/2016 1:19
*/

var LABEL_COLOR = '#000000';
var INPUT_COLOR = '#000000';
var ERROR_COLOR = '#a94442';
var HELP_COLOR = '#999999';
var BORDER_COLOR = '#cccccc';
var DISABLED_COLOR = '#777777';
var DISABLED_BACKGROUND_COLOR = '#eeeeee';
var FONT_SIZE = 17;
var FONT_WEIGHT = '500';

var stylesheet = {
  fieldset: {},
  formWrapper: {

  },
  // the style applied to the container of all inputs
  formGroup: {
    normal: {
      marginBottom: 5,
      marginLeft: 8,
      marginRight: 8,
      flexDirection: 'row',
    },
    error: {
      marginBottom: 5,
      marginLeft: 8,
      marginRight: 8,
      flexDirection: 'row',
    }
  },
  textboxContainer: {
    flex: 1,
    height: 30,
    borderColor: BORDER_COLOR,
    marginBottom: 5,
    borderBottomWidth: 1,
  },
  showPasswordButton: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'flex-end',
    position: 'absolute',
    marginLeft: -30,
  },
  showPasswordButtonImage: {
    resizeMode: 'contain',
    marginBottom: 5,
  },
  controlLabel: {
    normal: {
      color: LABEL_COLOR,
      fontSize: FONT_SIZE,
      marginTop: 7,
      marginBottom: 7,
      fontWeight: FONT_WEIGHT,
      width: 85,
    },
    // the style applied when a validation error occours
    error: {
      color: ERROR_COLOR,
      fontSize: FONT_SIZE,
      marginTop: 7,
      marginBottom: 7,
      fontWeight: FONT_WEIGHT,
      width: 85,
    }
  },
  helpBlock: {
    normal: {
      color: HELP_COLOR,
      fontSize: FONT_SIZE,
      marginBottom: 2
    },
    // the style applied when a validation error occours
    error: {
      color: HELP_COLOR,
      fontSize: FONT_SIZE,
      marginBottom: 2
    }
  },
  errorBlock: {
    fontSize: FONT_SIZE - 8,
    marginTop: -5,
    marginBottom: 2,
    marginLeft: 93,
    color: ERROR_COLOR
  },
  textbox: {
    normal: {
      flex: 1,
      color: INPUT_COLOR,
      backgroundColor: 'transparent',
      fontSize: FONT_SIZE,
      height: 36,
      padding: 0,
      borderRadius: 4,
      borderColor: BORDER_COLOR,
      borderWidth: 0,
      marginTop: 0,
      marginBottom: -10,
    },
    // the style applied when a validation error occours
    error: {
      flex: 1,
      color: INPUT_COLOR,
      backgroundColor: 'transparent',
      fontSize: FONT_SIZE,
      height: 36,
      padding: 0,
      borderRadius: 4,
      borderColor: ERROR_COLOR,
      borderWidth: 0,
      marginBottom: -10,
    },
    // the style applied when the textbox is not editable
    notEditable: {
      fontSize: FONT_SIZE,
      height: 36,
      padding: 7,
      borderRadius: 4,
      borderColor: BORDER_COLOR,
      borderWidth: 1,
      marginBottom: 5,
      color: DISABLED_COLOR,
      backgroundColor: DISABLED_BACKGROUND_COLOR
    }
  },
  checkbox: {
    normal: {
      marginBottom: 4
    },
    // the style applied when a validation error occours
    error: {
      marginBottom: 4
    }
  },
  select: {
    normal: {
      marginBottom: 4
    },
    // the style applied when a validation error occours
    error: {
      marginBottom: 4
    }
  },
  datepicker: {
    normal: {
      marginBottom: 4
    },
    // the style applied when a validation error occours
    error: {
      marginBottom: 4
    }
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
};

export default stylesheet;
