/**
* @Author: Hoang Pham Huu <hoangph>
* @Date:   11-07-2016 4:13
* @Email:  phamhuuhoang@gmail.com
* @Last modified by:   hoangph
* @Last modified time: 11-07-2016 6:25
*/

import t from 'tcomb-form-native/lib';
import stylesheet from './stylesheets/bootstrap';
import templates from 'tcomb-form-native/lib/templates/bootstrap';
import textbox from './templates/textbox';

export function configureForm() {
  templates.textbox = textbox;
  t.form.Form.stylesheet = stylesheet;
  t.form.Form.templates = templates;
  t.form.Form.defaultProps = {
    templates: t.form.Form.templates,
    stylesheet: t.form.Form.stylesheet,
    i18n: t.form.Form.i18n
  };
}
