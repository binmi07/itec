var React = require('react');
var { View, Text, TextInput, Image, TouchableOpacity } = require('react-native');

class CustomTextbox extends React.Component {

  state = {
    passwordDisplayed: false
  }

  toggleDisplay() {
    this.setState({ passwordDisplayed: !this.state.passwordDisplayed });
  }

  render() {
    let locals = this.props.locals;
    if (locals.hidden) {
      return null;
    }

    var stylesheet = locals.stylesheet;
    var formGroupStyle = stylesheet.formGroup.normal;
    var formWrapperStyle = stylesheet.formWrapper;
    var controlLabelStyle = stylesheet.controlLabel.normal;
    var textboxStyle = stylesheet.textbox.normal;
    var textBoxContainerStyle = stylesheet.textboxContainer;
    var showPasswordButtonStyle = stylesheet.showPasswordButton;
    var showPasswordButtonImageStyle = stylesheet.showPasswordButtonImage;
    var helpBlockStyle = stylesheet.helpBlock.normal;
    var errorBlockStyle = stylesheet.errorBlock;

    if (locals.hasError) {
      formGroupStyle = stylesheet.formGroup.error;
      controlLabelStyle = stylesheet.controlLabel.error;
      textboxStyle = stylesheet.textbox.error;
      helpBlockStyle = stylesheet.helpBlock.error;
    }

    if (locals.editable === false) {
      textboxStyle = stylesheet.textbox.notEditable;
    }
    // console.log(locals);
    var asterisk = locals.asterisk === true ? <Text style={[controlLabelStyle, { color: '#FF0000', width: 12 }]}>*</Text> : null;
    var label = locals.label ? <Text style={controlLabelStyle}>{locals.label}</Text> : null;
    var help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null;
    var error = locals.hasError && locals.error ? <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>{locals.error}</Text> : null;
    var showPassword = locals.secureTextEntry ?
    <TouchableOpacity
      accessibilityTraits="button"
      onPress={this.toggleDisplay.bind(this)}
      activeOpacity={0.5}
      style={showPasswordButtonStyle}>
      <Image style={[showPasswordButtonImageStyle, {opacity: this.state.passwordDisplayed ? 0.5 : 1}]} source={require('./img/common_eye.png')} />
    </TouchableOpacity> : null;
    return (
      <View style={formWrapperStyle}>
        <View style={formGroupStyle}>
          {asterisk}
          {label}
          <View style={textBoxContainerStyle}>
            <TextInput
              accessibilityLabel={locals.label}
              ref="input"
              autoCapitalize={locals.autoCapitalize}
              autoCorrect={locals.autoCorrect}
              autoFocus={locals.autoFocus}
              blurOnSubmit={locals.blurOnSubmit}
              editable={locals.editable}
              keyboardType={locals.keyboardType}
              maxLength={locals.maxLength}
              multiline={locals.multiline}
              onBlur={locals.onBlur}
              onEndEditing={locals.onEndEditing}
              onFocus={locals.onFocus}
              onLayout={locals.onLayout}
              onSelectionChange={locals.onSelectionChange}
              onSubmitEditing={(event) => { this.refs.input.blur(); locals.onSubmitEditing ? locals.onSubmitEditing() : null; }}
              placeholderTextColor={locals.placeholderTextColor}
              secureTextEntry={locals.secureTextEntry && (this.state.passwordDisplayed ? false : true) }
              selectTextOnFocus={locals.selectTextOnFocus}
              selectionColor={locals.selectionColor}
              numberOfLines={locals.numberOfLines}
              underlineColorAndroid={locals.underlineColorAndroid}
              clearButtonMode={locals.clearButtonMode}
              clearTextOnFocus={locals.clearTextOnFocus}
              enablesReturnKeyAutomatically={locals.enablesReturnKeyAutomatically}
              keyboardAppearance={locals.keyboardAppearance}
              onKeyPress={locals.onKeyPress}
              returnKeyType={locals.returnKeyType}
              selectionState={locals.selectionState}
              onChangeText={(value) => locals.onChange(value)}
              onChange={locals.onChangeNative}
              placeholder={locals.placeholder}
              style={textboxStyle}
              value={locals.value}
            />
          </View>
          {showPassword}
        </View>
        {help}
        {error}
      </View>
    );
  }

}

function textbox(locals) {
  return (
    <CustomTextbox ref="input" locals={locals} />
  );
}

module.exports = textbox;
