import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';

class RadioButton extends React.Component {
    static propTypes = {
        active: PropTypes.bool,
        disable: PropTypes.bool,
    };

    static defaultProps = {
        active: false,
        disable: false
    };

    render() {
        return (
            <View style = {this.props.active ? (this.props.disable ? styles.circleBigActiveDisable : styles.circleBigActive) 
                                              : styles.circleBig}>
                <View style = {this.props.active ? (this.props.disable ? styles.circleSmallActiveDisable : styles.circleSmallActive)
                                                  : styles.circleSmall}/>
            </View> 
    );
  }
}

var styles = StyleSheet.create({
    circleBigActiveDisable: {
        width: 25,
        height: 25,
        backgroundColor: "#b3bfc4",
        borderRadius: 25/2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circleSmallActiveDisable: {
        width: 12.5,
        height: 12.5,
        opacity: 0.4,
        backgroundColor: '#d8f4ff',
        borderRadius: 12.5,
    },  
    circleBigActive: {
        width: 25,
        height: 25,
        backgroundColor: "#00a6ec",
        borderRadius: 25/2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circleSmallActive: {
        width: 12.5,
        height: 12.5,
        opacity: 0.4,
        backgroundColor: 'white',
        borderRadius: 12.5,
    },  
    circleBig: {
        width: 25,
        height: 25,
        backgroundColor: "#e5edf0",
        shadowColor: "rgba(0, 0, 0, 0.04)",
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 0,
        shadowOpacity: 1,
        borderRadius: 25/2,
    },
    circleSmall: {

    },  
});

module.exports = RadioButton;
