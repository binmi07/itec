import React from 'react';
import {Image} from 'react-native';
import PropTypes from 'prop-types';

class IconBin extends React.Component {
    static propTypes = {
        width: PropTypes.number,
        height: PropTypes.number,
    };

    static defaultProps = {
        width: 50,
        height: 50,
    };

    render() {
        return (
            <Image 
                style = {{width: this.props.width, height: this.props.height}} 
                source = {require('./img/icoTrash.png')}/>
        );
    }
}


module.exports = IconBin;
