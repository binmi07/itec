import React from 'react';
import {Image} from 'react-native';
import PropTypes from 'prop-types';

class IconGo extends React.Component {
    static propTypes = {
        width: PropTypes.number,
        height: PropTypes.number,
        style: PropTypes.any,
    };

    static defaultProps = {
        width: 7,
        height: 12,
    };

    render() {
        return (
            <Image 
                style = {[{width: this.props.width, height: this.props.height}, this.props.style]} 
                source = {require('./img/icoArow.png')}/>
        );
    }
}

module.exports = IconGo;
