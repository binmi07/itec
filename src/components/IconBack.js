import React from 'react';
import {Image} from 'react-native';
import PropTypes from 'prop-types';

class IconBack extends React.Component {   
    static propTypes = {
        width: PropTypes.number,
        height: PropTypes.number,
    };

    static defaultProps = {
        width: 49,
        height: 49,
    };

    render() {
        return (
            <Image 
                style = {{width: this.props.width, height: this.props.height}} 
                source = {require('./img/icoBack.png')}/>
        );
    }
}

module.exports = IconBack;
