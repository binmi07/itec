import React from 'react';
import { 
    View, 
    StyleSheet, 
    Text,
    TextInput
} from 'react-native';

class TextInputForm extends React.Component {
    props: {
        width: number,
        height: number,
        placeholder: string,
        text: string,
    };

    constructor(props){
        super(props);
        this.state = {
            text: props.text == null ? '' : props.text,
        }
    }

    static defaultProps = {
        
    };

    render() {
        return (
            <View 
                style = {{
                            width: this.props.width,
                            height: this.props.height,
                            borderRadius: 4,
                            backgroundColor: 'white',}}>
                <TextInput
                    ref = 'input'
                    style = {{
                                textAlign: 'center',
                                width: this.props.width}}
                    caretHidden = {true}
                    placeholder = {this.props.placeholder}
                    placeholderTextColor = '#a5a8b3'
                    underlineColorAndroid = 'rgba(0,0,0,0)'
                    onChangeText = {(text) => this.setState({text})}
                    value={this.state.text}
                />
            </View>
    );
  }
}

var styles = StyleSheet.create({
    
});

module.exports = TextInputForm;
