import React from 'react';
import FontFamily from '../constants/FontFamily'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import {scale, moderateScale, verticalScale} from './ScaleRes';
import PropTypes from 'prop-types';

class SmallButton extends React.Component {   
    static propTypes = {
        disable: PropTypes.bool,
        width: PropTypes.number,
        height: PropTypes.number,
        backgroundColor: PropTypes.string,
        text: PropTypes.string,
        textSize: PropTypes.number,
        textFont: PropTypes.string,
        textColor: PropTypes.string,
        style: PropTypes.any,
        onPress: PropTypes.func,
    };

    static defaultProps = {
        disable: false,
        width: 76,
        height: 29,
        textSize: 12,
        textColor: 'black',
        backgroundColor: '#eefaff',
    };

    render() {
        if(this.props.disable === false){
            return (
                <TouchableOpacity onPress = {this.props.onPress}>
                    <View style = {[{
                                    width: this.props.width,
                                    height: this.props.height,
                                    backgroundColor: this.props.backgroundColor,
                                    borderRadius: 3,
                                    alignItems: 'center',
                                    justifyContent: 'center'}, this.props.style]}>
                        <Text style = {{
                                        fontFamily: this.props.textFont,
                                        fontSize: this.props.textSize,
                                        color: this.props.textColor,
                                        letterSpacing: 0.19,
                                        textAlign: "center"}}>
                            {this.props.text}
                        </Text>
                    </View>
                </TouchableOpacity>
            );
        }
        else{
            return(
                <View style = {[{
                    width: this.props.width,
                    height: this.props.height,
                    backgroundColor: '#b3bfc4',
                    borderRadius: 3,
                    alignItems: 'center',
                    justifyContent: 'center'}, this.props.style]}>
                    <Text style = {{
                        fontFamily: this.props.textFont,
                        fontSize: this.props.textSize,
                        color: 'white',
                        letterSpacing: 0.14,
                        textAlign: "center"}}>
                        {this.props.text}
                    </Text>
                </View>
            );
        }
    }
}

var styles = StyleSheet.create({
    circleBigActive: {
        width: 25,
        height: 25,
        backgroundColor: "#00a6ec",
        borderRadius: 25/2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circleSmallActive: {
        width: 12.5,
        height: 12.5,
        opacity: 0.4,
        backgroundColor: 'white',
        borderRadius: 12.5,
    },  
    circleBig: {
        width: 25,
        height: 25,
        backgroundColor: "#e5edf0",
        shadowColor: "rgba(0, 0, 0, 0.04)",
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 0,
        shadowOpacity: 1,
        borderRadius: 25/2,
    },
    circleSmall: {

    },  
});

module.exports = SmallButton;
