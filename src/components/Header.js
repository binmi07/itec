import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Text, Image, Dimensions, Platform } from 'react-native';
import * as FontFamily from '../constants/FontFamily';
import {Grid, Row, Col} from 'react-native-easy-grid';
import IconBack from './IconBack';
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
import {moderateScale, scale} from '../components/ScaleRes';

class Header extends React.Component {
    props: {
      Left: 'none' | 'back' | 'icon',
      LeftContent: any,
      Body: string;
      Right: any;
      onPress: void,
    };

    static defaultProps = {
        Left: 'none',
        LeftContent: <View/>,
        Body: 'title',
        Right: <View/>,        
    };

    onPress(){
      this.props.onPress();
    }

    render() {
      let LeftContent;
      if(this.props.Left === 'back') LeftContent = ( <TouchableOpacity onPress = {() => {this.onPress()}}> 
                                                        <IconBack/>
                                                      </TouchableOpacity>);
      return (
        <View>
          <View style = {styles.statusBar}></View>
          <View style = {{height: moderateScale(50), backgroundColor: "#1b3882"}}>          
            <Grid>
              <Col size = {24} style = {{justifyContent: 'center'}}>
                {this.props.Left === 'back' ? LeftContent : this.props.LeftContent}
              </Col>
              <Col size = {60} style = {{alignItems:'center', justifyContent: 'center'}}>
                <Text style = {styles.txtHeader}>{this.props.Body}</Text>
              </Col>
              <Col size = {24} style = {{justifyContent: 'center', alignItems: 'center'}}>
                {this.props.Right}
              </Col>
            </Grid>
          </View>
        </View>
    );
  }
}

var styles = StyleSheet.create({
  statusBar:{
    height: Platform.OS === 'ios' 
            ? HEIGHT === 812 
              ? 44
              : 20 
            : Platform.Version >= 21 
              ? 20 
              : 0,
    backgroundColor: "#1b3882"
  },
  txtHeader: {
    fontFamily: FontFamily.SFUIMedium,
    fontSize: scale(18),
    letterSpacing: 0.47,
    textAlign: "center",
    color: 'white'
  },
});

module.exports = Header;
