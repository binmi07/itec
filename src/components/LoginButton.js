import React from 'react';
import * as FontFamily from '../constants/FontFamily'
import { View, StyleSheet, Text, TouchableOpacity, Platform } from 'react-native';
import {scale, moderateScale, verticalScale} from './ScaleRes';
import PropTypes from 'prop-types';

class LoginButton extends React.Component {
    static propTypes = {
        disable: PropTypes.bool,
        width: PropTypes.number,
        height: PropTypes.number,
        backgroundColor: PropTypes.string,
        text: PropTypes.string,
        textSize: PropTypes.number,
        style: PropTypes.any,
        onPress: PropTypes.func,
    };

    static defaultProps = {
        disable: false,
        width: 315,
        height: 45,
        backgroundColor: '#3cc2fd',
    };

    render() {
        if(this.props.disable === false){
            return (
                <TouchableOpacity onPress = {this.props.onPress}>
                    <View style = {[{
                                    width: this.props.width,
                                    height: this.props.height,
                                    backgroundColor: this.props.backgroundColor,
                                    borderRadius: 4,
                                    ...Platform.select({
                                        ios: {
                                            shadowColor: "#1a285271",
                                            shadowOffset: {
                                                width: 0,   
                                                height: 6
                                            },
                                            shadowRadius: 12,
                                            shadowOpacity: 1,
                                        },
                                        android: {
                                            elevation: 5,
                                        }
                                    }),
                                    alignItems: 'center',
                                    justifyContent: 'center'}, this.props.style]}>
                        <Text style = {{
                                        width: this.props.width,
                                        fontFamily: FontFamily.SFUISemibold,
                                        fontSize: this.props.textSize,
                                        letterSpacing: 0.19,
                                        textAlign: "center",
                                        color: 'white'}}>
                            {this.props.text}
                        </Text>
                    </View>
                </TouchableOpacity>
            );
        }
        else{
            return (
                <View>
                    <View style = {[{
                                    width: this.props.width,
                                    height: this.props.height,
                                    backgroundColor: '#b3bfc4',
                                    borderRadius: 4,                                    
                                    alignItems: 'center',
                                    justifyContent: 'center'}, this.props.style]}>
                        <Text style = {{
                                        fontFamily: FontFamily.SFUISemibold,
                                        fontSize: this.props.textSize,
                                        letterSpacing: 0.19,
                                        textAlign: "center",
                                        color: 'white'}}>
                                        {this.props.text}
                        </Text>
                    </View>
                </View>
            );
        }
    }
}

var styles = StyleSheet.create({
    circleBigActive: {
        width: 25,
        height: 25,
        backgroundColor: "#00a6ec",
        borderRadius: 25/2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circleSmallActive: {
        width: 12.5,
        height: 12.5,
        opacity: 0.4,
        backgroundColor: 'white',
        borderRadius: 12.5,
    },  
    circleBig: {
        width: 25,
        height: 25,
        backgroundColor: "#e5edf0",
        shadowColor: "rgba(0, 0, 0, 0.04)",
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 0,
        shadowOpacity: 1,
        borderRadius: 25/2,
    },
    circleSmall: {

    },  
});

module.exports = LoginButton;
