import Communications from 'react-native-communications';
import {Platform, Alert} from 'react-native';
import * as MesTypes from '../constants/MesTypes';
import * as types from '../constants/ActionTypes';

function getCardActivate(Cards, CardId){
    let result = Cards.findIndex(x => x.id == CardId);
    return Cards[result > -1 ? result : 0];
}

function getCardNoFromStudentCode(Cards, StudenCode){
    let result = Cards.findIndex(x => x.student_code == StudenCode);
    return Cards[result > -1 ? result : 0].card_number;
}

function numberWithCommas(x){
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return parts.join(".") + ' VNĐ';
}

function callPhone(number){
    Alert.alert(
        '',
        MesTypes.NOTI_PHONE_CALL + number,
        [
         {text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
         {text: 'Gọi ngay', onPress: () => Communications.phonecall(number, false)},
        ],
        { cancelable: true }
    )
}

function forgetPass(number){
    Alert.alert(
        '',
        MesTypes.FORGET_PASSWORD,
        [
         {text: 'Hủy', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
         {text: 'Gọi ngay', onPress: () => Communications.phonecall(number, false)},
        ],
        { cancelable: true }
    )
}

function errorProcess(errorType, _response, dispatch){ 
    switch(errorType){
        case 404:
            Alert.alert(
                'Thông báo',
                MesTypes.ERROR_404,
                [ {text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false }); break;
        case 410:
            Alert.alert(
                'Thông báo',
                _response.reason,
                [ {text: 'OK', onPress: () => dispatch({ type: types.GO_SPLASH})},],{ cancelable: false }); break;
        case 500:
            Alert.alert(
                'Thông báo',
                MesTypes.ERROR_500,
                [ {text: 'OK', onPress: () => dispatch({ type: types.GO_MAIN})},],{ cancelable: false }); break;
        case 1000:
            Alert.alert(
                'Thông báo',
                MesTypes.ERROR_NETWORK,
                [ {text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false }); break;
        default:
            Alert.alert(
                'Thông báo',
                _response.reason,
                [ {text: 'OK', onPress: () => console.log('OK Pressed')}, ],{ cancelable: false }); break;
    } 
  } 

function sendEmail(email){
    Communications.email([email], null, null, null, '')
}

module.exports = {
    getCardActivate,
    getCardNoFromStudentCode,
    numberWithCommas,
    callPhone,
    forgetPass,
    sendEmail,
    errorProcess
};