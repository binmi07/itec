import React, {Component} from 'react';
import {Image, TouchableOpacity, StyleSheet, Dimensions, View} from 'react-native';

import Swiper from 'react-native-swiper';
import * as FontFamily from '../constants/FontFamily';
import PropTypes from 'prop-types';
import Communications from 'react-native-communications';

const widthImage = Dimensions.get('window').width * 0.92;
const heightImage = widthImage * 0.92 / 4;

export default class SlideShow extends Component{
  static propTypes = {
    banners: PropTypes.array,
  };

  constructor(props){
    super(props);
    this.state = {
      showSwiper:false,
    }
  }

  componentDidMount(){
    setTimeout(() => {
      this.setState({
          showSwiper:true
      })
      //fix the image no show or can drag  in android
    }, 500)
  }

  render(){
    if (this.state.showSwiper) {
      return (
      <Swiper 
        height = {heightImage + 20} 
        width = {widthImage}
        removeClippedSubviews={false}
        //onMomentumScrollEnd={(e, state, context) => console.log('index:', state.index)}  
        dot = {
          <View 
            style={
              {backgroundColor: '#809bb4', opacity: 0.5, width: 6, height: 6, borderRadius: 4, marginLeft: 7, marginRight: 7}} />}              
        activeDot = {
          <View 
            style={
              {backgroundColor: 'white', width: 8, height: 8, borderWidth:2, borderColor: '#00a6ec', borderRadius: 4, marginLeft: 3, marginRight: 3}} />}
        loop
        autoplay>
        {
          this.props.banners.map((item, key) => {
            return (
              <TouchableOpacity onPress = {() => {Communications.web(item.url_site)}} key={key}>
                <Image resizeMode='stretch' style={styles.image} source={{uri:item.url_image}} /> 
              </TouchableOpacity>
            )
          })
        }
      </Swiper>);
    }else{
      return <View style = {{height: heightImage + 20}}/>;
    }
  }
}

var styles = StyleSheet.create({
  image: {
    borderRadius: 6,
    width: widthImage,
    height: heightImage,
  }
});
