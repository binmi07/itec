import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import PropTypes from 'prop-types';

class Avatar extends React.Component {
    static propTypes = {
        width: PropTypes.number,
        height: PropTypes.number,
        url: PropTypes.any,
    };

    static defaultProps = {
        width: 45,
        height: 45,
    };

    render() {
        return (
            <Image
                style = {{
                            width: this.props.width,
                            height: this.props.height,
                            borderRadius: this.props.width / 2,    
                            overflow: 'hidden'
                            }} 
                source = {this.props.url} />
    );
  }
}

var styles = StyleSheet.create({
    
});

module.exports = Avatar;
