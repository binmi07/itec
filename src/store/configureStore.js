//var {applyMiddleware, createStore} = require('redux');
// var thunk = require('redux-thunk');
// var promise = require('./promise');
//var array = require('./array');
//var createLogger = require('redux-logger');
// var {AsyncStorage} = require('react-native');
import {applyMiddleware, createStore} from 'redux';
import promise from './promise';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import array from './array';
import {createLogger} from 'redux-logger';

var isDebuggingInChrome = __DEV__ && !!window.navigator.userAgent;

var logger = createLogger({
  predicate: (getState, action) => isDebuggingInChrome,
  collapsed: true,
  duration: true,
});

var createITECStore = applyMiddleware(thunk, promise, array, logger)(createStore);

function configureStore() {
  // TODO(frantic): reconsider usage of redux-persist, maybe add cache breaker
  const store = createITECStore(reducers);
  //onComplete();
  if (isDebuggingInChrome) {
    window.store = store;
  }
  return store;
}

module.exports = configureStore;