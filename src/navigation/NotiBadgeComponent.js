import { connect } from 'react-redux';
import NotiBadgeScreen from './NotiBadgeScreen';
import { StyleSheet, View, Text, Alert } from "react-native";

const mapStateToProps = ( state, ownProps ) => {
    return {
        NotiCount: state.noti.TotalNotRead,
    }
}

const mapDispatchToProps = ( dispatch, ownProps ) => ({
    
})

const NotiBadgeComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)( NotiBadgeScreen )

export default NotiBadgeComponent