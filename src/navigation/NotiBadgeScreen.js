import React, { Component } from 'react';
import { Text, Image, View, StyleSheet, Dimensions, Platform } from 'react-native';
import PropTypes from 'prop-types';
import * as FontFamily from '../constants/FontFamily';
const WIDTH = Dimensions.get('window').width;

export default class NotiBadgeScreen extends Component {
  static PropTypes = {
    url: PropTypes.any,
  };

  render() {
   // below is an example notification icon absolutely positioned    
    return (
      <View style={{
        zIndex: 0,
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'space-around',
        alignItems: 'center'}}>
        <Image source = {this.props.url}/>
        {
          this.props.NotiCount > 0 
          ? <View style = {styles.containerBadge}>
              <Text style = {styles.txtBadge}>{this.props.NotiCount}</Text>
            </View>
          : undefined
        }
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container:{

  },
  containerBadge:{
    position: 'absolute',
    top: 2,
    left: Platform.OS === 'android' ? WIDTH / 8 : 0,
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: '#e72121',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2,
  },
  txtBadge:{
    fontFamily: FontFamily.SFUIBold,
    fontSize: 11,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0.12,
    textAlign: "center",
    color: "#f1f5f6"
  }
});