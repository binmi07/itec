import AppNavigator from '../navigation/AppNavigator';
import {NavigationActions} from 'react-navigation'
import * as types from '../constants/ActionTypes';

export default function nav(state, action) {      
    let newState = AppNavigator.router.getStateForAction(action, state);    
    switch (action.type) {
        // case 'Navigation/NAVIGATE':
        //     console.log(state.routes);
        //     break;
        // case 'Navigation/BACK':
        //     console.log(state.routes);
        //     break;
        case types.GO_SPLASH:
            console.log('REDUCER-GO_SPLASH')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({routeName: 'Splash'})
                    ]
                })
            )
            break;
        case types.GO_WELCOME:
            console.log('REDUCER-GOWELCOME')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({routeName: 'Welcome'})
                    ]
                })
            )
            break;
        case types.GO_OTP:
            console.log('REDUCER-GOOTP')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate( {routeName: 'Otp'} ),
                state
            )
            break;
        case types.GO_LOGIN:
            console.log('REDUCER-GOLOGIN')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate( {routeName: 'Login'} ),
                state
            )
            break;
        case types.GO_LOGIN_FIRST:
            console.log('REDUCER-GO_LOGIN_FIRST')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({routeName: 'Login'})
                    ]
                })
            )
            break;
        case types.GO_MAIN:
            console.log('REDUCER-GOMAIN')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({routeName: 'Main'})
                    ]
                })
            )
            break;       
        case types.GO_PIN:
            console.log('REDUCER-GOPIN')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate( {routeName: 'Pin'} ),
                state
            )
            break;
        case types.GO_PASSWORDVERIFY:
            console.log('REDUCER-GO_PASSWORDVERIFY')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate( {routeName: 'PasswordVerify'} ),
                state
            )
            break;
        case types.GO_OTPVERIFY:
            console.log('REDUCER-GO_OTPVERIFY')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate( {routeName: 'OtpVerify'} ),
                state
            )
            break;
        case types.GO_ERRORNETWORK:
            console.log('REDUCER-GO_ERRORNETWORK')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate( {routeName: 'ErrorNetwork'} ),
                state
            )
            break;
        case types.GO_PAYMENTSUCCESS:
            console.log('REDUCER-GO_PAYMENTSUCCESS')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate( {routeName: 'PaymentSuccess'} ),
                state
            )
            break;
        case types.GO_HISTORYDETAIL:
            console.log('REDUCER-GO_HISTORYDETAIL')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate( {routeName: 'HistoryDetail'} ),
                state
            )
            break;
        case types.GO_CHECKINGDETAIL:
            console.log('REDUCER-GO_CHECKINGDETAIL')
            newState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate( {routeName: 'CheckingDetail'} ),
                state
            )  
            break;      
    }
    return newState || state;
};