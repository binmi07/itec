/**
 * Created by Administrator on 3/31/2017.
 */
import React from 'react';
import {TabNavigator, StackNavigator, TabBarBottom, TabBarTop, NavigationActions} from 'react-navigation';
// import Colors from '../constants/Colors';
import * as FontFamily from '../constants/FontFamily';
import {Dimensions, Image, Platform, ImageBackground} from 'react-native';
import NotiBadgeComponent from './NotiBadgeComponent';

//---------------------------------------------TAB MAIN---------------------------------
import HomeComponent from '../containers/HomeScreen/HomeComponent';
import HistoryComponent from '../containers/HistoryScreen/HistoryComponent';
import ProfileComponent from '../containers/ProfileScreen/ProfileComponent';
import NotiComponent from '../containers/NotiScreen/NotiComponent';

//---------------------------------------------
import ThemeForm from '../containers/ThemeForm/ThemeForm';

import PaymentComponent from '../containers/PaymentScreen/PaymentComponent';
import TuitionComponent from '../containers/PaymentScreen/TuitionComponent';
import PasswordVerifyComponent from '../containers/PaymentScreen/PasswordVerifyComponent';
import OtpVerifyComponent from '../containers/PaymentScreen/OtpVerifyComponent';
import PaymentSuccessComponent from '../containers/PaymentScreen/PaymentSuccessComponent';

import SplashComponent from '../containers/LoginScreen/SplashComponent'
import LoginComponent from '../containers/LoginScreen/LoginComponent';
import WelcomeComponent from '../containers/LoginScreen/WelcomeComponent';
import OTPComponent from '../containers/LoginScreen/OTPComponent';
import PINComponent from '../containers/LoginScreen/PINComponent';

import CheckingComponent from '../containers/CheckingScreen/CheckingComponent';
import CalendarComponent from '../containers/CalendarScreen/CalendarComponent';

import AutoPaymentComponent from '../containers/CardManager/AutoPaymentComponent';
import CardManagerComponent from '../containers/CardManager/CardManagerComponent';
import CardInfoComponent from '../containers/CardManager/CardInfoComponent';

import ProfileInfoComponent from '../containers/ProfileScreen/ProfileInfoComponent';
import SupportComponent from '../containers/ProfileScreen/SupportComponent';
import ChangePassComponent from '../containers/ProfileScreen/ChangePassComponent';
import ErrorNetworkComponent from '../containers/ProfileScreen/ErrorNetworkComponent';
import WebViewScreen from '../containers/ProfileScreen/WebViewScreen';

//------------------------------------------------------MODEL SCREEN-------------------------------
import ChooseDayModalComponent from '../containers/CardManager/ChooseDayModalComponent';
import ChooseCardModalComponent from '../containers/CardManager/ChooseCardModalComponent';
import NotiDetailComponent from '../containers/NotiScreen/NotiDetailComponent';
import TuitionDetailsComponent from '../containers/PaymentScreen/TuitionDetailsComponent';
import CheckingDetailsComponent from '../containers/CheckingScreen/CheckingDetailsComponent';
import HistoryDetailComponent from '../containers/HistoryScreen/HistoryDetailComponent';
import PaymentDetailsComponent from '../containers/PaymentScreen/PaymentDetailsComponent';

const MainScreenNavigator = TabNavigator({
    Home: { screen: HomeComponent,},
    History: {screen: HistoryComponent,},
    Noti: {screen: NotiComponent,},
    Profile: {screen: ProfileComponent,},
    }, {
    navigationOptions: ({ navigation }) => ({  
        tabBarOnPress: (tab, jumpToIndex) => {
            const { route, index, focused } = tab.scene;
            // console.log(tab.scene);
            if(!tab.focused){
                switch(route.routeName){
                    case 'Noti':
                    case 'History':
                        try{
                            route.params.tabActive();
                        }catch(error){}
                        break;
                }
                tab.jumpToIndex(tab.scene.index);
            }
        },
        tabBarIcon: ({ focused }) => {        
            const { routeName } = navigation.state;
            let iconName;
            switch (routeName) {
                case 'Home':
                    return (iconName = focused 
                        ?  <Image source = {require('./img/icoHomeActive.png') }/>
                        :  <Image source = {require('./img/icoHome.png') }/>);
                    // iconName = focused ? require('./img/icoHomeActive.png') : require('./img/icoHome.png');break;
                case 'History':
                    return (iconName = focused 
                        ?  <Image source = {require('./img/icoHistoryActive.png') }/>
                        :  <Image source = {require('./img/icoHistory.png') }/>);
                    // iconName = focused ? require('./img/icoHistoryActive.png') : require('./img/icoHistory.png');break;
                case 'Noti':
                    return (iconName = focused 
                        ?   <NotiBadgeComponent url = {require('./img/icoNotiActive.png')} />
                        :   <NotiBadgeComponent url = {require('./img/icoNoti.png')} />);
                    // iconName = focused ? require('./img/icoNotiActive.png'): require('./img/icoNoti.png');break;
                case 'Profile':
                    return (iconName = focused 
                        ?  <Image source = {require('./img/icoProfileActive.png') }/>
                        :  <Image source = {require('./img/icoProfile.png') }/>);
                    // iconName = focused ? require('./img/icoProfileActive.png') : require('./img/icoProfile.png');break;
            }
            return ( <Image source={iconName} /> );
        },
    }),
    swipeEnabled: false,
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    lazy: true,
    animationEnabled: false,
    swipeEnabled: false,   
    tabBarOptions:{
        activeTintColor :'#e91e63',
        showLabel: false,
        style: {				
            elevation: 10,
            marginBottom: 5,
            paddingBottom: 2,            
        }        
    },
});

const MainCardNavigator = StackNavigator({ 
    Splash: {screen: SplashComponent, navigationOptions: { header: null }},
    Main: {screen: MainScreenNavigator, navigationOptions: { header: null }},
    Welcome: {screen: WelcomeComponent, navigationOptions: { header: null }},
    Otp: {screen: OTPComponent, navigationOptions: { header: null }}, 
    Login: {screen: LoginComponent, navigationOptions: { header: null }},  
    Pin: {screen: PINComponent, navigationOptions: { header: null }},
    Payment: {screen: PaymentComponent, navigationOptions: { header: null }},
    AutoPayment: {screen: AutoPaymentComponent, navigationOptions: { header: null }},
    Checking: {screen: CheckingComponent, navigationOptions: { header: null }},
    Calendar: {screen: CalendarComponent, navigationOptions: { header: null }},
    CardManager: {screen: CardManagerComponent, navigationOptions: { header: null }},
    CardInfo: {screen: CardInfoComponent, navigationOptions: { header: null }},
    Theme: {screen: ThemeForm, navigationOptions: { header: null }},
    PaymentDetail: {screen: PaymentDetailsComponent, navigationOptions: { header: null }},
    ProfileInfo: {screen: ProfileInfoComponent, navigationOptions: { header: null }},
    Support: {screen: SupportComponent, navigationOptions: { header: null }},
    ChangePass: {screen: ChangePassComponent, navigationOptions: { header: null }}, 
    ErrorNetwork: {screen: ErrorNetworkComponent, navigationOptions: { header: null }},
    PasswordVerify: {screen: PasswordVerifyComponent, navigationOptions: { header: null }},
    OtpVerify: {screen: OtpVerifyComponent, navigationOptions: { header: null }},
    Tuition: {screen: TuitionComponent, navigationOptions: { header: null }},
    PaymentSuccess: {screen: PaymentSuccessComponent, navigationOptions: { header: null }},
    },{
        initialRouteName: 'Splash',
    }
);

const AppNavigator = StackNavigator({
    MainCardNavigator: { screen: MainCardNavigator },
    ChooseDay: { screen: ChooseDayModalComponent },
    ChooseCard: { screen: ChooseCardModalComponent },
    NotiDetail: { screen: NotiDetailComponent },
    TuitionDetails: { screen: TuitionDetailsComponent },
    CheckingDetail: { screen: CheckingDetailsComponent },
    HistoryDetail: { screen: HistoryDetailComponent },
    WebView: {screen: WebViewScreen},
    },{
        mode: 'modal',
        headerMode: 'none',
        initialRouteName: 'MainCardNavigator',
        cardStyle: {
            backgroundColor: 'rgba(0, 0, 0, 0.67)',
            opacity:0.99,
        },
    },
);

const navigateOnce = (getStateForAction) => (action, state) => {
    const {type, routeName} = action;
    if(state && type === 'Navigation/NAVIGATE' && routeName === state.routes[state.routes.length - 1].routeName) return null;
    else return getStateForAction(action, state);
    // you might want to replace 'null' with 'state' if you're using redux (see comments below)
};

AppNavigator.router.getStateForAction = navigateOnce(AppNavigator.router.getStateForAction);
MainCardNavigator.router.getStateForAction = navigateOnce(MainCardNavigator.router.getStateForAction);

export default AppNavigator