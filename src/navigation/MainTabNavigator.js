import React from 'react';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TabNavigator, TabBarBottom } from 'react-navigation';

import Colors from '../constants/Colors';

import HomeScreen from '../containers/HomeScreen/HomeScreen';
import HistoryScreen from '../containers/HistoryScreen/HistoryScreen';
import NotiScreen from '../containers/NotiScreen/NotiScreen';
import ProfileScreen from '../containers/ProfileScreen/ProfileScreen';
import {Icon} from 'react-native-elements';

export default TabNavigator({
    Home: {
      screen: HomeScreen,
    },
    History: {
      screen: HistoryScreen,
    },
    Noti: {
      screen: NotiScreen,
    },
    Profile: {
      screen: ProfileScreen,
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'Home':
            iconName = 'md-home';
            break;
          case 'History':
            iconName = 'ios-clock';
            break;
          case 'Noti':
            iconName = 'ios-notifications';
            break;
          case 'Profile':
            iconName = 'md-person';
            break;
        }
        return (
          /*<Ionicons
            name={iconName}
            size={20}
            style={{ marginBottom: -5 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />*/
          <Icon 
            name = {iconName}
            type = 'ionicon' />
          );
      },
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);
