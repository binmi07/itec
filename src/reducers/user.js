import * as types from '../constants/ActionTypes';
import update from 'immutability-helper';
import {AsyncStorage} from 'react-native';

const initialState = {
    TokenID: '3EF4415580ED3BBD4AD88C3B30B734AE',
    Username: '01686476291',
    Fullname: 'Nguyễn Văn Quân',
    PhoneNumber: '0936381333',
    LastLoginDateTime: '',
    AvatarURL: '',
    Status: 0,
    DateOfBirth: '', 
    Banners: [
        { 
            name: 'Banner 01',
            url_image: 'http://125.212.207.225:8080/media/images/banner01.png',
            url_site: 'http://ite.com.vn' 
        }
    ],
    about: {
        name: '',
        version: '',
    },
    LastLogin: '',    
    //-----------------------------OTP
    isOtpCounting: false,
    InputOtpCode: '',
    OtpTimeInSecond: 300,
    OtpRetryTime: 1,
    //-----------------------------
    prevKey: '',
    isReceiveNoti: true,
    isLogin: false,    
    isMain: false,
    isFirstStart: true,
    isRegisterSuccess: false,
    isItec: true,
};

function storeUserInfo(type, data){
    switch(type){
        case 'username':
            AsyncStorage.setItem('username', data); break;
        case 'cardidactivate':
            AsyncStorage.setItem('cardidactivate', data); break;
    }    
}

function checkisItec(version){
    let check = version.split(".");
    return check[check.length - 1] === "1" ? true : false;
    // return false;
}

export default function user(state = initialState, action) {
    switch (action.type) {
        case types.GO_SPLASH:
            return {
                ...state,
                isMain: false
            }
        case types.GO_LOGIN:
        case types.GO_LOGIN_FIRST:
            return {
                ...state,
                isLogin: false
            }
        case types.GET_USER_SUCCESS:
            console.log('REDUCER-GET_USER_SUCCESS');
            try{
                if(action.payload._response.force_login === 'true' || action.payload._response.force_login === 'false')
                    storeUserInfo('username', action.payload._response.username);
            }catch(error){ }
            return {...state, 
                        TokenID: action.payload._response.token, 
                        Username: action.payload._response.username, 
                        Fullname: action.payload._response.user_full_name,
                        PhoneNumber: action.payload._response.tel_number,
                        AvatarURL: action.payload._response.url_avatar,
                        Banners: action.payload._response.banners,
                        about: action.payload._response.about,
                        isItec: checkisItec(action.payload._response.about.version),
                        isMain: true};
        //--------------------------------------------------
        case types.NEW_PASSWORD_INMAIN_SUCCESS:
            console.log('REDUCER-NEW_PASSWORD_INMAIN_SUCCESS');
            return {...state, 
                        isMain: false};
        case types.ACTIVATE_SUCCESS:
            console.log('REDUCER-ACTIVATE_SUCCESS');
            storeUserInfo('username', action.payload._response.username);
            return {...state, 
                        Banners: action.payload._response.banners,
                        isLogin: false};
        //--------------------------------------------------
        case types.LOGIN_SUCCESS:
            console.log('REDUCER-LOGIN_SUCCESS');
            return {...state, 
                        TokenID: action.payload._response.token, 
                        // Username: action.payload._response.username, 
                        // Fullname: action.payload._response.user_full_name,
                        // PhoneNumber: action.payload._response.tel_number,
                        // AvatarURL: action.payload._response.url_avatar,
                        LastLogin: action.payload._response.last_login,
                        Banners: action.payload._response.banners,
                        isLogin: true};
        //--------------------------------------------------
        case types.CHANGE_PASSWORD_SUCCESS:
            return { ...state, isMain: false }
        case types.REQUEST_RESEND_OTP:
            return { ...state, OtpRetryTime: state.OtpRetryTime + 1 };
        case types.USER_OTP_COUNTDOWN:
            let OtpTimeInSecond = state.OtpTimeInSecond - 1;
            if (OtpTimeInSecond < 0) OtpTimeInSecond = 0;
            return { ...state, OtpTimeInSecond, isOtpCounting: true };
        case types.USER_INPUT_OTP_CODE:            
            let { InputOtpCode } = action.payload;
            return { ...state, InputOtpCode };
        case types.RESET_OTP_INPUT:
            return { ...state, InputOtpCode: initialState.InputOtpCode };
        case types.RESET_OTP_COUNTDOWN:
            console.log('REDUCER-RESET_OTP_COUNTDOWN');
            return { ...state, OtpTimeInSecond: initialState.OtpTimeInSecond };
        //--------------------------------------------------
        case types.UPDATE_KEY:
            return { ...state, prevKey: action.payload.prevKey }
        case types.UPDATE_NOTI_SETTING:
            let isReceiveNoti = !state.isReceiveNoti;
            return { ...state, isReceiveNoti };
        case types.UPDATE_FIRST_START:
            return { ...state, isFirstStart: false }
        default:
            return state;
    }
}