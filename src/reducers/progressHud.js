import * as types from '../constants/ActionTypes';

const initialState = {
  isLoading: false,
};

export default function user(state = initialState, action) {  
  switch (action.type) {
    case types.GET_USER:
    case types.ACTIVATE:
    case types.NEW_PASSWORD:
    case types.LOGIN:
    case types.LOGOUT:
    case types.SET_AUTOPAYMENT:
    case types.CHANGE_PASSWORD:
    case types.GET_BILL:
    case types.PAYMENT_REQUEST:
    case types.PAYMENT_VERIFY:
    case types.GET_NOTI:
    case types.GET_CALENDAR:
    case types.GET_HISTORYS:
    case types.GET_HISTORY_DETAIL:
    case types.GET_CHECKING:
    case types.GET_CHECKING_DETAIL:
      return { ...state, isLoading: true };
    case types.GET_USER_SUCCESS:
    case types.GET_USER_FAILURE:
    case types.ACTIVATE_SUCCESS:
    case types.ACTIVATE_FAILURE:
    case types.NEW_PASSWORD_SUCCESS:
    case types.NEW_PASSWORD_INMAIN_SUCCESS:
    case types.NEW_PASSWORD_FAILURE:
    case types.LOGIN_SUCCESS:
    case types.LOGIN_FAILURE:
    case types.LOGOUT_SUCCESS:
    case types.LOGOUT_FAILURE:
    case types.SET_AUTOPAYMENT_SUCCESS:
    case types.SET_AUTOPAYMENT_FAILURE:
    case types.CHANGE_PASSWORD_SUCCESS:
    case types.CHANGE_PASSWORD_FAILURE:
    case types.GET_BILL_SUCCESS:
    case types.GET_BILL_FAILURE:
    case types.PAYMENT_REQUEST_SUCCESS:
    case types.PAYMENT_REQUEST_FAILURE:
    case types.PAYMENT_VERIFY_SUCCESS:
    case types.PAYMENT_VERIFY_FAILURE:
    case types.GET_NOTI_SUCCESS:
    case types.GET_NOTI_NULL_SUCCESS:
    case types.GET_NOTI_FIRST_SUCCESS:
    case types.GET_NOTI_FAILURE:
    case types.GET_CALENDAR_SUCCESS:
    case types.GET_CALENDAR_FAILURE:
    case types.GET_HISTORYS_SUCCESS:
    case types.GET_HISTORYS_FAILURE:
    case types.GET_HISTORY_DETAIL_SUCCESS:
    case types.GET_HISTORY_DETAIL_FAILURE:
    case types.GET_CHECKING_SUCCESS:
    case types.GET_CHECKING_FAILURE:
    case types.GET_CHECKING_DETAIL_SUCCESS:
    case types.GET_CHECKING_DETAIL_FAILURE:
      return { ...state, isLoading: false };
      
    default:
      return { ...state, isLoading: false };
  }
  return state;
}
