
import { combineReducers } from 'redux';
import user from './user';
import nav from '../navigation/reducer_Nav';
import progressHud from './progressHud';
import noti from '../containers/NotiScreen/reducer_Noti';
import payment from '../containers/PaymentScreen/reducer_Payment';
import calendar from '../containers/CalendarScreen/reducer_Calendar';
import cards from '../containers/CardManager/reducer_Cards';
import history from '../containers/HistoryScreen/reducer_History';
import checking from '../containers/CheckingScreen/reducer_Checking';

const reducers = combineReducers({
  user: user,
  progressHud: progressHud,
  noti: noti,
  payment: payment,
  calendar: calendar,
  cards: cards,
  history: history,
  checking: checking,
  nav: nav,
});

export default reducers;
