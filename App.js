import React from 'react';
import {Text} from 'react-native';
import {Provider, connect} from 'react-redux'
import {createStore} from 'redux'
import AppNavigator from './src/navigation/AppNavigator'
import reducer from './src/reducers'
import {addNavigationHelpers} from 'react-navigation';
import configureStore from './src/store/configureStore';

import {LocaleConfig} from 'react-native-calendars';

Text.defaultProps.allowFontScaling = false;

LocaleConfig.locales['vn'] = {
  monthNames: ['Tháng 1','Tháng 2','Tháng 3','Tháng 4','Tháng 5','Tháng 6','Tháng 7','Tháng 8','Tháng 9','Tháng 10','Tháng 11','Tháng 12'],
  monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
  dayNames: ['Chủ nhật','Thứ 2','Thứ 3','Thứ 4','Thứ 5','Thứ 6','Thứ 7'],
  dayNamesShort: ['CN','T2','T3','T4','T5','T6','T7']
};

// import OneSignal from 'react-native-onesignal';

LocaleConfig.defaultLocale = 'vn';

const store = configureStore();

const AppWithNavigationState = connect(state => ({
    nav: state.nav,
}))(({dispatch, nav}) => (
    <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })}/>
));


class App extends React.Component {
    constructor() {
        super();
    }

    // componentWillMount() {
    //     OneSignal.addEventListener('received', this.onReceived);
    //     OneSignal.addEventListener('opened', this.onOpened);
    //     OneSignal.addEventListener('registered', this.onRegistered);
    //     OneSignal.addEventListener('ids', this.onIds);
    // }

    // componentWillUnmount() {
    //     OneSignal.removeEventListener('received', this.onReceived);
    //     OneSignal.removeEventListener('opened', this.onOpened);
    //     OneSignal.removeEventListener('registered', this.onRegistered);
    //     OneSignal.removeEventListener('ids', this.onIds);
    // }

    // onReceived(notification) {
    //     console.log("Notification received: ", notification);
    // }

    // onOpened(openResult) {
    //   console.log('Message: ', openResult.notification.payload.body);
    //   console.log('Data: ', openResult.notification.payload.additionalData);
    //   console.log('isActive: ', openResult.notification.isAppInFocus);
    //   console.log('openResult: ', openResult);
    // }

    // onRegistered(notifData) {
    //     console.log("Device had been registered for push notifications!", notifData);
    // }

    // onIds(device) {
	// 	console.log('Device info: ', device);
    // }


    render() {
        return (
            <Provider store={store}>
                <AppWithNavigationState />
            </Provider>
        )
    }
}
module.exports = App;

